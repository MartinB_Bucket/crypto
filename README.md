# PLEASE SEE Branch: Prototype7.5 for latest code #
# Code is being refactored for a new JavaFX interface #

# README #
New features added to this version include:

1. A Sync Manager to help maintain multiple copies of this program.

2. Shortcut Keys to Startmenu items.

3. Login and Create Account error reporting.

4. Secure Deletion of files.

5. A password and username peep button.

6. Complete Settings Menu.

7. Reduced I/O calls

### Using the program ###
>In the 'runnable' folder there is an executable .jar application. This needs at least [Java JDK 1.8](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html) installed.
>When you have launched the program, you must first create an account to use the rest of the features. After creating an account you can then login, you should then see a taskbar of features available to you.


### Getting Set Up In Eclipse ###
> This repository contains an eclipse project, to be able to run the code you will need to have eclipse installed.
> After eclipse has been installed:

> 1. Go to 'File', 'Import', Select 'Projects from Git' under the 'Git' folder and click 'Next'

> 2. Then select 'Clone URI' and click 'Next'

> 3. Then copy and paste the HTTPS text from bitbucket into the URI textbox and enter your password and click next

> 4. Then Import as an existing eclipse project
>
> The main file is Crypto.java, the project can be run from this file
>
### About ###
>
> This java program is an encryption program, and supports features such as:

> - An explorer to allow you to upload and download files

> - A Password Vault to allow account and passwords to be easily maintained

> - A Password Generator to allow you to create a secure password

> - A NotePad so you can edit encrypted files

> - A Picture Viewer to allow the viewing of Encrypted files in memory

###Bug List###
>
>1. Can't choose a non encrypted file for background images

>2. Closing Explorer still leaves it open

>3. Double clicking taskbar items causes unwanted behaviour

> If you find a bug please email me at 'martin@martin-birch.co.uk'

###Bug Fix List###
Version 7
>
>1. Can't delete last account

>2. Double Clicking a file in the explorer doesn't open it

>3. Rename Button hasn't been implemented in the explorer

>4. Ctrl-H doesn't work in NotePad as it bound to the textarea

>5. Creating an account then starting the program causes it to close

>6. Error Message received if change of background with no 'Files' folder

>7. Login stays open even when logged in

>8. Entering Text into KeyGenerator crashes the program

>9. File Explorer doesn't show the files created by cryptoPad

>10. Program Crashes if PictureViewer if it is open but with no files to display

>11. PictureViewer doesn't show filename of opened image when opened from startmenu

>12. Drag and drop mulitple images at once doesn't work

>13. Explorer Details pane doesn't show real filename

>14. Background FileChooser lets you choose non-image files

The GUI uses the systems look and feel and has been tested on Linux and Windows.

Current Size: ~11,000 lines, ~55 classes

Version: 7.4.0

>Copyright � 2015 � 2016 Martin Birch
>
>I grant permission for the code to be downloaded and compiled into a program, however, I take no responsibility for the code and what you use it for. The code maybe modified, however, I ask that you contact me first at 'martin@martin-birch.co.uk'. I do not give permission for any person to reuse this code for commercial distribution or in any application they intend to sell.
>
>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
>
>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
