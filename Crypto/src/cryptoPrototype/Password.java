package cryptoPrototype;

/**
 * Used to specify standard methods for items with a password
 * @author Martin Birch
 */
public interface Password {
	/**
	 * Gets the username
	 * @return char[] containing the username
	 */
    char[] getUser();
    /**
     * Gets the password
     * @return char[] containing the password
     */
    char[] getPass();
    /**
     * Sets the Username
     * @param user char[] new username
     */
    void setUser(char[] user);
    /**
     * Sets the Password
     * @param pass char[] new password
     */
    void setPass(char[] pass);
    /**
     * Used to change the password if there is an existing username and password
     * @param user char[] new username
     * @param pass char[] new password
     */
    void changePassword(char[] user, char[] pass);
}
