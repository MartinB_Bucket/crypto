package cryptoPrototype;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Settings implements WindowListener, PropertyChangeListener {
	private JFrame settingsWindow;
	private PropertyChangeSupport pSupport;
	
	public Settings(PropertyChangeListener listener) {
		pSupport = new PropertyChangeSupport(this);
		pSupport.addPropertyChangeListener(listener);
		createGUI();
	}
	
	public void setVisible(boolean isVisible) {
		settingsWindow.setVisible(isVisible);
	}
	
	private void createGUI() {
		settingsWindow = new JFrame("Settings");
    	settingsWindow.setLayout(new FlowLayout(FlowLayout.CENTER));

    	JPanel pnlSettings = new JPanel(new FlowLayout(FlowLayout.CENTER));
    	
    	JPanel pnlOptions = new JPanel();
    	pnlOptions.setLayout(new GridLayout(3,1));
    	
    	JButton btnChangePassword = new JButton("Change Password");
    	PropertyChangeListener listener = this;
    	btnChangePassword.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				settingsWindow.setVisible(false);
				pSupport.firePropertyChange("InvokeChangePassword", false, listener);
			}
    	});
    	
    	JButton btnSetDefaultImage = new JButton("Background Image");
    	btnSetDefaultImage.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setDialogTitle("Please Select an Image:");
				try {
					fileChooser.setCurrentDirectory(FilePaths.ROOT_DIRECTORY.toRealPath().toFile());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				int status = fileChooser.showOpenDialog(settingsWindow);
				if (status == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					if (selectedFile != null) {
						Path pFile = selectedFile.toPath();
						pSupport.firePropertyChange("InvokeChangeBackground", false, pFile);
					}
				}
			}
    	});
    	
    	JButton btnSyncAccounts = new JButton("Sync Accounts");
    	btnSyncAccounts.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				settingsWindow.setVisible(false);
				pSupport.firePropertyChange("InvokeChangeSyncAccounts", false, listener);
			}
    	});
    	
    	pnlOptions.add(btnChangePassword);
    	pnlOptions.add(btnSetDefaultImage);
    	pnlOptions.add(btnSyncAccounts);
    	
    	pnlSettings.add(pnlOptions);
    	settingsWindow.add(pnlSettings);
    	pnlSettings.setPreferredSize(new Dimension(200, 120));
    	settingsWindow.setSize(200, 140);
    	settingsWindow.setLocationRelativeTo(null);
    	settingsWindow.setResizable(false);
    	settingsWindow.addWindowListener(this);
    	settingsWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	@Override
	public void windowOpened(WindowEvent e) { }

	@Override
	public void windowClosing(WindowEvent e) {
		pSupport.firePropertyChange("WindowClosed", false, true);
	}

	@Override
	public void windowClosed(WindowEvent e) { }

	@Override
	public void windowIconified(WindowEvent e) { }

	@Override
	public void windowDeiconified(WindowEvent e) { }

	@Override
	public void windowActivated(WindowEvent e) { }

	@Override
	public void windowDeactivated(WindowEvent e) {	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals("WindowClosed")) {
			setVisible(true);
		}
	}
}
