package cryptoPrototype.syncManager;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Record implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5752678430465079285L;
	
	public static final int USERNAME = 0;
	public static final int PASSWORD = 1;
	public static final int SECURITY_QUESTION = 2;
	public static final int EXTRA_DETAILS = 3;
	public static final int TITLE = 4;
	
	public static final int STATE_MODIFIED = 7;
	public static final int STATE_REMOVED = 8;
	public static final int STATE_NEW = 9;
	
	
	private LocalDateTime time;
	private int state;
	private int itemChanged;
	private int accountID;
	private String accountTitle;
	
	public Record(int accountID, String title, int state, LocalDateTime time, int itemModified) {
		this.accountID = accountID;
		accountTitle = title;
		this.state = state;
		this.time = time;
		itemChanged = itemModified;
	}
	
	public void setTitle(String title) {
		accountTitle = title;
	}
	
	public String getTitle() {
		return accountTitle;
	}
	
	public int getItemModified() {
		return itemChanged;
	}
	
	/**
	 * @return the time
	 */
	public LocalDateTime getTime() {
		return time;
	}
	/**
	 * @return the state
	 */
	public int getState() {
		return state;
	}
	/**
	 * @return the accountID
	 */
	public int getAccountID() {
		return accountID;
	}
	
	public boolean equals(Record other) {
		return time.equals(other.time) && state == other.state && accountID == other.accountID;
	}
	
	public int compareTo(Record other) {
		return time.compareTo(other.time);
	}
}
