package cryptoPrototype.syncManager;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;

import cryptoPrototype.Encryption;
import cryptoPrototype.FilePaths;

public class RecordKeeper implements FilePaths {
	private File recordBase;
	private Encryption encrypt;
	private ArrayList<Record> log;
	private int current;
	private boolean fileOpen;
	
	public RecordKeeper(Encryption encrypt) {
		this.encrypt = encrypt;
		current = 0;
		recordBase = RECORD_LOG;
		log = new ArrayList<Record>();
		fileOpen = false;
	}
	
	public RecordKeeper(Encryption encrypt, File database) {
		this.encrypt = encrypt;
		current = 0;
		recordBase = database;
		log = new ArrayList<Record>();
		fileOpen = false;
	}
	
	public void createEntry(int accountID, String title, int state, LocalDateTime currentTime, int itemModified) {
		log.add(new Record(accountID, title, state, currentTime, itemModified));
	}

	@SuppressWarnings("unused")
	private class AppendableOOS extends ObjectOutputStream {

		public AppendableOOS(OutputStream out) throws IOException {
			super(out);
		}
    	
		protected void writeStreamHeader() throws IOException {
			reset();
		}
    }

	public void openLog() {
		if (!fileOpen) {
			try {
				FileInputStream inStream = new FileInputStream(recordBase);
				ByteArrayOutputStream outStream = new ByteArrayOutputStream();
				encrypt.decryptStream(inStream, outStream);
				inStream.close();
				ObjectInputStream objectIn = new ObjectInputStream(new ByteArrayInputStream(outStream.toByteArray()));
				
				Record entry = (Record) objectIn.readObject();
				while (entry != null) {
					log.add(entry);
					entry = (Record) objectIn.readObject();
				}
				
			} catch (IOException e) {
				//e.printStackTrace();
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			} catch (InvalidAlgorithmParameterException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			fileOpen = true;
		}
	}
	
	public void closeLog() {
		if (fileOpen) {
			if (log != null) {
				ByteArrayOutputStream bStream = new ByteArrayOutputStream();
				try {
					ObjectOutputStream outStream = new ObjectOutputStream(bStream);
					try {
						for (Record rc : log) {
							outStream.writeObject(rc);
						}
						outStream.writeObject(null);
					} catch (IOException e) {
						//Leave IOException
					} finally {
						outStream.flush();
						outStream.close();
					}
					try {
						encrypt.encryptBytes(bStream.toByteArray(), recordBase);
					} catch (InvalidKeyException e) {
						e.printStackTrace();
					}
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
			fileOpen = false;
		}
    }
	
	public Record removeEntry(Record record) {
		for (Record rc : log) {
			if (rc.equals(record)) {
				log.remove(rc);
				++current;
				return rc;
			}
		}
		return null;
	}
	
	public Record getEntry() {
		if (current < log.size()) {
			return log.get(current);
		} else {
			return null;
		}
	}
	
	public Record next() {
		++current;
		return getEntry();
	}
	
	//For accounts that are removed, also remove the log statements apart from remove
	//Check if any account names changed after they were inserted
	public void simplifyLog() {
		// Gather all New Records
		//Scan for modifications to new records
		ArrayList<Record> newRecords = new ArrayList<Record>();
		
		for (Record rc : log) {
			if (rc.getState() == Record.STATE_NEW) {
				newRecords.add(rc);
			}
		}
		
		//Ensure the title is the most up to date
		for (Record other : newRecords) {
			for (Record rc: log) {
				if (other.equals(rc) && rc.getState() == Record.STATE_MODIFIED) {
					if ((rc.compareTo(other) > 0) && rc.getItemModified() == Record.TITLE) {
						other.setTitle(rc.getTitle());
					}
				}
			}
		}
		
		log.removeIf( (Record rc) -> {
			for (Record other : newRecords) {
				if (rc.equals(other) && rc.getState() == Record.STATE_MODIFIED) {
					return true;
				}
			}
			return false;
		});
		
		// Gather All Deletion Records
		ArrayList<Record> deletedRecords = new ArrayList<Record>();
		
		for (Record rc : log) {
			if (rc.getState() == Record.STATE_REMOVED) {
				deletedRecords.add(rc);
			}
		}
		
		log.removeIf( (Record rc) -> {
			for (Record other : deletedRecords) {
				if (rc.equals(other)  && rc.getState() != Record.STATE_REMOVED) {
					return true;
				}
			}
			return false;
		});
		
	}
	
	public void sort() {
		Collections.sort(log, (rec1, rec2) -> rec1.compareTo(rec2));
	}
	
	public Record checkForLaterUpdate(Record other) {
		for (Record rc : log) {
			if (rc.getAccountID() == other.getAccountID() &&
					other.getItemModified() == rc.getItemModified()) {
				if (rc.compareTo(other) > 0) {
					return rc;
				}
			}
		}
		return null;
	}
}
