package cryptoPrototype.syncManager;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import cryptoPrototype.Encryption;
import cryptoPrototype.FilePaths;
import cryptoPrototype.keyVault.model.AccountDetails;
import cryptoPrototype.keyVault.model.IntegrityException;
import cryptoPrototype.keyVault.model.KeyVault;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.util.Callback;

public class SyncManager implements FilePaths {
	private static final int MODE_INSERTION = 0;
	private static final int MODE_DELETION = 1;
	
	private File otherDatabase;
	private final Encryption encrypt;
	private KeyVault currentKeyVault;
	private KeyVault otherKeyVault;
	private ArrayList<AccountDetails> currentAccounts;
	private ArrayList<AccountDetails> otherAccounts;
	
	private ListView<AccountDetails> listViewLeft;
	private ListView<AccountDetails> listViewRight;
	private HashMap<Integer, ArrayList<AccountDetails>> changesToMake;
	private int mode;
	
	private PropertyChangeSupport pSupport;
	
	private JFrame window;
	private JFXPanel fxPanel;
		
	public SyncManager(Encryption encrypt) {
		this.encrypt = encrypt; //Encryption encrypt 
		mode = MODE_INSERTION;
		pSupport = new PropertyChangeSupport(this);
		fxPanel =  new JFXPanel();
		createJFrame();
	}
	
	public void addPropertyChangeListener(PropertyChangeListener pListener) {
		pSupport.addPropertyChangeListener("WindowClosed", pListener);
	}
	
	private void createJFrame() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				window = new JFrame();
				window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				window.setTitle("Crypto");
				window.addWindowListener(new WindowListener() {
					@Override
					public void windowOpened(java.awt.event.WindowEvent e) {}
					
					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								pSupport.firePropertyChange("WindowClosed", false, true);								
							}
						});
					}
					
					@Override
					public void windowClosed(java.awt.event.WindowEvent e) {}
					@Override
					public void windowIconified(java.awt.event.WindowEvent e) {}
					@Override
					public void windowDeiconified(java.awt.event.WindowEvent e) {}
					@Override
					public void windowActivated(java.awt.event.WindowEvent e) {}
					@Override
					public void windowDeactivated(java.awt.event.WindowEvent e) {}
				});
				
				window.add(fxPanel);
				window.setSize(600, 350);
				window.revalidate();
				window.repaint();
				window.setLocationRelativeTo(null);
			}
		});
        createFileSelectionScreen(fxPanel);
	}
	
	private void createFileSelectionScreen(JFXPanel fxPanel) {
		BorderPane bPane = new BorderPane();
		VBox vCenter = new VBox();
		vCenter.setSpacing(10);
		vCenter.setAlignment(Pos.CENTER);

		Text txtHeader = new Text("Sync Accounts\n");

		Text txtNotification = new Text(
				"This will cause any key modifications in the "
						+ "other program to be synced to the accounts stored in this program. "
						+ "You will be prompted to confirm to add an account if it conflicts with an existing one. "
						+ "Likewise, you will need to confirm all deletions as well. "
						+ "Simple modifications are automatically updated after you press 'Next'. "
						+ "Changes made cannot be undone!\n");

		txtNotification.setTextAlignment(TextAlignment.JUSTIFY);
		txtNotification.setWrappingWidth(400);

		Text txtInput = new Text("Please select the other Crypto Program \n you would like to sync Accounts with:");
		FileChooser fChooser = new FileChooser();
		fChooser.setTitle("Select Another Crypto program:");
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JAR files (*.jar)", "*.jar");
		fChooser.getExtensionFilters().add(extFilter);

		Button btnSelectFile = new Button("Select Other Program");

		vCenter.getChildren().addAll(txtHeader, txtNotification, txtInput, btnSelectFile);

		HBox hBottom = new HBox();
		Button btnExit = new Button("<- Exit");
		btnExit.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
					}
				});
			}
		});

		Button btnNext = new Button("Next ->");
		btnNext.setDisable(true);
		btnNext.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				changesToMake = sync();
				createScreen(MODE_INSERTION, changesToMake);
			}			
		});

		btnSelectFile.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				File cryptoFile = fChooser.showOpenDialog(null);
				Path toDataBase = cryptoFile.toPath().getParent().resolve(DATABASE_VAULT.getName());

				if (Files.exists(toDataBase)) {
					otherDatabase = toDataBase.toFile();
					currentKeyVault = new KeyVault(DATABASE_VAULT, RECORD_LOG, encrypt);
					otherKeyVault = new KeyVault(otherDatabase, otherDatabase.toPath().getParent().resolve(RECORD_LOG.getName()).toFile(), encrypt);
					try {
						currentAccounts = currentKeyVault.loadKeys();
						otherAccounts = otherKeyVault.loadKeys();	
						btnNext.setDisable(false);
					} catch (InvalidKeyException e) {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {	
								JOptionPane.showMessageDialog(window, "Incompatible Password", 
										"Error Opening File", JOptionPane.ERROR_MESSAGE);		
							}
						});
						btnNext.setDisable(true);
						e.printStackTrace();
					} catch (InvalidAlgorithmParameterException e) {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {								
								JOptionPane.showMessageDialog(window, "Incompatible File", 
										"Error Opening File", JOptionPane.ERROR_MESSAGE);	
							}
						});
						btnNext.setDisable(true);
						e.printStackTrace();
					} catch (FileNotFoundException e) {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {								
								JOptionPane.showMessageDialog(window, "No Account Database Found", 
										"Error Opening File", JOptionPane.ERROR_MESSAGE);	
							}
						});
						btnNext.setDisable(true);
						e.printStackTrace();
					} catch (IntegrityException e) {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								JOptionPane.showMessageDialog(window, "Corrupted File", 
										"Error Opening File", JOptionPane.ERROR_MESSAGE);									
							}
						});

						btnNext.setDisable(true);
						e.printStackTrace();
					}
				}
			}
		});

		Region spacer = new Region();
		HBox.setHgrow(spacer, Priority.ALWAYS);
		hBottom.getChildren().addAll(btnExit, spacer, btnNext);

		bPane.setCenter(vCenter);
		bPane.setBottom(hBottom);
		Scene scene = new Scene(bPane, 600, 300);
		scene.getStylesheets().add(SyncManager.class.getResource("syncGUI.css").toExternalForm());
		int count = 0;
		while (fxPanel == null) {
			try {
				System.out.println("Sleeping: " + count);
				++count;
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}					
		}
		fxPanel.setScene(scene);
		//window.removeAll();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				window.setVisible(true);
			}
		});
	}
	
	private void createScreen(final int mode, final HashMap<Integer, ArrayList<AccountDetails>> map) {
		BorderPane bPane = new BorderPane();
		bPane.setTop(createHeading(mode));
		bPane.setLeft(createLeftSide(mode, map));
		bPane.setRight(createRightSide(mode, map));
		bPane.setCenter(createCenterSide());
		bPane.setBottom(createBottomSide());
		Scene scene = new Scene(bPane, 850, 450);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				window.setSize(850, 450);
			}
		});
		scene.getStylesheets().add(SyncManager.class.getResource("syncGUI.css").toExternalForm());
		fxPanel.setScene(scene);
	}
	
	private static HBox createHeading(final int mode) {
		HBox header = new HBox();
		header.setSpacing(0);
		VBox headingItems = new VBox();
		headingItems.setAlignment(Pos.CENTER);
		headingItems.setSpacing(10);
		Text lblHeading = new Text("Sync Manager");
		lblHeading.setFont(Font.font(lblHeading.getFont().getFamily(), 
							FontWeight.BOLD, lblHeading.getFont().getSize() + 5));
		//lblHeading.setFont(Font.font("Arial", FontWeight.BOLD, 20));
		Text lblSubHeading;
		if (mode == MODE_INSERTION) {
			lblSubHeading = new Text("Add Items To Be Added");
		} else {
			lblSubHeading = new Text("Add Items To Be Deleted");
		}
		headingItems.getChildren().add(lblHeading);
		headingItems.getChildren().add(lblSubHeading);
		headingItems.getChildren().add(new Text("Adding an Item accross that has the same name as an Account in the conflicts list will cause it to be overridden\n"));
		header.getChildren().add(headingItems);
		header.setAlignment(Pos.CENTER);
		return header;
	}
	
	private HBox createLeftSide(final int  mode, final HashMap<Integer, ArrayList<AccountDetails>> accounts) {
		HBox hLeftSide = new HBox();
		VBox vDetailsDisplay = new VBox();
		vDetailsDisplay.setMaxSize(150, 5000);
		VBox vAccountList = new VBox();
		vAccountList.setAlignment(Pos.CENTER);
		vAccountList.setMaxSize(200, 5000);

		Text txtID = new Text("Account ID:");
		TextField txtfID = new TextField();
		Text txtTitle = new Text("Title:");
		TextField txtfTitle = new TextField();
		Text txtUsername = new Text("Username:");
		TextField txtfUsername = new TextField(); 
		Text txtPassword = new Text("Password:");
		TextField txtfPassword = new TextField(); 
		Text txtSecurityQ = new Text("Security Q:");
		TextField txtfSecurityQ = new TextField(); 
		Text txtExtraDetails = new Text("Extra Details");
		TextArea txtfExtraDetails = new TextArea(); 

		txtfID.setDisable(true);
		txtfTitle.setDisable(true);
		txtfUsername.setDisable(true);
		txtfPassword.setDisable(true);
		txtfSecurityQ.setDisable(true);
		txtfExtraDetails.setDisable(true);
		
		vDetailsDisplay.getChildren().addAll(txtID, txtfID, txtTitle, txtfTitle, txtUsername, 
				txtfUsername, txtPassword, txtfPassword, txtSecurityQ, 
				txtfSecurityQ, txtExtraDetails, txtfExtraDetails);
		
		Text txtAccountListTitle = new Text("Account List:");
		ObservableList<AccountDetails> items = FXCollections.observableArrayList(getAccountsToShow(mode, 0, accounts));
		listViewLeft = new ListView<AccountDetails>(items);
		listViewLeft.setCellFactory(new Callback<ListView<AccountDetails>, ListCell<AccountDetails>>() {
			@Override
			public ListCell<AccountDetails> call(ListView<AccountDetails> param) {
				return new ListCell<AccountDetails>() {
					@Override
					public void updateItem(AccountDetails account, boolean empty) {
						super.updateItem(account, empty);
						if (account != null) {
							setText(String.valueOf(account.getTitle()));
						}
						if (empty) {
							setText(null);
							setGraphic(null);
						}
					}					
				};
			}
		});
		listViewLeft.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<AccountDetails>() {
			@Override
			public void changed(ObservableValue<? extends AccountDetails> observable, AccountDetails oldValue,
					AccountDetails newValue) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						if (newValue != null) {
							txtfID.setText(String.valueOf(newValue.getID()));
							txtfTitle.setText(String.valueOf(newValue.getTitle()));
							txtfUsername.setText(String.valueOf(newValue.getUsername()));
							txtfPassword.setText(String.valueOf(newValue.getPassword()));
							txtfSecurityQ.setText(String.valueOf(newValue.getSecurityQuestion()));
							txtfExtraDetails.setText(String.valueOf(newValue.getExtraDetails()));
						} else {
							txtfID.setText("");
							txtfTitle.setText("");
							txtfUsername.setText("");
							txtfPassword.setText("");
							txtfSecurityQ.setText("");
							txtfExtraDetails.setText("");
						}
					}
				});
			}
		});
		
		Text txtLeftSubHeading = new Text("Other Accounts");
		vAccountList.getChildren().add(txtAccountListTitle);
		vAccountList.getChildren().add(listViewLeft);
		vAccountList.getChildren().add(txtLeftSubHeading);
		
		hLeftSide.getChildren().addAll(vDetailsDisplay, vAccountList);
		
		return hLeftSide;
	}
	
	private HBox createRightSide(int  mode, HashMap<Integer, ArrayList<AccountDetails>> accounts) {
		HBox hRightSide = new HBox();
		VBox vDetailsDisplay = new VBox();
		vDetailsDisplay.setMaxSize(150, 5000);
		VBox vAccountList = new VBox();
		vAccountList.setAlignment(Pos.CENTER);
		vAccountList.setMaxSize(200, 5000);

		Text txtID = new Text("Account ID:");
		TextField txtfID = new TextField();
		Text txtTitle = new Text("Title:");
		TextField txtfTitle = new TextField();
		Text txtUsername = new Text("Username:");
		TextField txtfUsername = new TextField(); 
		Text txtPassword = new Text("Password:");
		TextField txtfPassword = new TextField(); 
		Text txtSecurityQ = new Text("Security Q:");
		TextField txtfSecurityQ = new TextField(); 
		Text txtExtraDetails = new Text("Extra Details");
		TextArea txtfExtraDetails = new TextArea(); 

		txtfID.setDisable(true);
		txtfTitle.setDisable(true);
		txtfUsername.setDisable(true);
		txtfPassword.setDisable(true);
		txtfSecurityQ.setDisable(true);
		txtfExtraDetails.setDisable(true);
		
		vDetailsDisplay.getChildren().addAll(txtID, txtfID, txtTitle, txtfTitle, txtUsername, 
				txtfUsername, txtPassword, txtfPassword, txtSecurityQ, 
				txtfSecurityQ, txtExtraDetails, txtfExtraDetails);
		
		Text txtAccountListTitle = new Text("Account List:");
		ObservableList<AccountDetails> items = FXCollections.observableArrayList(getAccountsToShow(mode, 1, accounts));
		listViewRight = new ListView<AccountDetails>(items);
		listViewRight.setCellFactory(new Callback<ListView<AccountDetails>, ListCell<AccountDetails>>() {
			@Override
			public ListCell<AccountDetails> call(ListView<AccountDetails> param) {
				return new ListCell<AccountDetails>() {
					@Override
					public void updateItem(AccountDetails account, boolean empty) {
						super.updateItem(account, empty);
						if (account != null) {
							setText(String.valueOf(account.getTitle()));
						}
						if (empty) {
							setText(null);
							setGraphic(null);
						}
					}					
				};
			}
		});
		listViewRight.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<AccountDetails>() {
			@Override
			public void changed(ObservableValue<? extends AccountDetails> observable, AccountDetails oldValue,
					AccountDetails newValue) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						if (newValue != null) {
							txtfID.setText(String.valueOf(newValue.getID()));
							txtfTitle.setText(String.valueOf(newValue.getTitle()));
							txtfUsername.setText(String.valueOf(newValue.getUsername()));
							txtfPassword.setText(String.valueOf(newValue.getPassword()));
							txtfSecurityQ.setText(String.valueOf(newValue.getSecurityQuestion()));
							txtfExtraDetails.setText(String.valueOf(newValue.getExtraDetails()));
						} else {
							txtfID.setText("");
							txtfTitle.setText("");
							txtfUsername.setText("");
							txtfPassword.setText("");
							txtfSecurityQ.setText("");
							txtfExtraDetails.setText("");
						}
					}
				});
			}
		});
		
		Text txtRightSubHeading = new Text("Conflicts");
		vAccountList.getChildren().add(txtAccountListTitle);
		vAccountList.getChildren().add(listViewRight);
		vAccountList.getChildren().add(txtRightSubHeading);
		
		hRightSide.getChildren().addAll(vAccountList, vDetailsDisplay);
		
		return hRightSide;
	}
	
	private VBox createCenterSide() {
		VBox vButtons = new VBox();
		vButtons.setSpacing(10);
		vButtons.setAlignment(Pos.CENTER);
		
		Button btnAdd = new Button("Add >");
		btnAdd.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						AccountDetails account = listViewLeft.getSelectionModel().getSelectedItem();
						if (account != null) {
							listViewLeft.getItems().remove(account);
							
							listViewRight.getItems().removeIf( (AccountDetails conflict) -> {
								return Arrays.equals(conflict.getTitle(), account.getTitle());
							});
							
							listViewRight.getItems().add(account);					
						}						
					}
				});
			}
		});
		
		Button btnRemove = new Button("< Remove");
		btnRemove.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						AccountDetails account = listViewRight.getSelectionModel().getSelectedItem();
						if (changesToMake.get(mode).contains(account)) {
							if (account != null) {
								listViewRight.getItems().remove(account);
								for (AccountDetails conflict : currentAccounts) {
									if (Arrays.equals(conflict.getTitle(), account.getTitle())) {
										listViewRight.getItems().add(conflict);	
									}
								}
								listViewLeft.getItems().add(account);					
							}													
						}
					}
				});
			}
		});
		
		Button btnReset = new Button("Reset");
		
		vButtons.getChildren().addAll(btnAdd, btnRemove, btnReset);
		
		return vButtons;
	}
	
	private void removeAccountDuplicates(final AccountDetails account) {
		for (AccountDetails accountToRemove : currentAccounts) {
			if (Arrays.equals(account.getTitle(), accountToRemove.getTitle())) {
				RecordKeeper rk = accountToRemove.getRecordKeeper();
				rk.openLog();
				rk.createEntry(accountToRemove.getID(), String.valueOf(accountToRemove.getTitle()), 
						Record.STATE_REMOVED, LocalDateTime.now(), 0);
				currentAccounts.remove(accountToRemove);
				break;
			}
		}
	}
	
	private void addAccount(final AccountDetails account) {
		RecordKeeper rk;
		if (currentAccounts.size() > 0) {
			rk = currentAccounts.get(0).getRecordKeeper();
		} else {
			rk  = new RecordKeeper(encrypt);
		}
		rk.openLog();

		AccountDetails newAccount = new AccountDetails(account.getPassword(), account.getUsername(), 
				account.getTitle(), account.getSecurityQuestion(), account.getDate(), account.getExtraDetails(), encrypt);

		rk.createEntry(newAccount.getID(), String.valueOf(newAccount.getTitle()), Record.STATE_NEW, newAccount.getDate(), 0);
		newAccount.addRecordKeeper(rk);
		currentAccounts.add(newAccount);
	}
	
	private HBox createBottomSide() {
		HBox hBottom = new HBox();
		hBottom.setId("BottomBox");
		HBox hLeftSide = new HBox();
		hLeftSide.setAlignment(Pos.BOTTOM_LEFT);
		HBox hRightSide = new HBox();
		hRightSide.setAlignment(Pos.BOTTOM_RIGHT);
		
		Button btnExit = new Button("<- Exit");
		btnExit.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				SwingUtilities.invokeLater(new Runnable() {
				    @Override
				    public void run() {
				    	window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
				    }
				});
			}
		});

		Button btnNext = new Button("Next ->");

		btnNext.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						if (mode == MODE_INSERTION) {
							ObservableList<AccountDetails> toAdd = listViewRight.getItems();
							for (AccountDetails account : toAdd) {
								removeAccountDuplicates(account);
							}

							for (AccountDetails account : toAdd) {
								addAccount(account);
							}

							mode = MODE_DELETION;
							createScreen(mode, changesToMake);
						} else if (mode == MODE_DELETION) {
							ObservableList<AccountDetails> toRemove = listViewRight.getItems();
							for (AccountDetails account : toRemove) {
								for (AccountDetails accountToRemove : currentAccounts) {
									if (Arrays.equals(account.getTitle(), accountToRemove.getTitle())) {
										RecordKeeper rk = accountToRemove.getRecordKeeper();
										rk.openLog();
										rk.createEntry(accountToRemove.getID(), String.valueOf(accountToRemove.getTitle()), 
												Record.STATE_REMOVED, LocalDateTime.now(), 0);
										currentAccounts.remove(accountToRemove);
										break;
									}
								}
							}
							// Close and finish
							try {
								currentKeyVault.writeKeys(currentAccounts);
							} catch (IOException e1) {
								e1.printStackTrace();
							}
							currentKeyVault.closeVault(currentAccounts);
							
							//Copy database and replace the other one 
							otherDatabase.delete();
							try {
								Files.copy(DATABASE_VAULT.toPath(), otherDatabase.toPath(), StandardCopyOption.REPLACE_EXISTING);
							} catch (IOException e) {
								e.printStackTrace();
							}
							System.out.println("Completed Successfully");
							//Close and go back
							//pSupport.firePropertyChange("WindowClosed", false, true);
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));							
								}
							});
						}
					}
				});
			}
		});
		
		Region spacer = new Region();
		HBox.setHgrow(spacer, Priority.ALWAYS);
		hLeftSide.getChildren().add(btnExit);
		hRightSide.getChildren().add(btnNext);
		hBottom.getChildren().addAll(hLeftSide, spacer, hRightSide);
		
		return hBottom;
	}
	
	public ArrayList<AccountDetails> getAccountsToShow(int mode, int side, 
			HashMap<Integer, ArrayList<AccountDetails>>  accounts) {

		if (side == 0) {
			return accounts.get(mode);
		} else {
			ArrayList<AccountDetails> list = accounts.get(mode);
			ArrayList<AccountDetails> conflicts = new ArrayList<AccountDetails>();
			for (AccountDetails account : list) {
				for (AccountDetails accountConflict : currentAccounts) {
					if (Arrays.equals(account.getTitle(), accountConflict.getTitle())) {
						conflicts.add(accountConflict);
					}
				}
			}
			return conflicts;
		}			
		
	}
	
	public HashMap<Integer, ArrayList<AccountDetails>> sync() {
		RecordKeeper currentRecords = new RecordKeeper(encrypt);
		RecordKeeper otherRecords = new RecordKeeper(encrypt, new File("TestProgram/syncData.db"));
		currentRecords.openLog();
		otherRecords.openLog();

		currentRecords.simplifyLog();
		currentRecords.sort();
		otherRecords.simplifyLog();
		otherRecords.sort();
		
		ArrayList<AccountDetails> newAccounts = null;
		ArrayList<AccountDetails> deleteAccounts = null;

		newAccounts = new ArrayList<AccountDetails>();
		deleteAccounts = new ArrayList<AccountDetails>();

		Record record = otherRecords.getEntry();

		char deleted[] = new char[] {'D','a','t','a',' ','D','e','l','e','t','e','d'};
		while (record != null) {
			if(record.getState() == Record.STATE_NEW) {
				for (AccountDetails ac : otherAccounts) {
					if (ac.getID() == record.getAccountID()) {
						newAccounts.add(ac);	
						break;
					}
				}
			} else if (record.getState() == Record.STATE_REMOVED) {
				for (AccountDetails ac : currentAccounts) {
					if (ac.getID() == record.getAccountID()) {
						deleteAccounts.add(new AccountDetails(deleted, deleted, record.getTitle().toCharArray(), deleted, record.getTime(), deleted, encrypt));
					}
				}
			} else {
				Record laterUpdateCurrent = currentRecords.checkForLaterUpdate(record);
				Record laterUpdateOther = otherRecords.checkForLaterUpdate(record);

				if (laterUpdateOther != null) {
					if (laterUpdateCurrent != null) {
						if (laterUpdateCurrent.compareTo(laterUpdateOther) < 0) {
							for (AccountDetails otherAccount : otherAccounts) {
								if (otherAccount.getID() == record.getAccountID()) {
									for (AccountDetails currentAccount : currentAccounts) {
										if (currentAccount.getID() == record.getAccountID()) {
											if (record.getItemModified() == Record.USERNAME) {
												currentAccount.addUsername(otherAccount.getUsername());
											} else if (record.getItemModified() == Record.PASSWORD) {
												currentAccount.addPassword(otherAccount.getPassword());
											} else if (record.getItemModified() == Record.TITLE) {
												currentAccount.addTitle(otherAccount.getTitle());
											} else if (record.getItemModified() == Record.EXTRA_DETAILS) {
												currentAccount.addExtraDetails(otherAccount.getExtraDetails());
											} else if (record.getItemModified() == Record.SECURITY_QUESTION) {
												currentAccount.addSecurityQuestion(otherAccount.getSecurityQuestion());
											}
										}
									}
								}
							}
						}						
					}
				} else {
					for (AccountDetails otherAccount : otherAccounts) {
						if (otherAccount.getID() == record.getAccountID()) {
							for (AccountDetails currentAccount : currentAccounts) {
								if (currentAccount.getID() == record.getAccountID()) {
									if (record.getItemModified() == Record.USERNAME) {
										currentAccount.addUsername(otherAccount.getUsername());
									} else if (record.getItemModified() == Record.PASSWORD) {
										currentAccount.addPassword(otherAccount.getPassword());
									} else if (record.getItemModified() == Record.TITLE) {
										currentAccount.addTitle(otherAccount.getTitle());
									} else if (record.getItemModified() == Record.EXTRA_DETAILS) {
										currentAccount.addExtraDetails(otherAccount.getExtraDetails());
									} else if (record.getItemModified() == Record.SECURITY_QUESTION) {
										currentAccount.addSecurityQuestion(otherAccount.getSecurityQuestion());
									}
								}
							}
						}
					}
				}
			}
			record = otherRecords.next();
		}

		currentRecords.closeLog();
		otherRecords.closeLog();

		HashMap<Integer, ArrayList<AccountDetails>> maps = new HashMap<Integer, ArrayList<AccountDetails>>();
		maps.put(MODE_INSERTION, newAccounts);
		maps.put(MODE_DELETION, deleteAccounts);
		return maps;
	}

}
