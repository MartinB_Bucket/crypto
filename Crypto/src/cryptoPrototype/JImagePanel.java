package cryptoPrototype;

import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

/**
 * Used to create a centered Image in the middle of a JPanel
 * @author Martin Birch
 */
public class JImagePanel extends JPanel {
	private static final long serialVersionUID = -461456648432546856L;
	
	private BufferedImage background;
	
	/**
	 * Create a new buffered JPanel with the specified layout manager and background Image
	 * @param layout  the LayoutManager to use
	 * @param img background Image to use
	 */
	public JImagePanel(LayoutManager layout, BufferedImage img) {
		super(layout, true);
		background = img;
	}

	/**
	 * Creates a new <code>JPanel</code> with a double buffer, a flow layout
	 * and a background Image
	 */
	public JImagePanel(BufferedImage img) {
		background = img;
	}
	
	/**
     * Create a new buffered JPanel with the specified layout manager
     *
     * @param layout  the LayoutManager to use
     */
    public JImagePanel(LayoutManager layout) {
        super(layout, true);
    }
	
    /**
     * Sets the Panels Background Image
     * @param img
     */
	public void setBackground(BufferedImage img) {
		background = img;
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (background.getWidth() < getWidth()) {
			if (background.getHeight() < getHeight()) {
				int x = (getWidth() - background.getWidth()) / 2;
				int y = (getHeight() - background.getHeight()) / 2;
				g.drawImage(background, x, y, background.getWidth(), background.getHeight(), null);
			} else {
				int x = (getWidth() - background.getWidth()) / 2;
				g.drawImage(background, x, 0, background.getWidth(), getHeight(), null);
			}
		} else {
			if (background.getHeight() < getHeight()) {
				int y = (getHeight() - background.getHeight()) / 2;
				g.drawImage(background, 0, y, getWidth(), background.getHeight(), null);
			} else {
				g.drawImage(background, 0, 0, getWidth(), getHeight(), null);
			}
		}
	}
}

