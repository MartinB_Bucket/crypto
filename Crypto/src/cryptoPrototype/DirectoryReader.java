package cryptoPrototype;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Used to create a generic way of reading a directory
 * @author Martin Birch
 */
public class DirectoryReader implements FilePaths {
	
	private DirectoryProcessor dirProcessor;
	private Encryption encryption;
	
	/**
	 * Class Constructor
	 * @param dirProcessor DirectoryProcessor methods will be called when processing either a file or directory
	 * @param encrypt Encryption object, only provide if file and folder names are hidden
	 */
	public DirectoryReader(DirectoryProcessor dirProcessor, Encryption encrypt) {
		this.dirProcessor = dirProcessor;
		this.encryption = encrypt;
	}
	
	/**
	 * Handles the reading and processing of the directory
	 * If the DirectoryProcessor return true for the method processDirectory, the sub directory will be read recursively 
	 * @param current Path of the directory to read
	 * @param flag boolean represents whether the files and folders names have been hidden
	 */
	public void readDirectory(Path current, boolean flag) {
		if (flag) {
			readEncryptedName(current);
		} else {
			readStandard(current);
		}
	}
	
	/**
	 * Reads a directory like it is a standard system directory, this method
	 * also processes all files in one directory before progressing onto folders.
	 * Additionally, the files and folders will be processed alphabetically.
	 * @param current Path of the directory to read
	 */
	private void readStandard(Path current) {
		ArrayList<Path> files = null;
		ArrayList<Path> folders = null;
		
		try (DirectoryStream<Path> entries = Files.newDirectoryStream(current))  {
			files = new ArrayList<Path>();
			folders = new ArrayList<Path>();
			for (Path entry : entries) { 
				if (Files.isDirectory(entry)) {
					folders.add(entry);
				} else {
					files.add(entry);
				}
			}
		} catch (IOException e) {
			
		}
		if (files != null) {
			Collections.sort(files);
			for (Path entry : files) {
				dirProcessor.processFile(entry, entry.getFileName().toString());
			}			
		}
		
		if (folders != null) {
			Collections.sort(folders);
			for (Path entry : folders) {
				if (dirProcessor.processDirectory(entry, entry.getFileName().toString())) {
					readDirectory(entry, false);
				}
			}			
		}
	}
	
	/**
	 * Configured to read the directory 'Files/' or any of it's sub directories.
	 * Using this method requires a valid Encryption object so the real file names can be 
	 * provided back to the user. This method also ensures that the files in a directory are processed 
	 * before the folders are.
	 * @param current Path of the directory to read.
	 */
	private void readEncryptedName(Path current) {
		HashMap<Integer, String> fileMap = new HashMap<Integer, String>();
		MasterFileEditor reader = new MasterFileEditor(encryption);
		
		try (DirectoryStream<Path> entries = Files.newDirectoryStream(current))  {
			for (Path entry : entries) {
				String strName = entry.getFileName().toString();
				if (strName.equals(MASTER_FILE)){
					fileMap = reader.processMasterFile(entry);
					break;
				}
			}
		} catch (IOException e) {

		}
		
		ArrayList<Path> files = null;
		ArrayList<Path> folders = null;
		
		try (DirectoryStream<Path> entries = Files.newDirectoryStream(current))  {
			files = new ArrayList<Path>();
			folders = new ArrayList<Path>();
			for (Path entry : entries) { 
				if (Files.isDirectory(entry)) {
					folders.add(entry);
				} else {
					files.add(entry);
				}
			}
		} catch (IOException e) {

		}
		
		if (files != null) {
			for (Path entry : files) {
				if ((!entry.getFileName().toString().contains(MASTER_FILE))){
					dirProcessor.processFile(entry, fileMap.get(Integer.valueOf(entry.getFileName().toString())));
				} else {
					dirProcessor.processFile(entry, "MASTER_FILE");
				}
			}			
		}
		
		if (folders != null) {
			for (Path entry : folders) {
				if (dirProcessor.processDirectory(entry, fileMap.get(Integer.valueOf(entry.getFileName().toString())))) {
					readDirectory(entry, true);
				}
			}			
		}
		
		reader.close();
	}


	/**
	 * Adds p3 onto p1, excluding part of p3 which is contained in p2.
	 * For example: <br>
	 * p1 = "C:\Users\Jack\Pictures", <br>
	 * p2 = "C:\Users\Jack\Pictures\Sample Pictures", <br>
	 * p3 = "C:\Users\Jack\Pictures\Sample Pictures\Images" <br>
	 * Result = "C:\Users\Jack\Pictures\Images"
	 * @param p1 A Path of a Directory
	 * @param p2 Part of p3 to remove
	 * @param p3 Path
	 * @return Path
	 */
	public static Path getNewPath(Path p1, Path p2, Path p3) {
		String filePath = null;
		Pattern pat = Pattern.compile(p2.getFileName().toString() + ".*");
		Matcher m = pat.matcher(p3.toString());
		if (m.find()) {
			filePath = m.group();
			pat = Pattern.compile("[^" + p2.getFileName().toString() + "]" + ".*");
			m = pat.matcher(filePath);
			if (m.find()) {
				filePath = m.group();
			}
		}
		return Paths.get(p1.toString() + filePath);
	}
}
