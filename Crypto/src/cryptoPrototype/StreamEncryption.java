package cryptoPrototype;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.LinkedList;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.DestroyFailedException;

/**
 * Handles the Encryption and Decryption of Streams.
 * @author Martin Birch
 */
public class StreamEncryption extends HashingSystem {
	private static final String CIPHER_ALGORITHM = "AES/CTR/NoPadding";
	/**
	 * Marker used to identify the end of the useful data. Superfluous bytes maybe present
	 * after this marker.
	 */
	private static final byte[] FILE_END = new byte[] {(byte) 76, (byte) 2, (byte) 81,
														(byte) 106, (byte) 6, (byte) 108,
														(byte) 89, (byte) 26, (byte) 13,
														(byte) 37};
	/**
	 * Used to identify how many of the marker bytes have been met so far.
	 * Should be reset to zero before decrypting data
	 */
	private int amountMet;
	
	public static final int KEYLENGTH_128 = 16;
	
	protected int readSize;
    protected SecretKey secretKey;
    
	/**
	 * Class Constructor
	 * @param letters char[] A password
	 */
    public StreamEncryption(char[] letters) {
        readSize = 2048;
        secretKey = new SecretKeySpec(hash(letters), "AES");
    }
    
    /**
     * Class Constructor
	 * @param letters char[] A password
     * @param readSize used to specify the size of bits to be read in and out at one time
     */
    public StreamEncryption(char[] letters, int readSize) {
        this.readSize = readSize;
        secretKey = new SecretKeySpec(hash(letters), "AES");
    }

    /**
     * Class Constructor
	 * @param letters char[] A password
	 * @param keylength The size of the key to use, 16 for 128bit, 24 for 192bit, and 32 for 256bit
     * @param readSize used to specify the size of bits to be read in and out at one time
     */
    public StreamEncryption(char[] letters, int keylength, int readSize) {
        this.readSize = readSize;
        secretKey = new SecretKeySpec(hash(letters,0,keylength), "AES");
    }
    
    /**
     * Changes the password that is used for the encryption
     * @param letters char[] A password
     */
    public void setKey(char[] letters) {
        try {
            secretKey.destroy();
            secretKey = new SecretKeySpec(hash(letters), "AES");
        } catch (DestroyFailedException ex) {
        	
        }
    }
    
    /**
     * Decrypts the bytes in the InputStream and sends the decrypted bytes to the OutputStream.
     * Can only decrypt data that was encrypted using the adjacent method.
     * @param inStream InputStream with Encrypted bytes 
     * @param outStream OutputStream for decrypted bytes to be sent to
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     */
    public void decryptStream(InputStream inStream, OutputStream outStream) throws InvalidKeyException, InvalidAlgorithmParameterException  {
    	if (inStream != null && outStream != null) {
    		CipherInputStream cStream = null;
        	try {
                Cipher c = Cipher.getInstance(CIPHER_ALGORITHM);
                
                byte[] b = new byte[16];   //Set An Array To the Size of the IV used in the Encryption
                int i = inStream.read(b);  //Read the IV from the file
                
                c.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(b));
                cStream = new CipherInputStream(inStream, c); //Cipher will decrypt and write bytes into output stream

                amountMet = 0;
                b = new byte[FILE_END.length];
                
                LinkedList<Byte> buffer = new LinkedList<Byte>();
                i = read(cStream, buffer, FILE_END.length);
                i = read(cStream, buffer, readSize);
                
                while (i > 0) {
                	for (int index = 0; index < i; ++index) {
                		outStream.write(buffer.removeFirst());
                	}
                	i = read(cStream, buffer, readSize);
                }

            } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            	e.printStackTrace();
			} finally {
            	if (cStream != null) {
            		try { cStream.close(); } catch (IOException e) { }
            	}
            }
    	}
    }

    /**
     * Encrypts the bytes read from the InputStream and sends the encrypted bytes to the OutputStream.
     * Data that has been encrypted using this method should only be decrypted using the adjacent method.
     * @param inStream InputStream containing data to encrypt
     * @param outStream OutputStream for encrypted data to be sent to
     * @throws InvalidKeyException
     */
    public void encryptStream(InputStream inStream, OutputStream outStream) throws InvalidKeyException {
    	if (inStream != null && outStream != null) {
    		CipherOutputStream cStream = null;
    		try {
    			SecureRandom secure = new SecureRandom();
    			Cipher c = Cipher.getInstance(CIPHER_ALGORITHM);
    			c.init(Cipher.ENCRYPT_MODE, secretKey, secure);
    			cStream = new CipherOutputStream(outStream, c);
    			
    			outStream.write(c.getIV());

    			byte[] b = new byte[readSize];
    			int i = inStream.read(b);
    			while (i != -1) {
    				cStream.write(b, 0, i);
    				i = inStream.read(b);
    			}
                cStream.write(FILE_END);
    			cStream.flush();
    		} catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException e) {
    			e.printStackTrace();
    		} finally {
    			if (cStream != null) {
    				try { cStream.close(); } catch (IOException e1) { }
    			}
    		}
    	}
    }
    
    /**
     * Changes a password placed on a encrypted stream, reads in the InputStream, decrypts it, and encrypts it using the new password.
     * Reads in a portion of the InputStream at a time, to prevent all the data being processed at one go.
     * <br> 
     * The Method verifyData must be called after using this method to ensure no superfluous characters where added during the process.
     * @param inStream InputStream containing the original encrypted data
     * @param outStream OutputStream for the updated encrypted data to go
     * @param newEncrypt Encryption containing the new Encryption password to use
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     */
    public void changeStreamPassword(InputStream inStream, OutputStream outStream, Encryption newEncrypt) throws InvalidKeyException, InvalidAlgorithmParameterException {
    	if (inStream != null && outStream != null) {
    		CipherOutputStream cOutStream1 = null;
    		CipherOutputStream cOutStream2 = null;
    		try {
    			Cipher cIn = Cipher.getInstance(CIPHER_ALGORITHM);
    			byte[] b = new byte[16];   //Set An Array To the Size of the IV used in the Encryption
                int i = inStream.read(b);  //Read the IV from the file
                cIn.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(b));
                
                SecureRandom secure = new SecureRandom();
                Cipher cOut = Cipher.getInstance(CIPHER_ALGORITHM);
                cOut.init(Cipher.ENCRYPT_MODE, newEncrypt.secretKey, secure);
                cOutStream2 = new CipherOutputStream(outStream, cOut);
                
                cOutStream1 = new CipherOutputStream(cOutStream2, cIn);
                
                outStream.write(cOut.getIV());

    			b = new byte[readSize];
    			i = inStream.read(b);
    			while (i != -1) {
    				cOutStream1.write(b, 0, b.length);
    				i = inStream.read(b);
    			}
    			cOutStream1.flush();
    		} catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException e) {
    			e.printStackTrace();
    		} finally {
    			if (cOutStream1 != null) {
    				try { cOutStream1.close(); } catch (IOException e1) { }
    			}
    		}
    	}
    }
    
    /**
     * This method should be used to read data in from a stream with encrypted data so the end of file marker can be identified.
     * The readSize is an integer amount the read method should try and read then add to the LinkedList (each byte is appended to the end of the list).
     * However the actual size of the data read is returned as an integer, 0 representing no data has been read.
     * @param in InputStream with encrypted data
     * @param buffer LinkedList<Byte> data is appended to the end of the list
     * @param readSize The bytes of data to try and add to the list
     * @return int amount of data actually read
     */
    private int read(InputStream in, LinkedList<Byte> buffer, int readSize) {
    	if (buffer == null) {
    		buffer = new LinkedList<Byte>();
    	}
    	int counter = 0;
    	for (int index = 0; index < readSize; ++index) {
    		int value = read(in);
    		if (value > -1) {
    			buffer.add((byte) value);
    			++counter;
    		} else {
    			break;
    		}
    	}
    	return counter;
    }
    
    /**
     * This method should be used to read data in from a stream with encrypted data so the end of file marker can be identified.
     * Returns a byte between 0 and 255, or -1, -2 if the end of the stream is reached or the end of file marker was reached 
     * @param in InputStream
     * @return integer between 0 and 255, or -1, or -2
     */
    private int read(InputStream in) {
    	if (amountMet == FILE_END.length) {
    		return -2;
    	}
    	try {
			int i = in.read();
			if (i > -1) {
				if (FILE_END[amountMet] == (byte) i) {
					amountMet++;
				} else {
					amountMet = 0;
				}
				return i;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	return -1;
    }
    
    
    /**
     * Destroys the Password held in the Object
     */
    public void destroy() {
        try {
            secretKey.destroy();
        } catch (DestroyFailedException ex) {
            
        }
    }
}
