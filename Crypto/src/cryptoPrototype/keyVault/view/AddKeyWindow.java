package cryptoPrototype.keyVault.view;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.time.LocalDateTime;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import cryptoPrototype.Encryption;
import cryptoPrototype.keyVault.model.AccountDetails;

/**
 * A simple window that allows the user to enter the data necessary to add a new key
 * @author Martin Birch
 */
public class AddKeyWindow implements ActionListener, KeyListener  {
    
	private JFrame window;
    private JTextField txtName;
    private JPasswordField txtPassword;
    private JTextField txtUsername;
    private JTextField txtSecurityQuestion;
    private JTextArea txtExtraDetails;
    private PropertyChangeSupport pSupport;
    
    private Encryption encrypt;
    
    /**
     * Class Constructor
     * @param listener PropertyChangeListener
     */
    public AddKeyWindow(PropertyChangeListener listener, Encryption encrypt) {
        pSupport = new PropertyChangeSupport(this);
        pSupport.addPropertyChangeListener(listener);
        this.encrypt = encrypt;
        window = new JFrame();
        window.setTitle("Add Key");
        window.add(createGUI());
        window.pack();
        window.setLocationRelativeTo(null);
        window.setResizable(false);
        window.setVisible(true);
    }
    
    /**
     * Creates and returns a JPanel that contains all the widgets for adding a key
     * @return JPanel
     */
    private JPanel createGUI() {
    	JPanel pnlAdd = new JPanel();
        pnlAdd.setLayout(new BoxLayout(pnlAdd, BoxLayout.Y_AXIS));
        
        JPanel pnlInput = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        JLabel lblName = new JLabel("Account Name:");
        c.ipadx = 10;
        c.anchor = GridBagConstraints.EAST;
        c.gridx = 0;
        c.gridy = 0;
        pnlInput.add(lblName, c);
        
        txtName = new JTextField();
        c.ipadx = 0;
        c.anchor = GridBagConstraints.WEST;
        c.gridx = 1;
        c.gridy = 0;
        c.gridwidth = 2;
        txtName.setPreferredSize(new Dimension(150, 20));
        pnlInput.add(txtName, c);
        
        JLabel lblPassword = new JLabel("Password:");
        c.ipadx = 10;
        c.anchor = GridBagConstraints.EAST;
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 1;
        pnlInput.add(lblPassword, c);

        txtPassword = new JPasswordField();
        txtPassword.setPreferredSize(new Dimension(150, 20));
        c.ipadx = 0;
        c.anchor = GridBagConstraints.WEST;  
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 2;
        c.gridheight = 1;
        pnlInput.add(txtPassword, c);
        txtPassword.addKeyListener(this);
        

        JLabel lblusername = new JLabel("Username:");
        c.ipadx = 10;
        c.anchor = GridBagConstraints.EAST;
        c.weightx = 0.0;
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 1;
        pnlInput.add(lblusername, c);

        txtUsername = new JTextField();
        txtUsername.setPreferredSize(new Dimension(150, 20));
        c.ipadx = 0;
        c.anchor = GridBagConstraints.WEST; 
        c.gridx = 1;
        c.gridy = 2;
        c.gridwidth = 2;
        c.gridheight = 1;
        pnlInput.add(txtUsername, c);
        txtUsername.addKeyListener(this);
        
        JLabel lblSecurityQuestion = new JLabel("Security Question:");
        c.ipadx = 10;
        c.anchor = GridBagConstraints.EAST;
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 1;
        pnlInput.add(lblSecurityQuestion, c);
        
        txtSecurityQuestion = new JTextField();
        Font fontUsed = txtSecurityQuestion.getFont();
        txtSecurityQuestion.setPreferredSize(new Dimension(150, 20));
        c.ipadx = 0;
        c.anchor = GridBagConstraints.WEST;
        c.gridx = 1;
        c.gridy = 3;
        c.gridwidth = 2;
        c.gridheight = 1;
        pnlInput.add(txtSecurityQuestion, c);
        txtSecurityQuestion.addKeyListener(this);

        JLabel lblExtraDetails = new JLabel("Extra Details:");
        c.ipadx = 10;
        c.anchor = GridBagConstraints.EAST;
        c.gridx = 0;
        c.gridy = 4;
        c.gridwidth = 1;
        pnlInput.add(lblExtraDetails, c);
        
        txtExtraDetails = new JTextArea();
        txtExtraDetails.setFont(fontUsed);
        JScrollPane scrDetails = new JScrollPane(txtExtraDetails);
        c.fill = GridBagConstraints.BOTH;
        c.ipadx = 0;
        c.anchor = GridBagConstraints.WEST;
        c.gridx = 1;
        c.gridy = 4;
        c.gridwidth = 2;
        c.gridheight = 3;
        c.weighty = 1.0;
        pnlInput.add(scrDetails, c);
        pnlInput.setPreferredSize(new Dimension(300, 160));
        
        JPanel pnlButtons = new JPanel();
        pnlButtons.setLayout(new BoxLayout(pnlButtons, BoxLayout.X_AXIS));
        JButton btnAddAccount = new JButton("Add Account");
        btnAddAccount.setName("btnAddAccount");
        btnAddAccount.addActionListener(this);
        JButton btnCancel = new JButton("Cancel");
        btnCancel.setName("btnCancel");
        btnCancel.addActionListener(this);
        pnlButtons.add(Box.createHorizontalStrut(10));
        pnlButtons.add(btnCancel);
        pnlButtons.add(Box.createHorizontalGlue());
        pnlButtons.add(btnAddAccount);
        pnlButtons.add(Box.createHorizontalStrut(10));
        
        pnlAdd.add(pnlInput);
        pnlAdd.add(pnlButtons);
        pnlAdd.add(Box.createVerticalStrut(10));
        
        return pnlAdd;
    }
    
    /**
     * Gets the details entered and returns them in the form of a KeyDetails
     * @return KeyDetails containing the data entered
     */
    public AccountDetails getKey() {
    	return new AccountDetails(txtPassword.getPassword(), txtUsername.getText().toCharArray(), 
    			txtName.getText().toCharArray(), txtSecurityQuestion.getText().toCharArray(), 
    			LocalDateTime.now(), txtExtraDetails.getText().toCharArray(), encrypt);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) { 
        if (e.getSource() instanceof JButton) {
            JButton btn = (JButton) e.getSource();
            switch (btn.getName()) {
                case "btnCancel":
                    window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
                    break;
                case "btnAddAccount":
                	pSupport.firePropertyChange("KeyAdded", null, getKey());
                    window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
                    break;
                default:
                    break;
            }
                
        }
    }
    
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			if (txtName.getText().length() > 0 && txtPassword.getPassword().length > 0) {
				pSupport.firePropertyChange("KeyAdded", null, getKey());
				window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
			} else {
				JOptionPane.showMessageDialog(window, "Please Enter at least a name and a Key!", 
                        "Cypto", JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
