/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptoPrototype.keyVault.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.SystemColor;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.border.EmptyBorder;

import cryptoPrototype.keyVault.controller.ControllerKeyVault;
import cryptoPrototype.keyVault.model.AccountDetails;
/**
 * A Window that displays all the Account the user has saved, and allows the user to view them individually
 * @author Martin Birch
 */
public class VaultGUI {
    
	private PropertyChangeSupport pSupport;
	private JFrame windowVault;
    private JPanel pnlRoot;
    private JPanel pnlKeyList;
    private JPanel pnlSideBar;
    private JTextField txtAccountName;
    private JTextField txtKeySelected;
    private JTextField txtUsername;
    private JTextField txtSecurityQ;
    private JTextArea txtExtraDetails;
    private JTextField txtSearch;
    private String lblKeyText;
    private ControllerKeyVault controllerKeyVault;
    
    private boolean showingPassword;
    private boolean showingUsername;
    
    /**
     * Class Constructor
     * @throws FileNotFoundException
     */
    public VaultGUI() throws FileNotFoundException {
        windowVault = new JFrame();
        createGUI();
        pSupport = new PropertyChangeSupport(this);
    }
    
    /**
     * Adds a controller
     * @param con ControllerKeyVault
     */
    public void addController(ControllerKeyVault con) {
    	controllerKeyVault = con;
    	try {
			createGUI();
		} catch (FileNotFoundException e) {
			
		}
    }
    
    /**
     * Adds a Property Change Listener to this class
     * @param listener PropertyChangeListener
     */
    public void addPropertyListener(PropertyChangeListener listener) {
    	pSupport.addPropertyChangeListener(listener);
    }
    
    /**
     * Gets the Account Heading Text
     * @return String heading text
     */
    public String getAccountHeadingText() {
    	return lblKeyText;
    }
    
    /**
     * Sets the Account Heading Text
     * @param str String heading text
     */
    public void setAccountHeadingText(String str) {
    	lblKeyText = str;
    }
    
    /**
     * Creates an non movable toolbar and set the buttons controllers
     * @param window JFrame
     */
    private JToolBar createToolBar() {
        JToolBar toolBar = new JToolBar();
        JButton addItem = new JButton("Add");
        addItem.setName("btnAdd");  
        toolBar.add(addItem); 
        
        JButton deleteItem = new JButton("Delete");
        deleteItem.setName("btnDelete");  
        toolBar.add(deleteItem);
        
        JButton editItem = new JButton("Edit");
        editItem.setName("btnEdit"); 
        toolBar.add(editItem);
        
        JButton saveItem = new JButton("Save");
        saveItem.setName("btnSave"); 
        toolBar.add(saveItem);
        
        toolBar.addSeparator();
        
        txtSearch = new JTextField();
        txtSearch.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        txtSearch.setName("txtSearch");
        txtSearch.setForeground(Color.LIGHT_GRAY);
        txtSearch.setPreferredSize(new Dimension(100, 25));
        toolBar.add(txtSearch);
        
        JButton btnSearch = new JButton();
        btnSearch.setName("btnSearch");
        btnSearch.setPreferredSize(new Dimension(25,25));
        //btnSearch.setIcon(new ImageIcon(new ImageIcon("Images\\search.png").getImage().getScaledInstance(25, 25, Image.SCALE_FAST)));
        btnSearch.setIcon(new ImageIcon(new ImageIcon(getClass().getResource("/Images/search.png")).getImage().getScaledInstance(25, 25, Image.SCALE_FAST)));
        btnSearch.setBackground(Color.WHITE);
        btnSearch.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        btnSearch.setOpaque(false);
        toolBar.add(btnSearch);
        
        if (controllerKeyVault != null) {
        	 saveItem.addActionListener(controllerKeyVault); 
        	 editItem.addActionListener(controllerKeyVault); 
        	 deleteItem.addActionListener(controllerKeyVault); 
        	 addItem.addActionListener(controllerKeyVault);
        	 txtSearch.addKeyListener(controllerKeyVault);
        	 btnSearch.addActionListener(controllerKeyVault);
        }
        toolBar.setFloatable(false);
        return toolBar;
    }
    
    /**
     * Create the basic GUI, removing any existing content
     * @throws FileNotFoundException
     */
    private void createGUI() throws FileNotFoundException { 
        windowVault.getContentPane().removeAll();
    	windowVault.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        windowVault.setPreferredSize(new Dimension(850, 651));
        windowVault.setTitle("Password Vault");
        pnlRoot = new JPanel(new BorderLayout());
        
        pnlRoot.add(createToolBar(),BorderLayout.PAGE_START);
        
        lblKeyText = "Account Name:   /\\";

		pnlKeyList = new JPanel();
		pnlSideBar = new JPanel();
		
        displayKeys(new ArrayList<AccountDetails>());
        infoBar(false, new ArrayList<AccountDetails>(),  null);
        pnlSideBar.setMaximumSize(new Dimension(250, 5000));
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, pnlKeyList, pnlSideBar);
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(600);
        pnlRoot.add(splitPane, BorderLayout.CENTER);
        
        windowVault.addWindowListener(controllerKeyVault);
        windowVault.add(pnlRoot);
        windowVault.pack();
        windowVault.setLocationRelativeTo(null);
        windowVault.setResizable(true);
    }
    
    /**
     * Sets the text in the search field, if the String provided is empty or is null then a default search message is displayed
     * @param txtText String to display in search field
     */
    public void setSearchText(String txtText) {
    	txtSearch.setText(txtText);
    	txtSearch.revalidate();
    	txtSearch.repaint();
    	windowVault.revalidate();
    	windowVault.repaint();
    }
    
    /**
     * Gets the Text in the Search Field except from the default Search Message
     * @return String search field text
     */
    public String getSearchText() {
    	if (txtSearch.getText().equals("Search")) {
    		return "";
    	} else {
    		return txtSearch.getText();
    	}
    }
    
    /**
     * Sets this window relative to another window
     * @param comp Component to be relative to
     */
    public void setLocationRelativeTo(Component comp) {
    	windowVault.setLocationRelativeTo(comp);
    }
    
    /**
     * Sets the visibility of this window
     * @param visible true or false
     */
    public void setVisible(boolean visible) {
        windowVault.setVisible(true);
    }
    
    /**
     * Creates the InfoBar on the right hand side of the JFrame, also displays any key selected key
     * @param editable Should the JTextboxes be editable
     * @param keys The Current list of keys
     * @param displaying The Key to display
     */
    public void infoBar(boolean editable, ArrayList<AccountDetails> keys, AccountDetails kd) {
        try {
            //pnlRoot.remove(pnlSideBar);
            pnlSideBar.removeAll();
        } catch (Exception e) {
            
        }
        
        
        //Create Panel To The Left
        //pnlSideBar = new JPanel();
        pnlSideBar.setMaximumSize(new Dimension(300, 30000));
        pnlSideBar.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        pnlSideBar.setLayout(new BoxLayout(pnlSideBar, BoxLayout.Y_AXIS));
        //pnlSideBar.setBackground(Color.GRAY);
        pnlSideBar.setBorder(BorderFactory.createRaisedBevelBorder());
        
        JLabel lblTitle = new JLabel("Account Name:");
        lblTitle.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        lblTitle.setBorder(new EmptyBorder(5,5,5,0));
        
        txtAccountName = new JTextField();
        Font fontUsed = txtAccountName.getFont();
        //txtAccountName.setBorder(new EmptyBorder(0,5,0,0));
        txtAccountName.setDisabledTextColor(Color.WHITE);
        txtAccountName.setForeground(Color.BLACK);
        txtAccountName.setMinimumSize(new Dimension(114, 24));
        txtAccountName.setMaximumSize(new Dimension(485, 24));
        txtAccountName.setEnabled(editable);
        txtAccountName.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        
        JLabel lblPass = new JLabel("Password:");
        lblPass.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        lblPass.setBorder(new EmptyBorder(5,5,5,0));
        
        txtKeySelected = new JTextField();
        //txtKeySelected.setBorder(new EmptyBorder(0,5,0,0));
        txtKeySelected.setDisabledTextColor(Color.WHITE);
        txtKeySelected.setForeground(Color.BLACK);
        txtKeySelected.setMinimumSize(new Dimension(114, 24));
        txtKeySelected.setMaximumSize(new Dimension(485, 24));
        txtKeySelected.setEnabled(editable);
        txtKeySelected.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        
        JLabel lblUsername = new JLabel("Username:");
        lblUsername.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        lblUsername.setBorder(new EmptyBorder(5,5,5,0));
        
        txtUsername = new JTextField();
        txtUsername.setBorder(new EmptyBorder(0,5,0,0));
        txtUsername.setDisabledTextColor(Color.WHITE);
        txtUsername.setForeground(Color.BLACK);
        txtUsername.setMinimumSize(new Dimension(129, 24));
        txtUsername.setMaximumSize(new Dimension(500, 24));
        txtUsername.setEnabled(editable);
        txtUsername.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        
        JLabel lblSecurityQ = new JLabel("Security Question:");
        lblSecurityQ.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        lblSecurityQ.setBorder(new EmptyBorder(5,5,5,0));
        
        txtSecurityQ = new JTextField();
        txtSecurityQ.setBorder(new EmptyBorder(0,5,0,0));
        txtSecurityQ.setDisabledTextColor(Color.WHITE);
        txtSecurityQ.setForeground(Color.BLACK);
        txtSecurityQ.setMinimumSize(new Dimension(129, 24));
        txtSecurityQ.setMaximumSize(new Dimension(500, 24));
        txtSecurityQ.setEnabled(editable);
        txtSecurityQ.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        
        JButton btnUsernamePeep = new JButton("-_-");
        btnUsernamePeep.setName("btnPeepUsername");
        btnUsernamePeep.setEnabled(false);
        btnUsernamePeep.setMinimumSize(new Dimension(15, 24));
        btnUsernamePeep.setMaximumSize(new Dimension(15, 24));
        btnUsernamePeep.addActionListener(controllerKeyVault);
        
        JPanel pnlUsername = new JPanel(new BorderLayout());
        pnlUsername.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        pnlUsername.setMinimumSize(new Dimension(129, 24));
        pnlUsername.setMaximumSize(new Dimension(500, 24));        
        
        pnlUsername.add(txtUsername, BorderLayout.CENTER);
        pnlUsername.add(btnUsernamePeep, BorderLayout.EAST);
        
        JButton btnPasswordPeep = new JButton("-_-");
        btnPasswordPeep.setName("btnPeepPassword");
        btnPasswordPeep.setEnabled(false);
        btnPasswordPeep.setMinimumSize(new Dimension(15, 24));
        btnPasswordPeep.setMaximumSize(new Dimension(15, 24));
        btnPasswordPeep.addActionListener(controllerKeyVault);
        
        JPanel pnlPassword = new JPanel(new BorderLayout());
        pnlPassword.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        pnlPassword.setMinimumSize(new Dimension(129, 24));
        pnlPassword.setMaximumSize(new Dimension(500, 24));        
        
        pnlPassword.add(txtKeySelected, BorderLayout.CENTER);
        pnlPassword.add(btnPasswordPeep, BorderLayout.EAST);
        
        JButton btnCopyPassword = new JButton("Copy Password");
        btnCopyPassword.setName("btnCopyPassword");
        btnCopyPassword.setMinimumSize(new Dimension(129, 24));
        btnCopyPassword.setMaximumSize(new Dimension(500, 24));
        btnCopyPassword.setEnabled(false);
        btnCopyPassword.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        btnCopyPassword.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        btnCopyPassword.addActionListener(controllerKeyVault);
        
        JButton btnCopyUsername = new JButton("Copy Username");
        btnCopyUsername.setName("btnCopyUsername");
        btnCopyUsername.setMinimumSize(new Dimension(129, 24));
        btnCopyUsername.setMaximumSize(new Dimension(500, 24));
        btnCopyUsername.setEnabled(false);
        btnCopyUsername.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        btnCopyUsername.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        btnCopyUsername.addActionListener(controllerKeyVault);

        JButton btnEdit = new JButton("Edit");
        btnEdit.setMinimumSize(new Dimension(129, 24));
        btnEdit.setMaximumSize(new Dimension(500, 24));
        btnEdit.setEnabled(false);
        btnEdit.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        btnEdit.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        btnEdit.addActionListener(controllerKeyVault);
        

        JButton btnUpdatePassword = new JButton("Update Password");
        btnUpdatePassword.setName("btnUpdatePassword");
        btnUpdatePassword.setMinimumSize(new Dimension(129, 24));
        btnUpdatePassword.setMaximumSize(new Dimension(500, 24));
        btnUpdatePassword.setEnabled(false);
        btnUpdatePassword.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        btnUpdatePassword.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        btnUpdatePassword.addActionListener(controllerKeyVault);
        btnUpdatePassword.setVisible(false);
        
        JLabel lblExtraDetails = new JLabel("Extra Details:");
        lblExtraDetails.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        lblExtraDetails.setBorder(new EmptyBorder(5,5,5,0));
        
        txtExtraDetails = new JTextArea();
        txtExtraDetails.setFont(fontUsed);
        //txtExtraDetails.setBorder(new EmptyBorder(0,5,0,0));
        txtExtraDetails.setDisabledTextColor(Color.WHITE);
        txtExtraDetails.setForeground(Color.BLACK);
        //txtExtraDetails.setMinimumSize(new Dimension(129, 24));
        //txtExtraDetails.setMaximumSize(new Dimension(500, 24));
        txtExtraDetails.setEnabled(editable);
        txtExtraDetails.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        
        JScrollPane scrDetails = new JScrollPane(txtExtraDetails);
        //scrDetails.setBorder(new EmptyBorder(0,5,0,0));
        scrDetails.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        scrDetails.setMinimumSize(new Dimension(129, 100));
        scrDetails.setMaximumSize(new Dimension(500, 100));
        
        if (editable) {
            txtKeySelected.setBackground(Color.WHITE);
            txtAccountName.setBackground(Color.WHITE);
            txtUsername.setBackground(Color.WHITE);
            txtSecurityQ.setBackground(Color.WHITE);
            txtExtraDetails.setBackground(Color.WHITE);
            btnEdit.setName("btnSave");
            btnEdit.setText("Save");
            btnUpdatePassword.setVisible(true);
            btnUpdatePassword.setEnabled(true);
        } else {
            txtKeySelected.setBackground(Color.LIGHT_GRAY);
            txtAccountName.setBackground(Color.LIGHT_GRAY);
            txtUsername.setBackground(Color.LIGHT_GRAY);
            txtSecurityQ.setBackground(Color.LIGHT_GRAY);
            txtExtraDetails.setBackground(Color.LIGHT_GRAY);
            btnEdit.setName("btnEdit");
        }
        
        if (kd != null) {
        	txtAccountName.setText(String.valueOf(kd.getTitle()));
            txtKeySelected.setText(String.valueOf(kd.getPassword(), 0, kd.getPassword().length));
            txtUsername.setText(String.valueOf(kd.getUsername(), 0, kd.getUsername().length));
            txtSecurityQ.setText(String.valueOf(kd.getSecurityQuestion()));
            txtExtraDetails.setText(String.valueOf(kd.getExtraDetails()));
            btnCopyUsername.setEnabled(true);
            btnCopyPassword.setEnabled(true);
            if (!editable) {
            	btnPasswordPeep.setEnabled(true);
            	btnUsernamePeep.setEnabled(true);            	
            }
            btnEdit.setEnabled(true);
        }
        
        JLabel keyTotal = new JLabel("Accounts Displayed: " + keys.size());
        keyTotal.setBorder(BorderFactory.createEmptyBorder(0, 10, 5, 0));
        JPanel accountDetails = new JPanel();
        accountDetails.setLayout(new BoxLayout(accountDetails, BoxLayout.Y_AXIS));
        
        accountDetails.add(lblTitle);
        accountDetails.add(txtAccountName);
        accountDetails.add(lblUsername);
        accountDetails.add(pnlUsername);
       // accountDetails.add(txtUsername);
        accountDetails.add(Box.createVerticalStrut(5));
        accountDetails.add(btnCopyUsername);
        accountDetails.add(Box.createVerticalStrut(5));
        accountDetails.add(lblPass);
        accountDetails.add(pnlPassword);
        //accountDetails.add(txtKeySelected);
        accountDetails.add(Box.createVerticalStrut(5));
        accountDetails.add(btnCopyPassword);
        accountDetails.add(Box.createVerticalStrut(5));
        accountDetails.add(lblSecurityQ);
        accountDetails.add(txtSecurityQ);
        accountDetails.add(lblExtraDetails);
        accountDetails.add(scrDetails);
        accountDetails.add(Box.createVerticalStrut(20));
        accountDetails.add(btnUpdatePassword);
        accountDetails.add(Box.createVerticalStrut(10));
        accountDetails.add(btnEdit);
        
        int increaseHeight = 0;
        if (btnUpdatePassword.isVisible()) {
        	increaseHeight = 30;
        }
        
        accountDetails.setMinimumSize(new Dimension(140, 450 + increaseHeight));
        accountDetails.setPreferredSize(new Dimension(150, 450 + increaseHeight));
        pnlSideBar.add(accountDetails);
        pnlSideBar.add(Box.createVerticalGlue());
        pnlSideBar.add(keyTotal);
        
        pnlSideBar.setMinimumSize(new Dimension(140, 100));
        pnlSideBar.setMaximumSize(new Dimension(152, 1500));
        pnlSideBar.setPreferredSize(new Dimension(151, 500));
        
        //pnlRoot.add(pnlSideBar, BorderLayout.EAST);

        pnlSideBar.revalidate();
        pnlSideBar.repaint();
        windowVault.repaint();
        windowVault.revalidate();
    }
    
    /**
     * Creates the centered table styled list of keys, removes existing layout if it is already present
     * @param pnlRoot JPanel
     * @param keys ArrayList<KeyDetails> to display
     * @throws FileNotFoundException
     */
    private void displayKeys(ArrayList<AccountDetails> keys) throws FileNotFoundException {
        try {
            //root.remove(pnlLayout);
            pnlKeyList.removeAll();
        } catch (Exception e) {
            
        }
        
        //pnlKeyList = new JPanel();
        pnlKeyList.setLayout(new BoxLayout(pnlKeyList, BoxLayout.Y_AXIS));
       
        JPanel pnlHeader = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        pnlHeader.setBackground(Color.GRAY);
        JPanel pnlKey = new JPanel();
        pnlKey.setLayout(new FlowLayout(FlowLayout.LEFT));
        Font f = new Font("Droid Serif", Font.ITALIC, 15);
        JLabel lblKeyTitle = new JLabel(lblKeyText); lblKeyTitle.setFont(f);
        pnlKey.add(lblKeyTitle); pnlKey.setBackground(Color.GRAY);
        pnlKey.addMouseListener(controllerKeyVault);
        pnlKey.setName("HeaderAccountName");
        pnlHeader.setBorder(new EmptyBorder(0,20,0,0));
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.WEST;
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 2;
        c.weightx = 1.0;
        pnlHeader.add(pnlKey, c);
        
        JPanel pnlKeyAge = new JPanel();
        JLabel lblKeyAgeTitle = new JLabel("Password Age:"); lblKeyAgeTitle.setFont(f);
        pnlKeyAge.add(lblKeyAgeTitle); pnlKeyAge.setBackground(Color.GRAY);
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 2;
        c.gridy = 0;
        c.gridwidth = 1;
        c.weightx = 0.0;
        c.anchor = GridBagConstraints.EAST;
        pnlHeader.add(pnlKeyAge, c);
        
        pnlHeader.setPreferredSize(new Dimension(0, 30));
        pnlHeader.setMaximumSize(new Dimension(3000, 30));
        pnlHeader.setMinimumSize(new Dimension(170, 30));
        
        JPanel pnlTable = new JPanel();
        pnlTable.setLayout(new BoxLayout(pnlTable, BoxLayout.Y_AXIS));
        //pnlTable.setBackground(Color.DARK_GRAY);
        JScrollPane scrPane = new JScrollPane(pnlTable);
        scrPane.getVerticalScrollBar().setUnitIncrement(16);
        boolean changeColor = false;
        for (AccountDetails k : keys) {
            JPanel pnlRow = new JPanel(new GridBagLayout());
            pnlRow.addMouseListener(controllerKeyVault); pnlRow.setName("KeyNo: " + k.getID());
            
            JLabel lblKey = new JLabel(String.valueOf(k.getTitle()));
            Font font = lblKey.getFont();
            Font newFont = new Font(font.getFontName(), font.getStyle(), font.getSize() + 1);
            lblKey.setFont(newFont);
            JPanel pnlKeyHolder = new JPanel();
            pnlKeyHolder.setLayout(new FlowLayout(FlowLayout.LEFT));
            pnlKeyHolder.add(lblKey);
            pnlKeyHolder.setMinimumSize(new Dimension(75, 30));
            pnlKeyHolder.setMaximumSize(new Dimension(3000, 30));
            
            c.fill = GridBagConstraints.BOTH;
            c.anchor = GridBagConstraints.WEST;
            c.gridx = 0;
            c.gridy = 0;
            c.gridwidth = 2;
            c.weightx = 1.0;
            pnlRow.add(pnlKeyHolder, c);
            
            JPanel pnlAge = new JPanel(new FlowLayout());
            JLabel lblKeyAge = new JLabel(String.valueOf(k.getAge()));
            font = lblKey.getFont();
            newFont = new Font(font.getFontName(), font.getStyle(), font.getSize());
            lblKeyAge.setFont(newFont);
            pnlAge.add(lblKeyAge);
            pnlAge.setMinimumSize(new Dimension(75, 30));
            pnlAge.setMaximumSize(new Dimension(75, 30));
            pnlAge.setPreferredSize(new Dimension(75, 30));
            
            c.fill = GridBagConstraints.BOTH;
            c.gridx = 2;
            c.gridy = 0;
            c.gridwidth = 1;
            c.weightx = 0.0;
            c.anchor = GridBagConstraints.WEST;
            pnlAge.setPreferredSize(new Dimension(75,30));
            pnlAge.setMaximumSize(new Dimension(75, 30));
            pnlRow.add(pnlAge, c);
            
            pnlAge.setOpaque(false);
            pnlKeyHolder.setOpaque(false);
            
            if (changeColor) {
                pnlRow.setBackground(SystemColor.controlDkShadow);
                changeColor = false;
            } else {
                pnlRow.setBackground(SystemColor.DARK_GRAY);
                changeColor = true;
            }
            
            pnlRow.setMinimumSize(new Dimension(170,30));
            pnlRow.setMaximumSize(new Dimension(3000, 30));
            pnlRow.setBorder(new EmptyBorder(0,20,0,0));
            pnlTable.add(pnlRow);
        }
        
        pnlKeyList.add(pnlHeader);
        pnlKeyList.add(scrPane);
        pnlKeyList.revalidate();
        pnlKeyList.repaint();
        //pnlRoot.add(pnlLayout, BorderLayout.CENTER);
        
    }
    
    /**
     * Refreshes the keys on display
     * @param keys The ArrayList of KeyDetails to display
     * @param displaying The Current Selected key, use -1 to selected no key to display
     */
    public void refreshKeys(ArrayList<AccountDetails> keys, AccountDetails account)  {
        try {
			displayKeys(keys);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
        infoBar(false, keys, account);
        windowVault.repaint();
        windowVault.revalidate();
    }

	/**
	 * Fires a Property Change Event
	 * @param propertyName String
	 * @param b1 old value boolean
	 * @param b2 new value boolean
	 */
    public void firePropertyChange(String propertyName, boolean b1, boolean b2) {
    	pSupport.firePropertyChange(propertyName, b1, b2);
    }
    
    /**
     * Gets the Account name text of the Key Selected
     * @return Account name char[]
     */
    public char[] getTxtAccountName() {
    	return txtAccountName.getText().toCharArray();
	}

    /**
     * Gets the Password of the Key Selected
     * @return password char[]
     */
	public char[] getTxtKeySelected() {
		return txtKeySelected.getText().toCharArray();
	}

	/**
	 * Gets the username of the Key Selected
	 * @return username char[]
	 */
	public char[] getTxtUsername() {
		return txtUsername.getText().toCharArray();
	}

	/**
	 * Gets the security question of the Key Selected
	 * @return security question char[]
	 */
	public char[] getTxtSecurityQ() {
		return txtSecurityQ.getText().toCharArray();
	}

	/**
	 * Gets the extra details about the key of the Key Selected
	 * @return extra details char[]
	 */
	public char[] getTxtExtraDetails() {
		return txtExtraDetails.getText().toCharArray();
	}
	
	public void hidePassword() {
		txtKeySelected.setText("**********");
		showingPassword = false;
	}
	
	public void hideUsername() {
		txtUsername.setText("**********");
		showingUsername = false;
	}
	
	public void showPassword(AccountDetails account) {
		txtKeySelected.setText(String.valueOf(account.getPassword()));
		showingPassword = true;
	}
	
	public void showUsername(AccountDetails account) {
		txtUsername.setText(String.valueOf(account.getUsername()));
		showingUsername = true;
	}
	
	public void toggleShowPassword(AccountDetails account) {
		if (showingPassword) {
			txtKeySelected.setText("**********");
			showingPassword = false;
		} else {
			txtKeySelected.setText(String.valueOf(account.getPassword()));
			showingPassword = true;
		}
	}
	
	public void toggleShowUsername(AccountDetails account) {
		if (showingUsername) {
			txtUsername.setText("**********");
			showingUsername = false;
		} else {
			txtUsername.setText(String.valueOf(account.getUsername()));
			showingUsername = true;
		}
	}
	
	/**
	 * Sets the password text
	 * @param str
	 */
	public void setPasswordText(String str) {
		txtKeySelected.setText(str);
		txtKeySelected.revalidate();
		txtKeySelected.repaint();
		windowVault.revalidate();
		windowVault.repaint();
	}
	
	public JFrame getWindow() {
		return windowVault;
	}
	
	public void dispose() {
		windowVault.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
}
