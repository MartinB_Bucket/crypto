package cryptoPrototype.keyVault.controller;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import cryptoPrototype.Encryption;
import cryptoPrototype.FilePaths;
import cryptoPrototype.keyVault.model.AccountDetails;
import cryptoPrototype.keyVault.model.IntegrityException;
import cryptoPrototype.keyVault.model.KeyVault;
import cryptoPrototype.keyVault.view.AddKeyWindow;
import cryptoPrototype.keyVault.view.VaultGUI;
import cryptoPrototype.passwordGenerator.model.Generator;
import cryptoPrototype.syncManager.Record;
import cryptoPrototype.syncManager.RecordKeeper;
/**
 * A Controller needed to add the functionality to VaultGUI
 * @author Martin Birch
 */
public class ControllerKeyVault implements FilePaths, ActionListener, MouseListener, PropertyChangeListener, WindowListener, KeyListener {

	private VaultGUI keyVaultGUI;
	private KeyVault keyVault;
	private ArrayList<AccountDetails> keys;
    private AccountDetails selectedAccount;
    private Encryption encrypt;
    private boolean allowClose;
    private boolean hasChanges;
	
    /**
	 * Class Constructor
	 * @param keyVaultGUI VaultGUI
	 * @param database location of KeyVault data
	 * @param encrypt Encryption placed on the file
	 */
	public ControllerKeyVault(VaultGUI keyVaultGUI, File database, Encryption encrypt) {
		this.keyVault = new KeyVault(database, RECORD_LOG, encrypt);
		this.keyVaultGUI = keyVaultGUI;
		this.encrypt = encrypt;
	}

	/**
	 * Loads the keys in, and refreshes the VaultGUI
	 * @throws InvalidAlgorithmParameterException 
	 * @throws InvalidKeyException 
	 */
	public void refreshKeys() throws InvalidKeyException, InvalidAlgorithmParameterException {
		try {
			keys = keyVault.loadKeys();
			keyVaultGUI.refreshKeys(keys, null);
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "No Account Database Found", 
					"Error Opening File", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (IntegrityException e) {
			 JOptionPane.showConfirmDialog(keyVaultGUI.getWindow(), "Integrity Error Whilst Reading Keys! Aborting!", 
						"Integrity Error", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	/**
	 * Saves the changes made to a key, and then writes the changes to the file
	 */
	private void saveKeys() {
		for (AccountDetails kd : keys) {
            if (kd.getID() == selectedAccount.getID()) {
            	if (!Arrays.equals(kd.getPassword(), keyVaultGUI.getTxtKeySelected())) {
            		kd.setPassword(keyVaultGUI.getTxtKeySelected());            		
            	}
            	if (!Arrays.equals(kd.getExtraDetails(), keyVaultGUI.getTxtExtraDetails())) {
            		kd.addExtraDetails(keyVaultGUI.getTxtExtraDetails());            		
            	}
            	if (!Arrays.equals(kd.getSecurityQuestion(), keyVaultGUI.getTxtSecurityQ())) {
            		kd.addSecurityQuestion(keyVaultGUI.getTxtSecurityQ());            		
            	}
            	if (!Arrays.equals(kd.getTitle(), keyVaultGUI.getTxtAccountName())) {
            		kd.addTitle(keyVaultGUI.getTxtAccountName());            		
            	}
            	if (!Arrays.equals(kd.getUsername(), keyVaultGUI.getTxtUsername())) {
            		kd.addUsername(keyVaultGUI.getTxtUsername());            		
            	}
                hasChanges = true;
            }
        }
        keyVaultGUI.refreshKeys(keys, selectedAccount);
	}
	
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JComponent) {
        	JComponent b = (JComponent) e.getSource();
        	String btnName = b.getName();
            switch (btnName) {
                case "btnAdd":
                    new AddKeyWindow(this, encrypt);
                    break;
                case "btnEdit":
                	keyVaultGUI.infoBar(true, keys, selectedAccount);
                	keyVaultGUI.showPassword(selectedAccount);
                	keyVaultGUI.showUsername(selectedAccount);
                    break;
                case "btnDelete":
                    try {
                        for (AccountDetails kd : keys) {
                            if (kd.getID() == selectedAccount.getID()) {
                            	RecordKeeper rk = kd.getRecordKeeper();
                				rk.openLog();
                				rk.createEntry(kd.getID(), String.valueOf(kd.getTitle()), 
                						Record.STATE_REMOVED, LocalDateTime.now(), 0);
                                keys.remove(kd);
                                kd.destroy();
                                break;
                            }
                        }
                        hasChanges = true;
                		writeKeys(false);
                    } catch (Exception ex) {
                        
                    }
                    keyVaultGUI.refreshKeys(keys, null);
                    break;
                case "btnSave":
                    saveKeys();
                	keyVaultGUI.hidePassword();
                	keyVaultGUI.hideUsername();    
            		writeKeys(false);                
                    break;
                case "btnUpdatePassword":
                    Generator gen = new Generator();
                    keyVaultGUI.setPasswordText(gen.createPassword(3, 4));
                    break;
                case "btnPeepPassword":
                	keyVaultGUI.toggleShowPassword(selectedAccount);
                	break;
                case "btnPeepUsername":
                	keyVaultGUI.toggleShowUsername(selectedAccount);
                	break;
                default:
                    break;
            }
            if (btnName.contains("btnCopy")) {
            	char[] chars = null;
            	if (b.getName().equals("btnCopyUsername")) {
            		chars = selectedAccount.getUsername();
            	} else if (b.getName().equals("btnCopyPassword")) {
            		chars = selectedAccount.getPassword();
            	}
            	StringSelection selection = new StringSelection(String.copyValueOf(chars));
            	Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            	clipboard.setContents(selection, null);
            }
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() instanceof JPanel) {
            JPanel p = (JPanel) e.getSource();
            if (p.getName().equals("HeaderAccountName")) {
            	if (keyVaultGUI.getAccountHeadingText().equals("Account Name:   \\/")) {
            		keyVault.sortKeys(keys, KeyVault.SORTBY_TITLE_ACENDING);
            		keyVaultGUI.setAccountHeadingText("Account Name:   /\\");
        			keyVaultGUI.refreshKeys(keys, selectedAccount);
        		} else {
        			keyVault.sortKeys(keys, KeyVault.SORTBY_TITLE_DESCENDING);
        			keyVaultGUI.setAccountHeadingText("Account Name:   \\/");
        			keyVaultGUI.refreshKeys(keys, selectedAccount);
        		}
            } else if (p.getName().contains("KeyNo: ")) {
            	int newAccountNo = Integer.parseInt(p.getName().substring(7, p.getName().length()));

            	AccountDetails kd = null;
            	for (AccountDetails key : keys) {
            		if (key.getID() == newAccountNo) {
            			kd = key;
            			break;
            		}
            	}
            	selectedAccount = kd;
            	keyVaultGUI.infoBar(false, keys, selectedAccount);
            	keyVaultGUI.hidePassword();
            	keyVaultGUI.hideUsername();
            }
        } else {
            selectedAccount = null;
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals("KeyAdded")) {
			if (evt.getNewValue() instanceof AccountDetails) {
				AccountDetails kd = (AccountDetails) evt.getNewValue();
				if (kd != null) {
					if (keys.size() > 0) {
						RecordKeeper rk = keys.get(0).getRecordKeeper();
						if (rk != null) {
							kd.addRecordKeeper(rk);
							keys.add(kd);
						} else {
							rk = new RecordKeeper(encrypt, RECORD_LOG);
							kd.addRecordKeeper(rk);
							keys.add(kd);
						}
						rk.createEntry(kd.getID(), String.valueOf(kd.getTitle()), Record.STATE_NEW, kd.getDate(), 0);
					} else {
						RecordKeeper rk = new RecordKeeper(encrypt, RECORD_LOG);
						kd.addRecordKeeper(rk);
						keys.add(kd);
						rk.createEntry(kd.getID(), String.valueOf(kd.getTitle()), Record.STATE_NEW, kd.getDate(), 0);
					}
					hasChanges = true;
				}
				selectedAccount = null;
				keyVaultGUI.refreshKeys(keys, selectedAccount);
			}
		}
	}

	@Override
	public void windowOpened(WindowEvent e) {}

	@Override
	public void windowClosing(WindowEvent e) {
		if (!allowClose) {
			writeKeys(true);			
		}
		keyVault.closeVault(keys);
	}

	/**
	 * Handles the writing of the account to the file, if there
	 * are problems when writing to the file the user is informed 
	 * and asked if they would like to exit.
	 * @param closeOnSuccess Should the VaultGUI close after the writing has finished
	 */
	private void writeKeys(boolean closeOnSuccess) {
		try {
			if (hasChanges) {
				keyVault.writeKeys(keys);
				hasChanges = false;
			}
			if (closeOnSuccess) {
				keyVaultGUI.firePropertyChange("WindowClosed", false, true);
				keyVaultGUI.dispose();
			}
		} catch (IOException e1) {
			int value = JOptionPane.showConfirmDialog(keyVaultGUI.getWindow(), "Changes Can Not Be Saved! \n Would You Like To Exit?", 
					"Fatal Write Error", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
			if (value == JOptionPane.YES_OPTION) {
				allowClose = true;
				keyVaultGUI.dispose();
				keyVaultGUI.firePropertyChange("WindowClosed", false, true);
			}
			e1.printStackTrace();
		}
	}
	
	@Override
	public void windowClosed(WindowEvent e) {}

	@Override
	public void windowIconified(WindowEvent e) {}

	@Override
	public void windowDeiconified(WindowEvent e) {}

	@Override
	public void windowActivated(WindowEvent e) {}

	@Override
	public void windowDeactivated(WindowEvent e) {}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	/**
	 * Performs a test on toTest to check if it contains a sub array containing the characters in charsRequired
	 * @param toTest char[] of character to check
	 * @param charsRequired char[] of required characters, order sensitive
	 * @return
	 */
	private boolean containsCharacters(char[] toTest, char[] charsRequired) {
		if (charsRequired == null || charsRequired.length == 0) {
			return true;
		}
		
		if (toTest.length < charsRequired.length) {
			return false;
		}
		
		if (charsRequired.length > 1) {
			ArrayList<Integer> valid = new ArrayList<Integer>();
			for (int i = 0; i < toTest.length - 1; ++i) { // Deduct one due to a pair being needed, so there is no point in checking last index as it has no pair
				if (toTest[i] == charsRequired[0]) {
					if (toTest[i+1] == charsRequired[1]) {
						valid.add(i);
					}
				}
			}

			for (Integer i : valid) {
				if (containsCharacters(Arrays.copyOfRange(toTest, i, toTest.length), Arrays.copyOfRange(charsRequired, 1, charsRequired.length))) {
					return true;
				}
			}

		} else {
			for (int i = 0; i < toTest.length; ++i) {
				if (toTest[i] == charsRequired[0]) {
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {
		String strSearch = keyVaultGUI.getSearchText();
		ArrayList<AccountDetails> refinedList = new ArrayList<AccountDetails>();
		for (AccountDetails kd : keys) {
			if (containsCharacters(kd.getTitle(),strSearch.toCharArray())) {
				refinedList.add(kd);
			}
		}
		keyVaultGUI.refreshKeys(refinedList, null);
	}
}
