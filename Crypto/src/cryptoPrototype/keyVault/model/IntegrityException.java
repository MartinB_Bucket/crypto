package cryptoPrototype.keyVault.model;

public class IntegrityException extends Exception {
	private static final long serialVersionUID = 2596677805293708438L;

	public IntegrityException(String message) {
		super(message);
	}
}
