package cryptoPrototype.keyVault.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

import javax.security.auth.Destroyable;

import cryptoPrototype.Encryption;
import cryptoPrototype.FilePaths;
import cryptoPrototype.HashingSystem;
import cryptoPrototype.syncManager.Record;
import cryptoPrototype.syncManager.RecordKeeper;

/**
 * Holds the details about an account, each object has its own unique id
 * @author Martin Birch
 */
public class AccountDetails implements Destroyable, Serializable, FilePaths  {
    
	private static final long serialVersionUID = 8765432345679876543L;
	
	private static int amount = 0;
    private transient Encryption encrypt;
	private int id;

    private byte[] password;
    private byte[] username;
    private byte[] securityQuestion;
    private byte[] extraDetails;
    private byte[] title;
    private byte[] integrity;
    
    private transient boolean logChanges;
    
    private LocalDateTime dateCreated;
    
    private transient RecordKeeper rKeeper;
    
    /**
     * Class Constructor
     * @param password char[]
     * @param username char[]
     * @param title char[]
     * @param securityQuestion char[]
     * @param dateCreated LocalDate
     * @param extraDetails char[]
     * @param Encrption encrypt
     */
    public AccountDetails(char[] password, char[] username, char[] title, char[] securityQuestion, LocalDateTime dateCreated, char[] extraDetails, Encryption encrypt) {
    	this.password = encrypt(password, encrypt);
    	this.username = encrypt(username, encrypt);
    	this.securityQuestion = encrypt(securityQuestion, encrypt);
    	this.dateCreated = dateCreated;
    	this.extraDetails = encrypt(extraDetails, encrypt);
    	this.encrypt = encrypt;
    	amount++;
    	id = amount;
    	logChanges = true;
    	this.title = encrypt(title, encrypt);
    }
    
    /**
     * Class Constructor
     * @param Encrption encrypt
     */
    public AccountDetails(Encryption encrypt) {
    	this.encrypt = encrypt;
        amount++;
        id = amount;
        logChanges = true;
    }
    
    public void addRecordKeeper(RecordKeeper recordKeeper) {
    	rKeeper = recordKeeper;
    	rKeeper.openLog();
    }
    
    public RecordKeeper getRecordKeeper() {
    	return rKeeper;
    }
    
    public void setLogChanges(boolean flag) {
    	logChanges = flag;
    }
    
    /**
     * Adds an Encryption object to be used
     * @param Encryption  encrypt
     */
    public void addEncryption(Encryption encrypt) {
    	this.encrypt = encrypt;
        rKeeper = new RecordKeeper(encrypt, RECORD_LOG);
    	rKeeper.openLog();
    }

    /**
     * Used to change the encryption object if there is an existing one present
     * @param Encryption encrypt
     */
    public void changeEncryption(Encryption encrypt) {
    	this.password = encrypt(decrypt(password), encrypt);
    	this.username = encrypt(decrypt(username), encrypt);
    	this.title = encrypt(decrypt(title), encrypt);
    	this.securityQuestion = encrypt(decrypt(securityQuestion), encrypt);
    	this.extraDetails = encrypt(decrypt(extraDetails), encrypt);
    	this.encrypt = encrypt;
    }
    
    /**
     * Used to encrypt an array of char[]
     * @param letters char[]
     * @return byte[]
     */
    private byte[] encrypt(char[] letters, Encryption encrypt) {
    	if (letters != null && letters.length > 0) {
    		try {
    			return encrypt.encryptBytesToBytes(HashingSystem.convertCharToByte(letters));
    		} catch (InvalidKeyException | FileNotFoundException e) {
    			e.printStackTrace();
    		}
    	}
    	return new byte[0];
    }
    
    /**
     * Used to decrypt an array of byte[]
     * @param letters byte[]
     * @return char[]
     */
    private char[] decrypt(byte[] letters) {
    	if (letters != null && letters.length > 0) {
    		try {
    			return HashingSystem.convertByteToChar(encrypt.decryptBytesToBytes(letters));
    		} catch (InvalidKeyException | InvalidAlgorithmParameterException | IOException e) {
    			e.printStackTrace();
    		}
    	}
    	return new char[0];
    }

	/**
     * Gets the username
     * @return username char[]
     */
    public char[] getUsername() {
    	return decrypt(username);
    }
    
    /**
     * Adds the username
     * @param username char[]
     */
    public void addUsername(char[] username) {
    	this.username = encrypt(username, encrypt);
    	if (logChanges) {
    		rKeeper.createEntry(id, String.valueOf(getTitle()), Record.STATE_MODIFIED, dateCreated, Record.USERNAME);
    	}
    }
    
    /**
     * Gets the age of the key
     * @return long days
     */
    public long getAge() {
        //Duration d =  Duration.between(dateCreated, LocalDate.now());
        //return d.toDays();
    	return dateCreated.until(LocalDateTime.now(), ChronoUnit.DAYS);
    }
    
    /**
     * Gets the date the key was created
     * @return LocalDate date the key was created
     */
    public LocalDateTime getDate() {
        return dateCreated;
    }
    
    /**
     * Gets the Key ID
     * @return int ID
     */
    public int getID() {
         return id;
    }

    /**
     * Gets the password
     * @return password char[]
     */
    public char[] getPassword() {
        return decrypt(password);
    }

    /**
     * Sets the password and updates the date
     * @param password char[]
     */
    public void setPassword(char[] password) {
        this.password = encrypt(password, encrypt);
        dateCreated = LocalDateTime.now();
        if (logChanges) {
        	rKeeper.createEntry(id, String.valueOf(getTitle()), Record.STATE_MODIFIED, dateCreated, Record.PASSWORD);
        }
    }
    
    /**
     * Adds a password without changing the date
     * @param password
     */
    public void addPassword(char[] password) {
        this.password = encrypt(password, encrypt);
        if (logChanges) {
        	rKeeper.createEntry(id, String.valueOf(getTitle()), Record.STATE_MODIFIED, dateCreated, Record.PASSWORD);
        }
    }
    
    /**
     * Gets the Security Question
     * @return char[] security question
     */
    public char[] getSecurityQuestion() {
		return decrypt(securityQuestion);
	}

    /**
     * Sets the security question
     * @param securityQuestion char[]
     */
	public void addSecurityQuestion(char[] securityQuestion) {
		this.securityQuestion = encrypt(securityQuestion, encrypt);
        if (logChanges) {
        	rKeeper.createEntry(id, String.valueOf(getTitle()), Record.STATE_MODIFIED, dateCreated, Record.SECURITY_QUESTION);
        }
	}

	/**
	 * Gets the Extra Details stored about the key
	 * @return Extra Details char[]
	 */
	public char[] getExtraDetails() {
		return decrypt(extraDetails);
	}

	/**
	 * Adds the Extra Details about the key
	 * @param extraDetails char[]
	 */
	public void addExtraDetails(char[] extraDetails) {
		this.extraDetails = encrypt(extraDetails, encrypt);
        if (logChanges) {
        	rKeeper.createEntry(id, String.valueOf(getTitle()), Record.STATE_MODIFIED, dateCreated, Record.EXTRA_DETAILS);
        }
	}

	/**
	 * Gets the Key Title
	 * @return title char[]
	 */
	public char[] getTitle() {
		return decrypt(title);
	}

	/**
	 * Adds the Key Title
	 * @param title char[]
	 */
	public void addTitle(char[] title) {
        if (logChanges) {
        	rKeeper.createEntry(id, String.valueOf(getTitle()), Record.STATE_MODIFIED, dateCreated, Record.TITLE);
        }
		this.title = encrypt(title, encrypt);
	}
    
	@Override
    public void destroy() {
        for (int i = 0; i < password.length; i++) {
        	password[i] = 'a';
		}
        for (int i = 0; i < username.length; i++) {
        	username[i] = 'a';
		}
        for (int i = 0; i < extraDetails.length; i++) {
        	extraDetails[i] = 'a';
		}
        for (int i = 0; i < securityQuestion.length; i++) {
        	securityQuestion[i] = 'a';
		}
        for (int i = 0; i < title.length; i++) {
        	title[i] = 'a';
		}
        rKeeper.closeLog();
    }
	
    public boolean checkIntegrity() {
    	byte[] check = getIntegrityHash();
    	return Arrays.equals(integrity, check);
    }
    
    private byte[] getIntegrityHash() {
    	byte[] bytes = new byte[username.length + password.length + title.length + securityQuestion.length + extraDetails.length];
		int position = 0;
		System.arraycopy(username, 0, bytes, position, username.length);
		position += username.length;
		System.arraycopy(password, 0, bytes, position, password.length);
		position += password.length;
		System.arraycopy(title, 0, bytes, position, title.length);
		position += title.length;
		System.arraycopy(securityQuestion, 0, bytes, position, securityQuestion.length);
		position += securityQuestion.length;
		System.arraycopy(extraDetails, 0, bytes, position, extraDetails.length);
		
		HashingSystem hs = new HashingSystem(HashingSystem.MODE_FAST);
		return hs.hash(bytes);
    }
    
	/**
	 * Writes the object, to the ObjectOutputStream used by Serializable
	 * @param out ObjectOutputStream
	 * @throws IOException
	 */
	private void writeObject(ObjectOutputStream out) throws IOException {
		integrity = getIntegrityHash();
    	out.writeInt(id);
    	out.writeObject(username);
    	out.writeObject(password);
    	out.writeObject(title);
    	out.writeObject(securityQuestion);
    	out.writeObject(extraDetails);
    	out.writeObject(dateCreated);
    	out.writeObject(integrity);
    	out.writeInt(amount);
        out.flush();
    }
    
	/**
	 * Reads the object in from the ObjectInputStream by Serializable
	 * @param in ObjectInputStream
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
    	id = in.readInt();
    	username = (byte[]) in.readObject();
    	password = (byte[]) in.readObject();
    	title = (byte[]) in.readObject();
    	securityQuestion = (byte[]) in.readObject();
    	extraDetails = (byte[]) in.readObject();
    	dateCreated = (LocalDateTime) in.readObject();
    	integrity = (byte[]) in.readObject();
    	int temp = in.readInt(); 
    	if (temp > amount) {
    		amount = temp;
    	}
    	logChanges = true;
    }

}
