package cryptoPrototype.keyVault.model;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import cryptoPrototype.Encryption;
import cryptoPrototype.syncManager.RecordKeeper;
/**
 * Reads, writes and sorts the keys for the ControllerKeyVault
 * @author Martin Birch
 */
public class KeyVault {
    private File database;
    private File records;
    private Encryption encrypt;
    
    public final static int SORTBY_TITLE_DESCENDING = 1;
    public final static int SORTBY_TITLE_ACENDING = 2;
    public final static int SORTBY_KEY_AGE = 3;
    
    /**
     * Class Constructor
     * @param database File location of the data containing keys for the VaultGUI
     * @param encrypt Encryption placed upon the file
     */
    public KeyVault(File database, File recordLog, Encryption encrypt) {
        this.encrypt = encrypt;
        this.database = database;
        records = recordLog;
    }
    
    /**
     * Reads the keys in from the file and stores them in an Array
     * @return ArrayList<KeyDetails> containing all the data from the file
     * @throws InvalidAlgorithmParameterException 
     * @throws InvalidKeyException 
     * @throws FileNotFoundException 
     * @throws IOException 
     */
    public ArrayList<AccountDetails> loadKeys() throws IntegrityException, InvalidKeyException, InvalidAlgorithmParameterException, FileNotFoundException {
    	ArrayList<AccountDetails> keys = new ArrayList<AccountDetails>();
    	if (Files.exists(Paths.get(database.getAbsolutePath()))) {
    		BufferedInputStream inBufferedFile = new BufferedInputStream(new FileInputStream(database));
    		try {
    			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    			encrypt.decryptStream(inBufferedFile, outStream);
    			inBufferedFile.close();
    			try {
    				ObjectInputStream objInStream = new ObjectInputStream(new ByteArrayInputStream(outStream.toByteArray()));
    				try {
    					RecordKeeper rk = new RecordKeeper(encrypt, records);
    					AccountDetails key = (AccountDetails) objInStream.readObject();
    					while (key != null) {
    						key.addEncryption(encrypt);
    						key.addRecordKeeper(rk);
    						keys.add(key);
    						key = (AccountDetails) objInStream.readObject();
    					}
    				} catch (IOException | ClassNotFoundException e) {
    					e.printStackTrace();
    				} finally {
    					objInStream.close();
    				}
    			} catch (IOException ex) {
    				ex.printStackTrace();
    			}
    		} catch (IOException e) {
    			e.printStackTrace();
    			//System.out.println("Database For Passwords Not Found");
    		}
    	}
    	
    	for (AccountDetails key : keys) {
    		if (!key.checkIntegrity()) {
    			throw new IntegrityException("Integrity Check Failed \n " + key.getID());
    		}
    	}
    	return sortKeys(keys, SORTBY_TITLE_ACENDING);
    }
    
    /**
     * Method to sort and return the ArrayList of KeyDetails
     * @param keys ArrayList of KeyDetails
     * @param sort sorting method to apply
     * @return sorted ArrayList of KeyDetails
     */
    public ArrayList<AccountDetails> sortKeys(ArrayList<AccountDetails> keys, int sort) {
    	switch(sort) {
    	case SORTBY_TITLE_ACENDING:
    		Collections.sort(keys, (kd1, kd2) -> String.valueOf(kd1.getTitle()).compareTo(String.valueOf(kd2.getTitle())));
    		break;
    	case SORTBY_TITLE_DESCENDING:
    		Collections.sort(keys, (kd1, kd2) -> String.valueOf(kd1.getTitle()).compareTo(String.valueOf(kd2.getTitle())));
    		Collections.reverse(keys);
    		break;
    	case SORTBY_KEY_AGE:
    		Collections.sort(keys, new Comparator<AccountDetails>(){
				@Override
				public int compare(AccountDetails kd1, AccountDetails kd2) {
					return Integer.compare((int) kd1.getAge(), (int) kd2.getAge());
				}
    		});
    	}
    	return keys;
    }
    
    /**
     * Writes the ArrayList of KeyDetails back to the file
     * @param keys ArrayList<KeyDetails>
     */
    public void writeKeys(ArrayList<AccountDetails> keys) throws IOException {
    	if (keys != null) {
    		ByteArrayOutputStream bStream = new ByteArrayOutputStream();
    		ObjectOutputStream outStream = new ObjectOutputStream(bStream);
    		try {
    			for (AccountDetails kd : keys) {
    				outStream.writeObject(kd);
    			}
    			outStream.writeObject(null);
    		} catch (IOException e) {
    			//Leave IOException
    		} finally {
    			outStream.flush();
    			outStream.close();
    		}
    		try {
    			encrypt.encryptBytes(bStream.toByteArray(), database);
    		} catch (InvalidKeyException e) {
    			e.printStackTrace();
    		}
    	}
    }
       
    /**
     * Changes the Encryption used to load and store the keys
     * @param encryptNew New Encryption to use
     * @throws IOException 
     * @throws InvalidAlgorithmParameterException 
     * @throws InvalidKeyException 
     */
    public void changeVaultKey(Encryption encryptNew) throws IntegrityException, InvalidKeyException, InvalidAlgorithmParameterException, IOException {
    	ArrayList<AccountDetails> keys = loadKeys();
    	for (AccountDetails key : keys) {
    		key.changeEncryption(encryptNew);
    	}
        encrypt.destroy();
    	encrypt = encryptNew;
    	writeKeys(keys);
    }

    public void closeVault(ArrayList<AccountDetails> keys) {
    	for (AccountDetails kd : keys) {
    		kd.destroy();
    	}    	
    }
}
