package cryptoPrototype.startmenu;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cryptoPrototype.JImageButton;

public class TaskBar extends JPanel implements PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1836349467872902777L;
	
	private ArrayList<MenuItem> taskbarItems;
	private boolean itemsEnabled;
	
	
	public TaskBar() {
		super(new FlowLayout(FlowLayout.LEFT));
		FlowLayout flow = (FlowLayout) getLayout();
		flow.setHgap(0);
		flow.setVgap(0);
		itemsEnabled = true;
	}
	
	private void loadButtons() {
		String str = "/Images/taskbar/";
		
		JLabel lblStart = new JLabel();
		JLabel lblend = new JLabel();
		
		try {
			ImageIcon img = new ImageIcon(ImageIO.read(getClass().getResource(str + "taskbar-left.png").openStream()));
			lblStart.setIcon(new ImageIcon(img.getImage().getScaledInstance(20, 50, Image.SCALE_SMOOTH)));
			img = new ImageIcon(ImageIO.read(getClass().getResource(str + "taskbar-right.png").openStream()));
			lblend.setIcon( new ImageIcon(img.getImage().getScaledInstance(20, 50, Image.SCALE_SMOOTH)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		lblStart.setPreferredSize(new Dimension(20, 50));
		add(lblStart);
		
		for (MenuItem m : taskbarItems) {
			if (m.isFavourite()) {
				JImageButton btnItem = new JImageButton(m.getProgramImage(), m.getOnHoverImage());
				btnItem.setMnemonic(m.getMnemonic());
				btnItem.addActionListener( new ActionListener () {
					@Override
					public void actionPerformed(ActionEvent e) {
						if (itemsEnabled) {
							m.startprogram();
						}
					}
				});
				btnItem.setName(m.getProgramName()); btnItem.addPropertyChangeListener(this);
				btnItem.setPreferredSize(new Dimension(50,50));
				btnItem.setBorder(BorderFactory.createEmptyBorder());
				btnItem.setToolTipText(m.getToolTip());
				add(btnItem);
			}
		}
		lblend.setPreferredSize(new Dimension(20, 50));
		add(lblend);
	}
	
	public void setEnableItems(boolean flag) {
		itemsEnabled = flag;
	}
	
	public void setItems(ArrayList<MenuItem> items) {
		taskbarItems = items;
		removeAll();
		loadButtons();
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		/*
		if (evt.getPropertyName().equals("ButtonClicked")) {
			if (evt.getSource() instanceof JImageButton) {
				JImageButton btn = (JImageButton) evt.getSource();
				for (MenuItem m : taskbarItems) {
					if (m.getProgramName().equals(btn.getName())) {
						m.startprogram();
						break;
					}
				}
			}			
		}*/
	}
	
	
}
