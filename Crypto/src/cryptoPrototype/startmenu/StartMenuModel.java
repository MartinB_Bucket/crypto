package cryptoPrototype.startmenu;

import java.awt.Image;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import cryptoPrototype.Encryption;
import cryptoPrototype.FilePaths;
import cryptoPrototype.Settings;
import cryptoPrototype.cryptoPad.controller.ControllerPad;
import cryptoPrototype.cryptoPad.view.CryptoPadGUI;
import cryptoPrototype.cryptoPictureViewer.controller.ControllerPicViewer;
import cryptoPrototype.cryptoPictureViewer.view.PictureViewer;
import cryptoPrototype.fileExplorer.controller.ControllerExplorer;
import cryptoPrototype.fileExplorer.model.GVFileHandler;
import cryptoPrototype.fileExplorer.view.ExplorerGUI;
import cryptoPrototype.keyVault.controller.ControllerKeyVault;
import cryptoPrototype.keyVault.view.VaultGUI;
import cryptoPrototype.passwordGenerator.controller.ControllerPassGen;
import cryptoPrototype.passwordGenerator.view.PasswordGeneratorGUI;

/**
 * This class is used to hold all the programs and there image path links to be loaded to either a startmenu or taskbar.
 * @author Martin Birch
 */
public class StartMenuModel implements FilePaths {
	private ArrayList<MenuItem> items;
	
	//private StartMenu m;
	private PropertyChangeListener p;
	private Encryption encrypt;
	
	/**
	 * Class Constructor, creates all the program information. The PropertyChangeListener is notified
	 * when any changes are made to any of the MenuItem, such as closing and opening of programs.
	 * @param p PropertyChangeListener
	 * @param encrypt Encryption currently being used
	 */
	public StartMenuModel(PropertyChangeListener p, Encryption encrypt) {
		this.encrypt = encrypt;
		//this.m = m;
		this.p = p;
		items = new ArrayList<MenuItem>();
		createItems();
	}
	
	/**
	 * Gets an ArrayList of MenuItems of all the programs to be added to either a startmenu or taskbar
	 * @return ArrayList<MenuItem>
	 */
	public ArrayList<MenuItem> getItems() {
		return items;
	}
	
	private void createItems() {
		items.add(createPasswordVault());
		items.add(createExplorer());
		items.add(createPasswordGenerator());
		items.add(createNotePad());
		items.add(createPictureViewer());
		items.add(createSettings());
	}
	
	private MenuItem createPasswordVault() {
		MenuItem passwordVault = new MenuItem( new Startable() {

			@Override
			public void startprogram() {
				try {
					VaultGUI vaultGUI = new VaultGUI();
					vaultGUI.addPropertyListener(p);
					ControllerKeyVault vaultController = new ControllerKeyVault(vaultGUI, DATABASE_VAULT, encrypt);
					vaultGUI.addController(vaultController);
					vaultController.refreshKeys();
					vaultGUI.setVisible(true);
				} catch (InvalidKeyException e) {
					JOptionPane.showMessageDialog(null, "Incompatible Password", 
							"Error Opening File", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				} catch (InvalidAlgorithmParameterException e) {
					JOptionPane.showMessageDialog(null, "Incompatible File", 
							"Error Opening File", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				} catch (FileNotFoundException e) {
					JOptionPane.showMessageDialog(null, "No Account Database Found", 
							"Error Opening File", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}

			@Override
			public String getProgramName() {
				return "VaultGUI";
			}

			@Override
			public Image getProgramImage() {
				return new ImageIcon(getClass().getResource("/Images/taskbar/decryptedTaskBar.png")).getImage();
			}

			@Override
			public Image getOnHoverImage() {
				return new ImageIcon(getClass().getResource("/Images/taskbar/decryptedHoverTaskBar.png")).getImage();
			}
			
		}, p);
		
		passwordVault.setFavourite(true);
		passwordVault.setToolTip("Password Vault");
		passwordVault.setTaskBarOnly(false);
		passwordVault.setMnemonic(KeyEvent.VK_P);
		
		return passwordVault;
	}

	private MenuItem createExplorer() {
		MenuItem explorer = new MenuItem( new Startable() {

			@Override
			public void startprogram() {
				if (!Files.exists(ROOT_DIRECTORY)) {
					try {
						Files.createDirectory(ROOT_DIRECTORY);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				GVFileHandler fileHandler = new GVFileHandler(encrypt);
				fileHandler.setDirectory(ROOT_DIRECTORY);
				fileHandler.lockToDirectory(true);
				ExplorerGUI browser = new ExplorerGUI(fileHandler, encrypt); 
				browser.addPropertyListener(p);
				ControllerExplorer explorer = new ControllerExplorer(browser, encrypt, fileHandler);
				browser.addController(explorer);
				browser.setVisible(true);
			}

			@Override
			public String getProgramName() {
				return "FileExplorer";
			}

			@Override
			public Image getProgramImage() {
				return new ImageIcon(getClass().getResource("/Images/taskbar/ComputerTaskBar.png")).getImage();
			}

			@Override
			public Image getOnHoverImage() {
				return new ImageIcon(getClass().getResource("/Images/taskbar/ComputerHoverTaskBar.png")).getImage();
			}
			
		}, p);
		
		explorer.setFavourite(true);
		explorer.setToolTip("File Explorer");
		explorer.setTaskBarOnly(false);
		explorer.setMnemonic(KeyEvent.VK_E);
		
		return explorer;
	}

	private MenuItem createPasswordGenerator() {
		MenuItem passwordGenerator = new MenuItem( new Startable() {

			@Override
			public void startprogram() {
				PasswordGeneratorGUI passGen = new PasswordGeneratorGUI();
				passGen.addPropertyChangeListener(p);
				ControllerPassGen con = new ControllerPassGen(passGen);
				passGen.addController(con);
				passGen.setVisible(true);
			}

			@Override
			public String getProgramName() {
				return "PasswordGenerator";
			}

			@Override
			public Image getProgramImage() {
				return new ImageIcon(getClass().getResource("/Images/taskbar/keygenTaskBar.png")).getImage();
			}

			@Override
			public Image getOnHoverImage() {
				return new ImageIcon(getClass().getResource("/Images/taskbar/keygenHoverTaskBar.png")).getImage();
			}
			
		}, p);
		
		passwordGenerator.setFavourite(true);
		passwordGenerator.setToolTip("Password Generator");
		passwordGenerator.setTaskBarOnly(false);
		passwordGenerator.setMnemonic(KeyEvent.VK_G);
		
		return passwordGenerator;
	}

	private MenuItem createNotePad() {
		MenuItem notePad = new MenuItem( new Startable() {

			@Override
			public void startprogram() {
				CryptoPadGUI view = new CryptoPadGUI();
				view.addPropertyChangeListener(p);
				ControllerPad controller = new ControllerPad(view, ROOT_DIRECTORY, encrypt);
				view.addController(controller);
				view.setVisible(true);
			}

			@Override
			public String getProgramName() {
				return "notePad";
			}

			@Override
			public Image getProgramImage() {
				return new ImageIcon(getClass().getResource("/Images/taskbar/padTaskBar.png")).getImage();
			}

			@Override
			public Image getOnHoverImage() {
				return new ImageIcon(getClass().getResource("/Images/taskbar/padHoverTaskBar.png")).getImage();
			}
			
		}, p);
		
		notePad.setFavourite(true);
		notePad.setToolTip("Note Pad");
		notePad.setTaskBarOnly(false);
		notePad.setMnemonic(KeyEvent.VK_N);
		
		return notePad;
	}

	private MenuItem createPictureViewer() {
		MenuItem pictureViewer = new MenuItem( new Startable() {

			@Override
			public void startprogram() {
				PictureViewer view = new PictureViewer();
				view.addPropertyChangeListener(p);
				ControllerPicViewer controller = new ControllerPicViewer(view, ROOT_DIRECTORY, encrypt);
				view.addController(controller);
				view.setVisible(true);
			}

			@Override
			public String getProgramName() {
				return "PictureViewer";
			}

			@Override
			public Image getProgramImage() {
				return new ImageIcon(getClass().getResource("/Images/taskbar/PicturesViewerTaskBar.png")).getImage();
			}

			@Override
			public Image getOnHoverImage() {
				return new ImageIcon(getClass().getResource("/Images/taskbar/PicturesViewerHoverTaskBar.png")).getImage();
			}
			
		}, p);
		
		pictureViewer.setFavourite(true);
		pictureViewer.setToolTip("Picture Viewer");
		pictureViewer.setTaskBarOnly(false);
		pictureViewer.setMnemonic(KeyEvent.VK_V);
		
		return pictureViewer;
	}

	private MenuItem createSettings() {
		MenuItem settings = new MenuItem( new Startable() {

			@Override
			public void startprogram() {
				Settings view = new Settings(p);
				view.setVisible(true);
			}

			@Override
			public String getProgramName() {
				return "SettingsMenu";
			}

			@Override
			public Image getProgramImage() {
				return new ImageIcon(getClass().getResource("/Images/taskbar/settingsTaskBar.png")).getImage();
			}

			@Override
			public Image getOnHoverImage() {
				return new ImageIcon(getClass().getResource("/Images/taskbar/settingsHoverTaskBar.png")).getImage();
			}
			
		}, p);
		
		settings.setFavourite(true);
		settings.setToolTip("Settings");
		settings.setTaskBarOnly(true);
		settings.setMnemonic(KeyEvent.VK_S);
		
		return settings;
	}
}
