package cryptoPrototype.startmenu;

import java.awt.Image;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
/**
 * This class is used to provide a way of encapsulating the data about the programs that are to be added to the startmenu or taskbar.
 * @author Martin Birch
 */
public class MenuItem implements Startable {
	
	private Startable program;
	private boolean favourite;
	private String tooltip;
	private boolean isTaskBarOnly;
	private int shortcutKey;
	
	//private StartMenu m;
	private PropertyChangeSupport pSupport;
	
	/**
	 * Class Constructor
	 * @param s Startable
	 */
	public MenuItem(Startable s, PropertyChangeListener p) {
		program = s;
		//this.m = m;
		pSupport = new PropertyChangeSupport(this);
		pSupport.addPropertyChangeListener(p);
	}

	public void setMnemonic(int vkP) {
		shortcutKey = vkP;
	}
	
	public int getMnemonic() {
		return shortcutKey;
	}
	
	public boolean isFavourite() {
		return favourite;
	}
	
	public void setFavourite(boolean flag) {
		favourite = flag;
	}
	
	public void setToolTip(String s) {
		tooltip = s;
	}
	
	public String getToolTip() {
		return tooltip;
	}
	
	public boolean isTaskBarOnly() {
		return isTaskBarOnly;
	}
	
	public void setTaskBarOnly(boolean flag) {
		isTaskBarOnly = flag;
	}
	
	@Override
	public void startprogram() {
		Thread thread = new Thread( new Runnable() {
			@Override
			public void run() {
				//m.play();
				pSupport.firePropertyChange("ProgramLoading", false, true);
				program.startprogram();
				pSupport.firePropertyChange("ProgramLoading", true, false);
				//m.stop();
				pSupport.firePropertyChange("ApplicationOpened", false, true);
			}
		});
		thread.start();
	}
	
	@Override
	public String getProgramName() {
		return program.getProgramName();
	}
	
	@Override
	public Image getProgramImage() {
		return program.getProgramImage();
	}

	@Override
	public Image getOnHoverImage() {
		return program.getOnHoverImage();
	}
}
