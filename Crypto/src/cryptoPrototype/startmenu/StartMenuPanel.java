package cryptoPrototype.startmenu;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import cryptoPrototype.Encryption;
import cryptoPrototype.FilePaths;

public class StartMenuPanel extends JPanel implements FilePaths {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1082571865063315819L;

	private TaskBar taskbar;
	private BufferedImage imgBuff = null;
	private Encryption encrypt;
	
	public StartMenuPanel(PropertyChangeListener listener, Encryption encrypt) {
		super(new BorderLayout());
		this.encrypt = encrypt;
		taskbar = new TaskBar();
		
		StartMenuModel menuItems = new StartMenuModel(listener, encrypt);

		taskbar.setItems(menuItems.getItems());

		taskbar.setOpaque(false);
		if (Files.exists(DEFAULT_BACKGROUND_IMAGE)) {
			try {
				try {
					imgBuff = ImageIO.read(new ByteArrayInputStream(encrypt.decryptToBytes(DEFAULT_BACKGROUND_IMAGE.toFile())));							
				} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
					e.printStackTrace();
				}
			} catch (IOException e) {
				
			}
		}
		
		if (imgBuff == null) {
			loadDefaultBackground();
		}

		JPanel pnlCenter = new JPanel(new FlowLayout(FlowLayout.CENTER));
		pnlCenter.add(taskbar);
		pnlCenter.setOpaque(false);
		add(pnlCenter, BorderLayout.SOUTH);
	}
	
	public void setEnableTaskBarItems(boolean flag) {
		taskbar.setEnableItems(flag);
	}
	
	private void loadDefaultBackground() {
		try {
			imgBuff = ImageIO.read(getClass().getResource("/Images/55285815188cc.jpg").openStream());
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	private static boolean testImage(Path path, Encryption encrypt) {
		try {
		    Image image = ImageIO.read(  new ByteArrayInputStream(
		    		encrypt.decryptToBytes( path.toFile() )  )  );
		    if (image == null) {
		    	JOptionPane.showMessageDialog(null, "The file "+ path.getFileName().toString() +
		    			" could not be opened , it is not an image", "Could Not Change Background", 
		    			JOptionPane.ERROR_MESSAGE);
		        return false;
		    } else {
		    	return true; 	
		    }
		} catch(IOException | InvalidKeyException | InvalidAlgorithmParameterException ex) {
	    	JOptionPane.showMessageDialog(null, "The file "+ path.getFileName().toString() +
	    			" could not be opened , an error occurred.", "Could Not Change Background", 
	    			JOptionPane.ERROR_MESSAGE);
		    return false;
		}
	}
	
	public static void setDefaultImage(Path path, Encryption encrypt) {
		try {
			if (testImage(path, encrypt)) {
				if (Files.exists(DEFAULT_BACKGROUND_IMAGE)) {
					Files.delete(DEFAULT_BACKGROUND_IMAGE);
				}
				if (path.startsWith(ROOT_DIRECTORY.toRealPath())) {
					Files.copy(path, DEFAULT_BACKGROUND_IMAGE);					

				} else {					
					encrypt.encryptFile(path.toFile(), DEFAULT_BACKGROUND_IMAGE.toFile());				
				}
			}
		} catch (InvalidKeyException | IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void resetDefaultImage() {
		try {
			Files.delete(DEFAULT_BACKGROUND_IMAGE);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void refreshDisplay() {
		if (Files.exists(DEFAULT_BACKGROUND_IMAGE)) {
			try {
				try {
					imgBuff = ImageIO.read(new ByteArrayInputStream(encrypt.decryptToBytes(DEFAULT_BACKGROUND_IMAGE.toFile())));							
				} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
					e.printStackTrace();
				}
			} catch (IOException e) {
				
			}
		}
		
		if (imgBuff == null) {
			loadDefaultBackground();
		}
		super.repaint();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(imgBuff,0,0, getWidth(), getHeight(), null);
	}
}
