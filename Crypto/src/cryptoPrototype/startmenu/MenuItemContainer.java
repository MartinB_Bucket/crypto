package cryptoPrototype.startmenu;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

/**
 * This class is used by the startmenu to store the positioning information for a program.
 * @author Martin Birch
 */
public class MenuItemContainer {
	
	private int x;
	private int y;
	private int height;
	private int width;
	
	private Image bufferedImage;
	private Image bufferedImageHover;
	
	private MenuItem m;
	
	public MenuItemContainer(MenuItem m) {
		this.m = m;
	}
	
	/**
	 * Sets the x position
	 * @param x int
	 */
	public void setX(int x){
		this.x = x;
	}

	/**
	 * Sets the y position
	 * @param y int
	 */
	public void setY(int y){
		this.y = y;
	}
	
	/**
	 * Sets the width
	 * @param width int
	 */
	public void setWidth(int width){
		this.width = width;
	}
	
	/**
	 * Sets the height
	 * @param height int
	 */
	public void setHeight(int height){
		this.height = height;
	}
	
	/**
	 * Gets the height
	 * @return height int
	 */
	public int getHeight(){
		return height;
	}
	
	/**
	 * Gets the width
	 * @return width int
	 */
	public int getWidth(){
		return width;
	}
	
	/**
	 * Gets the x position
	 * @return x position
	 */
	public int getX(){
		return x;
	}
	
	/**
	 * Gets the y position
	 * @return y position
	 */
	public int getY(){
		return y;
	}
	
	/**
	 * Get the MenuItem encapsulated
	 * @return MenuItem
	 */
	public MenuItem getMenuItem() {
		return m;
	}
	
	
	private BufferedImage resizeImage(Image img, int width, int height) {
		boolean preserveAlpha = false;
		int imageType = preserveAlpha ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
		BufferedImage imgNEW = new BufferedImage(width, height, imageType);
		Graphics2D g = imgNEW.createGraphics();
		g.drawImage(img, 0, 0, width, height, null);
		g.dispose();
		return imgNEW;
	}
	
	/**
	 * Gets the Image to display to represent the program. This method also buffers the calls made.
	 * @param width of the image to return
	 * @param height of the image to return
	 * @return
	 */
	public Image getImage(int width, int height) {
		if (bufferedImage == null) {
			bufferedImage = resizeImage(m.getProgramImage(), width, height);
			return bufferedImage;
		} else {
			if (bufferedImage.getWidth(null) == width && bufferedImage.getHeight(null) == height) {
				return bufferedImage;
			} else {
				bufferedImage = resizeImage(m.getProgramImage(), width, height);
				return bufferedImage;
			}
		}
	}
	
	/**
	 * Gets the Image to display to represent the program on hover. This method also buffers the calls made.
	 * @param width of the image to return
	 * @param height of the image to return
	 * @return
	 */
	public Image getImageHover(int width, int height) {
		if (bufferedImageHover == null) {
			bufferedImageHover = resizeImage(m.getOnHoverImage(), width, height);
			return bufferedImageHover;
		} else {
			if (bufferedImageHover.getWidth(null) == width && bufferedImageHover.getHeight(null) == height) {
				return bufferedImageHover;
			} else {
				bufferedImageHover = resizeImage(m.getOnHoverImage(), width, height);
				return bufferedImageHover;
			}
		}
	}
}
