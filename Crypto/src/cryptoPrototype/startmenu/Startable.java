package cryptoPrototype.startmenu;

import java.awt.Image;
/**
 * An interface used to define a valid item that can be added to the StartMenu
 * @author Martin Birch
 */
public interface Startable {
	/**
	 * Method used to start the running the of program, all methods and/or objects needed
	 *  to successfully create the program and display it should be placed here.
	 */
	public void startprogram();
	/**
	 * Gets the Name of this program
	 * @return String name of the program
	 */
	public String getProgramName();
	/**
	 * Gets the image that should be displayed to represent the program on the StartMenu
	 * @return
	 */
	public Image getProgramImage();
	/**
	 * Gets the image that should appear when the user is hovering over the image
	 * @return Image
	 */
	public Image getOnHoverImage();
}
