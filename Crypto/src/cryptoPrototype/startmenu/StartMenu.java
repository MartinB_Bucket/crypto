package cryptoPrototype.startmenu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Arc2D;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;

import javax.swing.JComponent;

/**
 * A StartMenu capable to holding any reasonable number of items
 * @author Martin Birch
 */
public class StartMenu extends JComponent implements MouseListener, MouseMotionListener {
	
	private static final long serialVersionUID = -2456818806353065680L;
	PropertyChangeSupport pSupport;
	
	private ArrayList<MenuItemContainer> programs;
	private int centerX;
	private int centerY;
	private int margin;
	private int width;
	private int height;
	private int x;
	private int y;
	private int start;
	private boolean run;
	private MenuItemContainer itemHasFocus;
	
	//private Image imgBackground;
	/*
	public static void main(String[] args) {
		JFrame window = new JFrame();
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		StartMenu start = new StartMenu(null);
		start.addItem(new Item("File", new ImageIcon("Images/word.png").getImage(), new ImageIcon("Images/Access.png").getImage()));
		start.addItem(new Item("Access", new ImageIcon("Images/Access.png").getImage(), new ImageIcon("Images/word.png").getImage()));
		start.addItem(new Item("Excel", new ImageIcon("Images/Excel.png").getImage(), new ImageIcon("Images/word.png").getImage()));
		start.addItem(new Item("Computer", new ImageIcon("Images/Powerpoint.png").getImage(), new ImageIcon("Images/word.png").getImage()));
		start.addItem(new Item("Computer2", new ImageIcon("Images/Computer.png").getImage(), new ImageIcon("Images/word.png").getImage()));
		start.addItem(new Item("Excel2", new ImageIcon("Images/Excel.png").getImage(), new ImageIcon("Images/word.png").getImage()));
		window.add(start);
		window.setSize(700, 700);
		window.setVisible(true);
	}*/
	
	/**
	 * Class Constructor
	 * @param listener PropertyChangeListener
	 */
	public StartMenu(PropertyChangeListener listener) {
		//pSupport.addPropertyChangeListener(listener);
		programs = new ArrayList<MenuItemContainer>();
		margin = 10;
		this.x = x + margin;
		this.y = y + margin;
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		
		//imgBackground = new ImageIcon("Images/tiger.png").getImage();
	}
	
	/**
	 * Adds an item to be displayed on the StartMenu
	 * @param object MenuItem
	 */
	public void addItem(MenuItem object) {
		programs.add(new MenuItemContainer(object));
	}
	
	/**
	 * Sets the items to be displayed on the StartMenu
	 * @param items ArrayList<MenuItem>
	 */
	public void setItems(ArrayList<MenuItem> items) {
		for (MenuItem m : items) {
			programs.add(new MenuItemContainer(m));
		}
	}
	
	/**
	 * Used to increment the loading image
	 */
	private void increment() {
		if (start < 12) {
			start++;
		} else {
			start = 1;
		}
		repaint(centerX - 85, centerY - 85, centerX + 85, centerY + 85);
	}
	
	/**
	 * Plays the loading animation
	 */
	public void play() {
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				while (run) {
					try {
						increment();
						Thread.sleep(60);
					} catch (InterruptedException e) {
						
					}
				}
			}
		});
		run = true;
		t1.start();
	}
	
	/**
	 * Stops the loading animation
	 */
	public void stop() {
		run = false;
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		calculateSize();
		g.setColor(Color.BLACK);
		if (programs != null) {
			int amount = 0;
			for (MenuItemContainer m : programs) {
				if (!m.getMenuItem().isTaskBarOnly()) {
					amount++;
				}
			}
			
			if (amount < 4) {
				amount = 4;
			}
			
			double degree = 360 / amount;
			double degreeCount = 0.0;
			int raduis = width / 3;
			
			int imgWidth = 60;
			int imgHeight = 70;
			for (MenuItemContainer item : programs) {
				if (!item.getMenuItem().isTaskBarOnly()) {
					//Image img = item.getMenuItem().getProgramImage();
					Image img = item.getImage(imgWidth, imgHeight);
					if (itemHasFocus != null) {
						if (itemHasFocus.equals(item)) {
							//img = item.getMenuItem().getOnHoverImage();
							img = item.getImageHover(imgWidth, imgHeight);
						}
					}
					Dimension imgLocation = getLocation(degreeCount, raduis+40, imgWidth, imgHeight);

					item.setWidth(imgWidth); item.setHeight(imgHeight);
					item.setX(imgLocation.width); item.setY(imgLocation.height);

					g.drawImage(img, imgLocation.width, imgLocation.height, imgWidth, imgHeight, null);
					degreeCount += degree;
				}
			}
		}
		//g.drawImage(imgBackground, centerX-100, centerY-150, 200, 300, null);
		
		//Paint TaskBar
		if (run) {
			paintLoading(g);
		}
	}
	
	/**
	 * Used to paint the loading animation on the screen
	 * @param g Graphics
	 */
	private void paintLoading(Graphics g) {
		g.setColor(Color.BLACK);
		Color orginalColor = Color.LIGHT_GRAY;
		Color newColor = orginalColor;
		int noCircles = 12;
		int circleSize = 5;
		double angle = 360.0 / noCircles;
		double currentAngle = angle * start;
		int raduis = 85;
		for (int i = 0; i < noCircles; ++i) {
			if (currentAngle > 360) {
				currentAngle = currentAngle - 360;
			}
			newColor = getNewColor(newColor, false, (float) 0.06);
			g.setColor(newColor);
			Dimension circleLocation = getLocation(currentAngle, raduis, circleSize, circleSize);
			g.fillOval(circleLocation.width, circleLocation.height, circleSize, circleSize);
			currentAngle +=angle;
		}
	}
	
	/**
	 * Used to calculate the size and position to draw the StartMenu
	 */
	private void calculateSize() {
		height = getHeight() - margin * 2;
		width = getWidth() - margin * 2;
		if (height > width) {
			height = width;
		} else if (width > height) {
			width = height;
		}
		centerX = (width / 2) + margin;
		centerY = (height / 2) + margin;
	}
	
	/**
	 * Used to get a darker or lighter color than the one provided
	 * @param col Color
	 * @param increment boolean should the decreamentValue be added or subtracted
	 * @param decrementValue value to add or subtract
	 * @return new Color
	 */
	private Color getNewColor(Color col, boolean increment, float decrementValue) {
		int r = col.getRed();
		int g = col.getGreen();
		int b = col.getBlue();
		float[] hsb = Color.RGBtoHSB(r, g, b, null);
		if (increment) {
			if(hsb[2] < 1.0) {
				hsb[2] += decrementValue;
			}
		} else {
			if(hsb[2] > 0.0) {
				hsb[2] -= decrementValue;
			}
		}
		Color newColor = Color.getHSBColor(hsb[0], hsb[1], hsb[2]);
		return newColor;
	}
	
	/**
	 * 
	 * @param g
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param startDegree
	 * @param endDegree
	 */
	public void drawCircle(Graphics2D g, double x, double y, double width, double height, double startDegree, double endDegree) {
		//g.fill(new Arc2D.Double(x, y, pieSize.width, pieSize.height, startDegree, -currentDegree, Arc2D.PIE));
		Color orginalColor = g.getColor();
		int renderAmount = this.width / 10;
		if (renderAmount > 40) {
			renderAmount = 40;
		}
		Color newColor = orginalColor;
		for (int i = 0; i < renderAmount; ++i) {
			newColor = getNewColor(newColor, false, (float) 0.01);
		}
		double newHeight = height;
		double newWidth = width;
		double newX = x;
		double newY = y;
		for (int i = 0; i < renderAmount; ++i) {
			newColor = getNewColor(newColor, true, (float) 0.01);
			g.setColor(newColor);
			g.fill(new Arc2D.Double(newX, newY, newWidth, newHeight, startDegree, endDegree, Arc2D.PIE));
			newHeight-= 2; newY++;
			newWidth-= 2; newX++;
		}
	}
	
	/**
	 * Can be used to draw a shaded background circle
	 * @param g Graphics
	 */
	@SuppressWarnings("unused")
	private void drawCircle(Graphics g) {
		Graphics2D g2 = (Graphics2D) g.create();
		Color orginalColor = g2.getColor();
		int renderAmount = 30;
		Color newColor = orginalColor;
		
		for (int i = 0; i < renderAmount; ++i) {
			newColor = getNewColor(newColor, false, (float) 0.01);
		}
		
		int newHeight = height;
		int newWidth = width;
		int newX = x;
		int newY = y;
		g.setColor(newColor); g.fillOval(newX, newY, newWidth, newHeight);
		int changeColor = 2;
		for (int i = 0; i < (renderAmount * 2); ++i) { 
			if (i >= changeColor) {
				newColor = getNewColor(newColor, true, (float) 0.01);
				g.setColor(newColor);
				g.fillOval(newX, newY, newWidth, newHeight);
				changeColor = (int) Math.pow(i, 1.034);
			} else {
				if (i % 3 == 0) {
					newColor = getNewColor(newColor, true, (float) 0.01);
				}
			}
			newHeight-= 2; newY++;
			newWidth-= 2; newX++;
		}
		
		g.setColor(orginalColor);
		//g.fillOval(newX, newY, newWidth, newHeight);
		g.setColor(Color.BLACK);
		g.drawOval(x, y, width, height);
		
	}
	
	/**
	 * Gets the longest width from the ArrayList of String provided
	 * @param string ArrayList<String> to be analysed
	 * @param g Graphics
	 * @return int containing the width of the longest string
	 */
	public int getTextWidth(String s, Graphics g) {
		return g.getFontMetrics().stringWidth(s);
	}
	
	/**
	 * Is used to find the appropriate location to draw a String around the edge of a circle
	 * @param angle The angle to text is to be drawn around the circle
	 * @param raduis The distance away from the circle the label is to be drawn
	 * @param txt The text that is to be drawn
	 * @param g The Graphics that will draw the text
	 * @return Dimension at which the text should be draw x,y
	 */
	private Dimension getLocation(double angle, int raduis, int width, int height) {
		Dimension labelLocation = getLocation(angle, raduis);
		int x = labelLocation.width;
		int y = labelLocation.height;
		
		if (angle == 0 || angle == 360) { 
			labelLocation.setSize(x - (width / 2), y);
		} else if (angle < 90) {
			labelLocation.setSize(x - width, y);
	    } else if (angle == 90) {
	    	labelLocation.setSize(x - width, y - (height / 2));
	    } else if (angle > 90 && angle < 180) {
	    	labelLocation.setSize(x - width, y - height);
		} else if (angle == 180) {
			labelLocation.setSize(x - (width / 2), y - height); 
		} else if (angle > 180 && angle < 270) {
			labelLocation.setSize(x, y - height); 
		} else if (angle == 270) {
			labelLocation.setSize(x, y - (height / 2));
		} else if (angle > 270) {
			labelLocation.setSize(x, y);
		}
		
		return labelLocation;
	}
	
    /**
     * Is used to get coordinates from a position around a circle
     * @param position of the coordinate around the circle to retrieve
     * @param circleDividedAmount the amount the circle should be split up to produce the amount of possible positions around the circle
     * @param raduis the length away from the raduis of the circle
     */
    private Dimension getLocation(double angle, int raduis) {
        int x = centerX;
        int y = centerY;
        
        if (angle == 0 || angle == 360) {
        	return new Dimension(x, y - raduis); 
        } else if (angle == 90) {
        	return new Dimension(x + raduis, y); 
        } else if (angle == 180) {
        	return new Dimension(x, y + raduis); 
        } else if (angle == 270) {
        	return new Dimension(x - raduis, y); 
        }
        
        double length = 0.0;
        double height = 0.0;
        if (angle < 90) {
        	double v = 90.0 - angle;
        	v = v / (180/Math.PI);
        	length = Math.cos(v) * raduis;
        	height = Math.sin(v) * raduis;
        	x += length;
        	y -= height;
        } else if (angle > 90 && angle < 180) {
        	double v = angle - 90;
        	v = v / (180/Math.PI);
        	length = Math.cos(v) * raduis;
        	height = Math.sin(v) * raduis;
        	x += length;
        	y += height;
        } else if (angle > 180 && angle < 270) {
        	double z = angle - 180;
        	double v = 90.0 - z;
        	v = v / (180/Math.PI);
        	length = Math.cos(v) * raduis;
        	height = Math.sin(v) * raduis; 
        	x -= length;
        	y += height;
        } else if (angle > 270) {
        	double z = 360 - angle;
        	double v = 90.0 - z;
        	v = v / (180/Math.PI);
        	length = Math.cos(v) * raduis;
        	height = Math.sin(v) * raduis;
        	x -= length;
        	y -= height;
        }
        return new Dimension(x, y);
    }

    /**
     * Gets the MenuItem that the mouse is currently hovering over, returns null
     * if it is not hovering over any item
     * @param mouseX int location
     * @param mouseY int location
     * @return MenuItem or null, if the mouse is currently over a MenuItem or not
     */
    private MenuItemContainer isOnProgram(int mouseX, int mouseY) {
    	for (MenuItemContainer item : programs) {
    		if (isOnItem(item, mouseX, mouseY)) {
    			return item;
    		}
    	}
    	return null;
    }
    
    /**
     * Tests whether the mouse is on a specific MenuItem
     * @param item MenuItem to test
     * @param mouseX int location
     * @param mouseY int location
     * @return true or false, if the mouse is or isn't on the item
     */
    private boolean isOnItem(MenuItemContainer item, int mouseX, int mouseY) {
    	return ((mouseX >= item.getX()) && (mouseX <= (item.getX() + item.getWidth())) &&
				(mouseY >= item.getY()) && (mouseY <= (item.getY() + item.getHeight())));
    }
    
	@Override
	public void mouseClicked(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		MenuItemContainer item = isOnProgram(mouseX, mouseY);
		if (item != null) {
			item.getMenuItem().startprogram();
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		if (itemHasFocus != null) {
			if (!isOnItem(itemHasFocus, mouseX, mouseY)) {
				repaint(itemHasFocus.getX(), itemHasFocus.getY(), itemHasFocus.getWidth(), itemHasFocus.getHeight());
				itemHasFocus = null;
			}
		} else {
			MenuItemContainer item = isOnProgram(mouseX, mouseY);
			if (item != null) {
				itemHasFocus = item;
				repaint(itemHasFocus.getX(), itemHasFocus.getY(), itemHasFocus.getWidth(), itemHasFocus.getHeight());
			}
		}
	}

	
}
