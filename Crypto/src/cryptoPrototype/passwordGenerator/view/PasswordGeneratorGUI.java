package cryptoPrototype.passwordGenerator.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cryptoPrototype.passwordGenerator.controller.ControllerPassGen;
/**
 * A Window that allows the user to create and copy a random generated password
 * @author Martin Birch
 */
public class PasswordGeneratorGUI {
	
	private PropertyChangeSupport pSupport;
	private JTextField txtResultOutput;
	private JTextField txtBlocks;
	private JTextField txtBlockLength;
	private JFrame frame;
	private ControllerPassGen controller;
	
	public static void main(String[] args) {
		PasswordGeneratorGUI genGUI = new PasswordGeneratorGUI();
		ControllerPassGen con = new ControllerPassGen(genGUI);
		genGUI.addController(con);
		genGUI.setVisible(true);
	}
	
	/**
	 * Class Constructor
	 */
	public PasswordGeneratorGUI() {
		pSupport = new PropertyChangeSupport(this);
		frame = new JFrame();
		frame.setTitle("Password Generator");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	/**
	 * Sets the visibility of the window
	 * @param visible true or false
	 */
	public void setVisible(boolean visible) {
		createGUI();
		frame.setLocationRelativeTo(null);
		frame.setVisible(visible);
	}
	
	/**
	 * Adds a controller to the class
	 * @param con ControllerPassGen
	 */
	public void addController(ControllerPassGen con) {
		controller = con;
		frame.addWindowListener(controller);
	}
	
	/**
	 * Used to create the layout
	 */
	private void createGUI() {
		frame.getContentPane().removeAll();
		frame.setLayout(new FlowLayout());
		JPanel root = new JPanel(new GridBagLayout());
		root.setBorder(BorderFactory.createEmptyBorder(15, 15, 5, 15));
		GridBagConstraints c = new GridBagConstraints();
		
		JPanel pnlOptions = new JPanel();
		pnlOptions.setLayout(new BoxLayout(pnlOptions, BoxLayout.X_AXIS));
		JLabel lblBlocks = new JLabel("Blocks: ");
		JLabel lblBlockLength = new JLabel("Block Length: ");
		txtBlocks = new JTextField();
		txtBlocks.setPreferredSize(new Dimension(60, 20));
		txtBlockLength = new JTextField();
		txtBlockLength.setPreferredSize(new Dimension(60, 20));
		pnlOptions.add(lblBlocks);
		pnlOptions.add(txtBlocks);
		pnlOptions.add(new JPanel());
		pnlOptions.add(lblBlockLength);
		pnlOptions.add(txtBlockLength);
		
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		root.add(pnlOptions, c);
		
		JPanel pnlResult = new JPanel(new FlowLayout());
		JLabel lblResult = new JLabel("Result: ");
		txtResultOutput = new JTextField();
		txtResultOutput.setPreferredSize(new Dimension(200, 20));
		pnlResult.add(lblResult);
		pnlResult.add(txtResultOutput);
		c.gridx = 0;
		c.gridy = 1;
		root.add(pnlResult, c);
		
		JPanel pnlButton = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JButton btnGenerate = new JButton("Generate"); btnGenerate.setName("Generate");
		JButton btnCopy = new JButton("Copy"); btnCopy.setName("Copy");
		if (controller != null) {
			btnGenerate.addActionListener(controller);
			btnCopy.addActionListener(controller);
		}
		pnlButton.add(btnCopy);
		pnlButton.add(btnGenerate);
		c.gridy = 2;
		
		root.add(pnlButton, c);
		
		frame.add(root);
		frame.pack();
		frame.revalidate();
		frame.repaint();
	}
	
	/**
	 * Adds a Property Change Listener to this class
	 * @param listener PropertyChangeListener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pSupport.addPropertyChangeListener(listener);
	}
	
	/**
	 * Fires a Property Change Event
	 * @param propertyName String
	 * @param o1 old value object
	 * @param o2 new value object
	 */
	public void firePropertyChange(String propertyName, Object o1, Object o2) {
		pSupport.firePropertyChange(propertyName, o1, o2);
	}

	/**
	 * Fires a Property Change Event
	 * @param propertyName String
	 * @param o1 old value boolean
	 * @param o2 new value boolean
	 */
	public void firePropertyChange(String propertyName, boolean o1, boolean o2) {
		pSupport.firePropertyChange(propertyName, o1, o2);
	}
	
	/**
	 * Sets the output text to display
	 * @param str String
	 */
	public void setResultText(String str) {
		txtResultOutput.setText(str);
	}
	
	/**
	 * Gets the amount of blocks the user has entered
	 * @return int blocks
	 */
	public int getBlocks() {
		try {
			if (txtBlocks.getText().length() > 0) {
				return Integer.parseInt(txtBlocks.getText());
			}
		} catch (NumberFormatException e) {
			return 0;
		}
		return 0;
	}
	
	/**
	 * Gets the length of the blocks that the user has entered
	 * @return int length
	 */
	public int getBlockLength() {
		try {
			if (txtBlockLength.getText().length() > 0) {
				return Integer.parseInt(txtBlockLength.getText());
			}
		} catch (NumberFormatException e) {
			return 0;
		}
		return 0;
	}
	
	/**
	 * Gets the text stored in the output JTextArea
	 * @return String
	 */
	public String getResultText() {
		return txtResultOutput.getText();
	}

}
