package cryptoPrototype.passwordGenerator.model;

import java.security.SecureRandom;
/**
 * Used to sequences of random numbers and letters
 * @author Martin Birch
 */
public class Generator {
	
	private SecureRandom secure;
	
	/**
	 * Class Constructor
	 */
	public Generator() {
		secure = new SecureRandom();
	}
	
	/**
	 * Creates a Password with n blocks which each have l length
	 * @param blocks int
	 * @param length int
	 * @return String password
	 */
	public String createPassword(int blocks, int length) {
		StringBuilder str = new StringBuilder();
		for (int k=0; k < blocks; ++k) {
			for (int i = 0; i < length; ++i) {
				str.append(createLetterOrDigit());
			}
			str.append("-");
		}
		if (str.length() > 0) {
			return str.toString().substring(0, str.length()-1);
		}
		return "";
	}
	
	/**
	 * creates a single random letter or digit
	 * @return char letter or digit
	 */
	private char createLetterOrDigit() {
		byte[] bytes = new byte[1];
		while (!Character.isLetterOrDigit(bytes[0])) {
			secure.nextBytes(bytes);			
		}
		return Character.toChars(bytes[0])[0];
	}
	
	@SuppressWarnings("unused")
	private char createLetter() {
		byte[] bytes = new byte[1];
		while (!Character.isLetter(bytes[0])) {
			secure.nextBytes(bytes);			
		}
		return Character.toChars(bytes[0])[0];
	}
	
	@SuppressWarnings("unused")
	private char createDigit() {
		byte[] bytes = new byte[1];
		while (!Character.isDigit(bytes[0])) {
			secure.nextBytes(bytes);			
		}
		return Character.toChars(bytes[0])[0];
	}
	
}
