package cryptoPrototype.passwordGenerator.controller;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;

import cryptoPrototype.passwordGenerator.model.Generator;
import cryptoPrototype.passwordGenerator.view.PasswordGeneratorGUI;
/**
 * A Controller that adds the functionality to the PasswordGeneratorGUI
 * @author Martin Birch
 */
public class ControllerPassGen implements ActionListener, WindowListener {
	private Generator generator;
	private PasswordGeneratorGUI passGUI;
	
	/**
	 * Class Constructor
	 * @param passGUI PasswordGeneratorGUI
	 */
	public ControllerPassGen(PasswordGeneratorGUI passGUI) {
		this.generator = new Generator();
		this.passGUI = passGUI;
	}
	
	@Override
	public void actionPerformed(ActionEvent av) {
		if (av.getSource() instanceof JButton) {
			if (((JButton)av.getSource()).getName().equalsIgnoreCase("Generate")) {
				passGUI.setResultText(generator.createPassword(passGUI.getBlocks(), passGUI.getBlockLength()));
			} else if (((JButton)av.getSource()).getName().equalsIgnoreCase("Copy")) {
				StringSelection selection = new StringSelection(passGUI.getResultText());
        		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        		clipboard.setContents(selection, null);
			}
		}
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		passGUI.firePropertyChange("WindowClosed", false, true);
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

}
