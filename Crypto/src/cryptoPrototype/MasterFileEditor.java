package cryptoPrototype;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This class provides the basic interactions needed to maintain the master files.
 * This will buffer calls made to multiple master file these may be located in different directories.
 * Once changes have been made using this object the close method needs to be 
 * called to ensure the changes are saved and no decrypted files are left on the system.
 * @author Martin Birch
 */
public class MasterFileEditor implements FilePaths {
	// Encryption Object to be used to encrypt and decrypt the master files
	protected Encryption encryption; 
	//Buffer that allows the next file name for a specific master file to be retrieved quickly
	private HashMap<Path, Integer> buffer;
	//Buffer that maps encrypted file names to their unencrypted ones for each master file
	private HashMap<Path, HashMap<Integer, String>> contentBuffer;
	
	/**
	 * Class Constructor
	 * @param encryption Encryption object to be used when encrypting and decrypting files
	 */
	public MasterFileEditor(Encryption encryption) {
		this.encryption = encryption;
		buffer = new HashMap<Path, Integer>();
		contentBuffer = new HashMap<Path, HashMap<Integer, String>>();
	}
	
	/**
	 * Creates a new master file, the path needs to include the name of the master file
	 * @param masterfile Path of the location to create the file
	 */
	public void createMasterFile(Path masterfile) {
		try {
			if (!Files.exists(masterfile.getParent())) {
				Files.createDirectory(masterfile.getParent());
			}
			Files.createFile(masterfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public boolean changeEntryName(Integer entryKey, Path masterFile, String oldFileName, String newFileName) {
		HashMap<Integer, String> fileMap = processMasterFile(masterFile);
		return fileMap.replace(entryKey, oldFileName, newFileName);
	}
	
	/**
	 * This method is used to decrypt a master file
	 * @param master Path of the master file
	 * @return Path of the decrypted master file
	 */
	private byte[] decryptFile(Path master) {
		//Path tempMaster = Paths.get(master.toString() + ".storagedump");
		
		//if (!Files.exists(tempMaster)) {
		if (!contentBuffer.containsKey(master)) {
			try {
				return encryption.decryptToBytes(master.toFile());
			} catch (InvalidKeyException | InvalidAlgorithmParameterException | IOException e) {
				e.printStackTrace();
			}			
		}
		//}
		
		return null;
	}
	
	/**
	 * Used to encrypt a master file that has been decrypted using the decrypt method.
	 * @param master Path of the master file (should point to the encrypted one)
	 */
	private void encryptFile(Path master) {
		//Path tempMaster = Paths.get(master.toString() + ".storagedump");
		
		//if (Files.exists(tempMaster)) {
			//try {
		if (contentBuffer.get(master) != null) {
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			for (Map.Entry<Integer, String> entry : contentBuffer.get(master).entrySet()) {
				writeEntryToStream(entry.getKey(), entry.getValue().getBytes(), outStream);
			}
			try {
				secureDelete(master, 2);
				encryption.encryptBytes(outStream.toByteArray(), master.toFile());
			} catch (InvalidKeyException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
				//Files.delete(tempMaster);
			//} catch (InvalidKeyException | IOException e) {
				//e.printStackTrace();
			//}
		//}
	}

	public static void secureDelete(Path file, int passes) throws IOException {
		if (Files.exists(file)) {
			File fileToDelete = file.toFile();
			long length = fileToDelete.length();
			RandomAccessFile r = new RandomAccessFile(fileToDelete, "rws");
			
			if (passes < 0) {
				passes = 1;
			}
			
			byte byteToWrite = (byte) 0;
			
			while (passes > 0) {
				byte[] bytes = new byte[1024];
				for (int i = 0; i < bytes.length; ++i) {
					bytes[i] = byteToWrite;
				}
				r.seek(0);
				long current = 0;
				
				while ((current < length) && !((current + bytes.length) > length)) {
					r.write(bytes);
					current += bytes.length;
				}
				
				while ((current < length)) {
					r.write(byteToWrite);
					++current;
				}
				
				--passes;
				byteToWrite = (byte) ~byteToWrite;
			}
			
			r.close();
			Files.delete(file);
			//System.out.println(fileToDelete.delete());
		}
	}
	
	/**
	 * Used to close files accessed and save any changes that were made. 
	 */
	public void close() {
		for (Entry<Path, HashMap<Integer, String>> entry : contentBuffer.entrySet()) {
			encryptFile(entry.getKey());
		}
	}
	
	/**
	 * Writes an entry's details to an output stream 
	 * @param key int containing the key number
	 * @param name byte[] containing the name of the file
	 * @param outStream OutputStream to write the data to
	 */
	private void writeEntryToStream(int key, byte[] name, ByteArrayOutputStream outStream) {
		try {
			outStream.write(key);
			outStream.write('?');
			outStream.write(name);
			outStream.write("|||||".getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// ByteArrayOutputStream outStream = new ByteArrayOutputStream();
	//
	/**
	 * Writes a new entry in the master file containing the name provided and returns
	 * the integer representation that will be mapped to it.
	 * @param masterfile Path of the master file to write the new entry to
	 * @param name String name to write to the file
	 * @return int key that maps to the name provided
	 */
	public int writeNewEntry(Path masterfile, String name) {
		int newKey = 0;
		if (buffer.containsKey(masterfile)) {
			newKey = buffer.get(masterfile) + 1;
		} else {
			newKey = getNextNumber(masterfile);
		}
		
		//try {
			//FileOutputStream outStream = new FileOutputStream(decryptFile(masterfile).toFile(), true);
			//writeEntryToFile(newKey, name.getBytes(), masterfile);
			//outStream.close();
		//} catch (IOException e) {
			//e.printStackTrace();
		//}
		
		HashMap<Integer, String> fileMap = contentBuffer.get(masterfile);
		fileMap.put(newKey, name);
		
		buffer.put(masterfile, newKey);
		
		return newKey;
	}
	
	/**
	 * Gets the next highest key value in a master file
	 * @param masterfile Path of master file to get the next key for
	 * @return int representing the new key
	 */
	private int getNextNumber(Path masterfile) {
		HashMap<Integer, String> fileMap = processMasterFile(masterfile);
		
		int i = 0;
		
		for (Integer k : fileMap.keySet()) {
			if (k > i) {
				i = k;
			}
		}
		
		return ++i;
	}
	
	/**
	 * Reads a master file and returns a HashMap containing a mapping of 
	 * encrypted file names to their unencrypted ones.
	 * Calls to this method are buffered.
	 * @param entry Path of the master file to read
	 * @return HashMap<Integer, String>
	 */
	public HashMap<Integer, String> processMasterFile(Path entry) {
		HashMap<Integer, String> fileMap = new HashMap<Integer, String>();
		
		if (contentBuffer.containsKey(entry)) {
			fileMap = contentBuffer.get(entry);
		} else {
			//try {
				//byte[] data = Files.readAllBytes(decryptFile(entry));
				byte[] data = decryptFile(entry);
				
				int markersMet = 0;
				boolean checkHashSeperator = true;
				byte[] keyHolder = new byte[4];
				int indexEmpty = 0;
				StringBuilder name = new StringBuilder();
				
				
				for (int x = 0; x < data.length; ++x ) {
					if (checkHashSeperator) {
						if (data[x] == (byte) 63) {
							checkHashSeperator = !checkHashSeperator;
						} else {
							keyHolder[indexEmpty++] = data[x];
						}
					} else if (data[x] == (byte) 124) { 
						markersMet++;
						if (markersMet == FILE_NAMES_FILE_DELIMITER_AMOUNT) {
							markersMet = 0;
							byte[] bytes = new byte[4];
							
							for (int i =  (4 - indexEmpty); i < 4; ++i) {
								bytes[i] = keyHolder[i - (4 - indexEmpty)];
							}
									
							ByteBuffer intKey = ByteBuffer.wrap(bytes);
							intKey.order(ByteOrder.BIG_ENDIAN);
							int keyValue = intKey.getInt();
							fileMap.put(keyValue, name.substring(0, name.length() - (FILE_NAMES_FILE_DELIMITER_AMOUNT-1)));
							keyHolder = new byte[4];
							indexEmpty = 0;
							name = new StringBuilder();
							checkHashSeperator = !checkHashSeperator;
							
						} else {
							name.append(String.valueOf( (char) data[x]));
						}
					} else {
						name.append(String.valueOf( (char) data[x]));
					}
				}
			//} catch (IOException e) {
				//e.printStackTrace();
			//}
			contentBuffer.put(entry, fileMap);
		}
		
		return fileMap;
	}

	/**
	 * Removes the path entry from it's master file and returns the decrypted file name
	 * @param p Path of the entry to remove
	 * @return String file name
	 */
	public String removeEntry(Path p) {
		Path masterfile = p.getParent().resolve(MASTER_FILE);
		
		HashMap<Integer, String> fileMap = processMasterFile(masterfile);
		
		String value = fileMap.remove(Integer.parseInt(p.getFileName().toString()));
		// Silence of the lambs
		/*try {
			BufferedOutputStream outStream = new BufferedOutputStream(new FileOutputStream(decryptFile(masterfile).toFile(), false));
			for (Map.Entry<Integer, String> entry : fileMap.entrySet()) {
				writeEntryToFile(entry.getKey(), entry.getValue().getBytes(), outStream);				
			}
			outStream.flush();
			outStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		
		return value;
	}
}
