
package cryptoPrototype;
/**
 * Used to specify a standard for the Salt used
 * @author Martin Birch
 */
public interface Salt {
	/**
	 * Used to specify the salt length to be applied to any hashes
	 * Changing this will cause existing hashes to become invalid 
	 */
    public static final int SALT_LENGTH = 8;
}
