package cryptoPrototype;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
/**
 * This object simplifies the encryption, and decryption files and bytes.
 * It requires that a password is given to it for which the encryption is based upon.
 * In each encryption call a random IV is used.
 * @author Martin Birch
 */
public class Encryption extends StreamEncryption {
    /**
     * Class Constructor, creates an Encryption object for the specified password
     * @param letters char[] containing the password to be used for encrypting and decrypting
     */
    public Encryption(char[] letters) {
        super(letters);
    }
    
    /**
     * Class Constructor, creates an Encryption object for the specified password
     * @param letters char[] containing the password to be used for encrypting and decrypting
     * @param readSize int the size of a file to read in one go (bytes), can increase or decrease speed of the encryption and decryption
     */
    public Encryption(char[] letters, int readSize) {
        super(letters, readSize);
    }
    
    /**
     * Class Constructor, creates an Encryption object for the specified password
     * @param letters char[] containing the password to be used for encrypting and decrypting
	 * @param keylength The size of the key to use, 16 for 128bit, 24 for 192bit, and 32 for 256bit
     * @param readSize int the size of a file to read in one go (bytes), can increase or decrease speed of the encryption and decryption
     */
    public Encryption(char[] letters, int keylength, int readSize) {
        super(letters, keylength, readSize);
    }
    /**
     * Encrypts the source file and saves the encrypted file at the destination location
     * @param sourceFile File to encrypt
     * @param destinationFile Location to output encrypted File
     * @throws FileNotFoundException
     * @throws InvalidKeyException
     */
	public void encryptFile(File sourceFile, File destinationFile) throws FileNotFoundException, InvalidKeyException {
		BufferedInputStream inStream = new BufferedInputStream(new FileInputStream(sourceFile.getPath()));
		BufferedOutputStream outStream = new BufferedOutputStream(new FileOutputStream(destinationFile.getPath()));
    	try {
    		encryptStream(inStream, outStream);
    	} finally {
    		try { inStream.close(); outStream.close(); } catch (IOException e) { }
    	}
    }
    
	/**
	 * Changes the password placed on a File
	 * @param sourceFile Location of the file to change
	 * @param destinationFile Location to store the updated file
	 * @param newEncrypt Encryption object containing the new Password
	 * @throws FileNotFoundException
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 */
	public void changeFilePassword(File sourceFile, File destinationFile, Encryption newEncrypt) throws FileNotFoundException, InvalidKeyException, InvalidAlgorithmParameterException {
		BufferedInputStream inStream = new BufferedInputStream(new FileInputStream(sourceFile.getPath()));
		BufferedOutputStream outStream = new BufferedOutputStream(new FileOutputStream(destinationFile.getPath()));
    	try {
    		changeStreamPassword(inStream, outStream, newEncrypt);
    	} finally {
    		try { inStream.close(); outStream.close(); } catch (IOException e) { }
    	}
    }
	
	/**
	 * Encrypts a series of bytes and store the result in a file
	 * @param bytes byte[] to encrypt
	 * @param destinationFile Location to store the encrypted bytes
	 * @throws FileNotFoundException
	 * @throws InvalidKeyException
	 */
    public void encryptBytes(byte[] bytes, File destinationFile) throws FileNotFoundException, InvalidKeyException  {
    	BufferedOutputStream outStream = new BufferedOutputStream(new FileOutputStream(destinationFile.getPath()));
    	try {
    		encryptStream(new ByteArrayInputStream(bytes), outStream);
    	} finally {
    		try { outStream.close(); } catch (IOException e) { }
    	}
    }
    
    /**
     * Encrypts a series of bytes and return the encrypted bytes
     * @param bytes byte[] to encrypt
     * @return byte[] of encrypted bytes
     * @throws FileNotFoundException
     * @throws InvalidKeyException
     */
    public byte[] encryptBytesToBytes(byte[] bytes) throws FileNotFoundException, InvalidKeyException  {
    	ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    	encryptStream(new ByteArrayInputStream(bytes), outStream);
    	return outStream.toByteArray();
    }
    
    /**
     * Decrypts the source file and saves the result at the destination location
     * @param sourceFile Encrypted file
     * @param destinationFile Decrypted File
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws IOException
     */
	public void decryptFile(File sourceFile, File destinationFile) throws InvalidKeyException, InvalidAlgorithmParameterException, IOException {
		BufferedOutputStream outStream = new BufferedOutputStream(new FileOutputStream(destinationFile));
		BufferedInputStream inStream = null; 
		try {
			inStream = new BufferedInputStream(new FileInputStream(sourceFile));
			decryptStream(inStream, outStream);
		} catch (IOException e) {

		} finally {
			outStream.close(); if (inStream != null) {inStream.close();}
		}
    }
	
	 /**
     * Decrypts the source file and return the result as a byte[]
     * @param sourceFile Encrypted file
     * @return byte[] decrypted bytes
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws IOException
     */
	public byte[] decryptToBytes(File sourceFile) throws InvalidKeyException, InvalidAlgorithmParameterException, IOException {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		BufferedInputStream inStream = null; 
		try {
			inStream = new BufferedInputStream(new FileInputStream(sourceFile));
			decryptStream(inStream, outStream);
		} catch (IOException e) {

		} finally {
			outStream.close(); if (inStream != null) {inStream.close();}
		}
		return outStream.toByteArray();
    }
	
	/**
	 * Decrypts the a byte[] and return the result as a byte[]
	 * @param bytes byte[] of encrypted bytes
	 * @return byte[] decrypted bytes
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 * @throws IOException
	 */
	public byte[] decryptBytesToBytes(byte[] bytes) throws InvalidKeyException, InvalidAlgorithmParameterException, IOException {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		decryptStream(new ByteArrayInputStream(bytes), outStream);
    	return outStream.toByteArray();
    }
}
