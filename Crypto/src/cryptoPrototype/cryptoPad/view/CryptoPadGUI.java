package cryptoPrototype.cryptoPad.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.BorderFactory;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;

import cryptoPrototype.cryptoPad.controller.ControllerPad;
/**
 * A NotePad designed to read and save encrypted files
 * Requires ControllerPad to handle its functionality
 * @author Martin Birch
 */
public class CryptoPadGUI {
	
	private PropertyChangeSupport pSupport;
	private ControllerPad controller;
	private JFrame window;
	private String docName;
	private JLabel docStatus;
	private JTextArea jtaContent;
	
	public static void main(String[] args) {
		CryptoPadGUI pad = new CryptoPadGUI();
		pad.setVisible(true);
		
	}
	/**
	 * Class Constructor, creates a visible notepad
	 */
	public CryptoPadGUI() {
		docName = "";
		window = new JFrame("CryptoPad: " + docName);
		window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		pSupport = new PropertyChangeSupport(this);
		createGUI();
	}
	
	/**
	 * Class Constructor, creates a visible notepad
	 * @param text sets the current text of the notepad
	 */
	public CryptoPadGUI(String text) {
		docName = "";
		window = new JFrame("CryptoPad: " + docName);
		window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		pSupport = new PropertyChangeSupport(this);
		createGUI();
		jtaContent.setText(text);
	}
	
	/**
	 * Creates the basic layout, also adds the controller if it is present
	 */
	private void createGUI() {
		window.getContentPane().removeAll();
		window.setJMenuBar(createMenuBar());
		JPanel jplRoot = new JPanel(new BorderLayout());
		JPanel jplOptions = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		jtaContent = new JTextArea();
		jtaContent.setLineWrap(true);
		jtaContent.setWrapStyleWord(true);
		
		jtaContent.getInputMap(JComponent.WHEN_FOCUSED)
			.put(KeyStroke.getKeyStroke(KeyEvent.VK_H, InputEvent.CTRL_DOWN_MASK), "none");

		JScrollPane scrContent = new JScrollPane(jtaContent);
		
		
		JPanel pnlDocStatus = new JPanel(new FlowLayout(FlowLayout.LEFT));
		pnlDocStatus.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
		docStatus = new JLabel(" ");
		Font current = docStatus.getFont();
		docStatus.setFont(new Font(current.getFontName(), Font.ITALIC, 10));
		docStatus.setBorder(null);
		pnlDocStatus.add(docStatus);
		
		jplRoot.add(jplOptions, BorderLayout.NORTH);
		jplRoot.add(scrContent, BorderLayout.CENTER);
		jplRoot.add(pnlDocStatus, BorderLayout.SOUTH);
		
		window.add(jplRoot);
		window.setSize(500, 500);
		window.setLocationRelativeTo(null);
		if (controller != null) {
			window.addWindowListener(controller);
		}
	}
	
	/**
	 * Creates a JMenuBar to be used, adds the controller if it exists
	 * @return JMenuBar
	 */
	private JMenuBar createMenuBar() {
		JMenuBar jmbMenuBar = new JMenuBar();
		
		JMenu jmuFile = new JMenu("File");
		JMenuItem jmiOpen = new JMenuItem("Open"); jmiOpen.addActionListener(controller);
		jmiOpen.setName("btnOpen");  jmuFile.add(jmiOpen); jmiOpen.setAccelerator(KeyStroke.getKeyStroke('O', Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		
		JMenuItem jmiNew = new JMenuItem("New"); jmiNew.addActionListener(controller);
		jmiNew.setName("btnNew");  jmuFile.add(jmiNew); jmiNew.setAccelerator(KeyStroke.getKeyStroke('N', Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		
		JMenuItem jmiSave = new JMenuItem("Save"); jmiSave.addActionListener(controller);
		jmiSave.setName("btnSave");  jmuFile.add(jmiSave); jmiSave.setAccelerator(KeyStroke.getKeyStroke('S', Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		
		JMenuItem jmiSaveAs = new JMenuItem("Save As"); jmiSaveAs.addActionListener(controller);
		jmiSaveAs.setName("btnSaveAs");  jmuFile.add(jmiSaveAs); 

		JMenuItem jmiExit = new JMenuItem("Exit"); jmiExit.addActionListener(controller);
		jmiExit.setName("btnExit");  jmuFile.add(jmiExit); jmiExit.setAccelerator(KeyStroke.getKeyStroke('Q', Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		
		JCheckBoxMenuItem jcmHide = new JCheckBoxMenuItem("Hide"); jcmHide.addActionListener(controller);
		jcmHide.setName("btnHide"); jcmHide.setAccelerator(KeyStroke.getKeyStroke('H', Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		
		jcmHide.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
			.put(  KeyStroke.getKeyStroke(KeyEvent.VK_H, InputEvent.CTRL_DOWN_MASK),
					  jcmHide.getAction()  	);
		
		jmbMenuBar.add(jmuFile);
		jmbMenuBar.add(jcmHide);
		return jmbMenuBar;
	}
	
	/**
	 * Adds a controller, and refreshes the layout to apply it to the widgets
	 * @param controller
	 */
	public void addController(ControllerPad controller) {
		this.controller = controller;
		createGUI();
	}
	
	/**
	 * Wraps the text
	 * @param wrap true or false if the text should be wrappred or not
	 */
	public void warpText(boolean wrap) {
		jtaContent.setLineWrap(wrap);
	}
	
	/**
	 * Sets the visibility of the notepad
	 * @param visible true or false depending on whether the notepad should be visible or not
	 */
	public void setVisible(boolean visible) {
		window.setVisible(visible);
	}
	
	/**
	 * Adds a PropertyChangeListener to this class
	 * @param listener PropertyChangeListener to add
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pSupport.addPropertyChangeListener(listener);
	}
	
	/**
	 * Sets the Name of the Document being used
	 * @param str String name of the document
	 */
	public void setDocumentName(String str) {
		docName = str;
		window.setTitle("CryptoPad: " + docName);
		refreshDisplay();
	}
	
	/**
	 * Sets the text to be displayed
	 * @param str Text to display
	 */
	public void setContent(String str) {
		jtaContent.setText(str);
		refreshDisplay();
	}
	
	/**
	 * Gets the text currently being displayed
	 * @return
	 */
	public String getContent() {
		return jtaContent.getText();
	}
	
	/**
	 * Sets the Font size
	 * @param size of the font
	 */
	public void setFontSize(int size) {
		Font current = jtaContent.getFont();
		jtaContent.setFont(new Font(current.getFontName(), current.getStyle(), size));
		refreshDisplay();
	}
	
	/**
	 * Sets the font the text should be displayed in
	 * @param f
	 */
	public void setFont(Font f) {
		jtaContent.setFont(f);
		refreshDisplay();
	}
	
	/**
	 * Refreshes the TextArea displaying the content of the document and the window
	 */
	private void refreshDisplay() {
		jtaContent.revalidate();
		jtaContent.repaint();
		window.revalidate();
		window.repaint();
	}

	/**
	 * Fires a Property Change Event
	 * @param propertyName String
	 * @param b1 boolean old value
	 * @param b2 boolean new value
	 */
	public void firePropertyChange(String propertyName, boolean b1, boolean b2) {
		pSupport.firePropertyChange(propertyName, b1, b2);
	}
	
	/**
	 * Gets the current document name
	 * @return String document name
	 */
	public String getDocumentName() {
		return docName;
	}

	/**
	 * Sets the status message to be displayed at the bottom of the screen
	 * @param status message String
	 */
	public void setStatus(String status) {
		docStatus.setText(status);
		docStatus.revalidate();
		docStatus.repaint();
		refreshDisplay();
	}

	/**
	 * Gets the JFrame being used to display the notepad
	 * @return JFrame of the notepad
	 */
	public JFrame getJFrame() {
		return window;
	}
}
