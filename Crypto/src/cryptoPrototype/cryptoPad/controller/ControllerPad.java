package cryptoPrototype.cryptoPad.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.time.LocalDateTime;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import cryptoPrototype.Encryption;
import cryptoPrototype.FileSystem;
import cryptoPrototype.cryptoPad.view.CryptoPadGUI;
/**
 * The Controller, that handles the methods needed to add the functionality to the CryptoPadGUI
 * @author Martin Birch
 */
public class ControllerPad implements ActionListener, WindowListener {

	private CryptoPadGUI padGUI;
	private Encryption encrypt;
	private byte[] data;
	private Path defaultDirectory;
	private File docCurrentLocation;
	private FileSystem fs;
	/**
	 * Class Constructor
	 * @param gui The CryptoPadGUI
	 * @param path The default directory for saving and reading files from
	 * @param encrypt The Encryption used on the files
	 */
	public ControllerPad(CryptoPadGUI gui, Path path, Encryption encrypt) {
		padGUI = gui;
		padGUI.addController(this);
		this.defaultDirectory = path;
		this.encrypt = encrypt;
		fs = new FileSystem(encrypt);
	}
	
	/**
	 * Converts a byte array to a String
	 * @param data byte[] to convert to String
	 * @return String representation of byte[]
	 */
	private String getString(byte[] data) {
		StringBuilder builder = new StringBuilder();
		for (byte b : data) {
			builder.append((char) b);
		}
		return builder.toString();
	}
	
	/**
	 * Converts a String to a byte[]
	 * @param str String to convert
	 * @return byte[] representation of the String provided
	 */
	private byte[] getBytes(String str) {
		byte[] bytes = new byte[str.length()];
		for (int i=0; i<str.length();++i) {
			bytes[i] = (byte) str.charAt(i);
		}
		return bytes;
	}
	
	/**
	 * Decrypts the file provided and sets the contents of it
	 * to the content of the view.
	 * @param f File to decrypt and load into the view
	 */
	public void openFile(File f) {
		try {
			byte[] data = encrypt.decryptToBytes(f);
			docCurrentLocation = f;
			String filename = fs.getFileName(f.toPath());
			System.out.println(filename);
			padGUI.setDocumentName(filename);
			padGUI.setContent(getString(data));
		} catch (InvalidKeyException | InvalidAlgorithmParameterException | IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() instanceof JMenuItem) {
			String name = ((JMenuItem) evt.getSource()).getName();
			if (name.equals("btnOpen")) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setCurrentDirectory(defaultDirectory.toFile());
				int value = fileChooser.showOpenDialog(null);
				if (value == JFileChooser.APPROVE_OPTION) {
					openFile(fileChooser.getSelectedFile());
				}
			} else if (name.equals("btnNew")) {
				docCurrentLocation = null;
				padGUI.setDocumentName("");
				padGUI.setContent("");
				padGUI.setStatus("");
			} else if (name.equals("btnSave")) {
				save();
			} else if (name.equals("btnSaveAs")) {
				saveAs();
			} else if (name.equals("btnExit")) {
				int promptExitting = JOptionPane.showConfirmDialog(null, "Save Before Closing", "Warning", JOptionPane.YES_NO_CANCEL_OPTION);
				if (promptExitting == JOptionPane.YES_OPTION) {
					save();
					padGUI.firePropertyChange("WindowClosed", false, true);
					padGUI.getJFrame().dispatchEvent(new WindowEvent(padGUI.getJFrame(), WindowEvent.WINDOW_CLOSING));
				} else if (promptExitting == JOptionPane.NO_OPTION) {
					
				}
			} else if (name.equals("btnHide")) {
				JCheckBoxMenuItem jcb = (JCheckBoxMenuItem) evt.getSource();
				if (jcb.isSelected()) {
					try {
						data = encrypt.encryptBytesToBytes(getBytes(padGUI.getContent()));
						padGUI.setContent(getString(data));
					} catch (InvalidKeyException | FileNotFoundException e) {
						e.printStackTrace();
					}
				} else {
					try {
						data = encrypt.decryptBytesToBytes(data);
						padGUI.setContent(getString(data));
					} catch (InvalidKeyException | InvalidAlgorithmParameterException | IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * Default method used to save a file, if no location is set for the 
	 * data to be saved at then the saveAs method is called.
	 */
	public void save() {
		if (docCurrentLocation == null) {
			saveAs();
		} else {
			try {
				encrypt.encryptBytes(getBytes(padGUI.getContent()), docCurrentLocation);
				padGUI.setStatus("Saved At: " + LocalDateTime.now().toString().substring(11, 19) + "  File Location: " + docCurrentLocation.getAbsolutePath());
			} catch (InvalidKeyException | FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Prompts the user to provide a location to save the file at and then calls the save method
	 */
	public void saveAs() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(defaultDirectory.toFile());
		int value = fileChooser.showSaveDialog(null);
		if (value == JFileChooser.APPROVE_OPTION) {
			File temp = fileChooser.getSelectedFile();
			byte[] empty = new byte[] {0};
			int fileName = fs.createFile(Paths.get(temp.getParent()), temp.getName(), empty);
			docCurrentLocation = new File(temp.getParent() + File.separator + String.valueOf(fileName)) ;
			save();
		}
	}

	@Override
	public void windowActivated(WindowEvent arg0) {}

	@Override
	public void windowClosed(WindowEvent arg0) {}

	@Override
	public void windowClosing(WindowEvent arg0) {
		fs.close();
		padGUI.firePropertyChange("WindowClosed", false, true);
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {}

	@Override
	public void windowDeiconified(WindowEvent arg0) {}

	@Override
	public void windowIconified(WindowEvent arg0) {}

	@Override
	public void windowOpened(WindowEvent arg0) {}

}
