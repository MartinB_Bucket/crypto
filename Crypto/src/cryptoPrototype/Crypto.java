package cryptoPrototype;

import java.awt.CardLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;

import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import cryptoPrototype.fileExplorer.controller.ControllerExplorer;
import cryptoPrototype.keyVault.controller.ControllerKeyVault;
import cryptoPrototype.keyVault.model.AccountDetails;
import cryptoPrototype.keyVault.model.IntegrityException;
import cryptoPrototype.keyVault.model.KeyVault;
import cryptoPrototype.keyVault.view.VaultGUI;
import cryptoPrototype.login.controller.CPWController;
import cryptoPrototype.login.controller.ControllerLogin;
import cryptoPrototype.login.model.LoginSystem;
import cryptoPrototype.login.view.LoginGUI;
import cryptoPrototype.startmenu.StartMenuPanel;
import cryptoPrototype.syncManager.SyncManager;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.util.Pair;
/**
 * This is themain class of the program through which all other classes are accessed.
 * To start the program call the method startProgram.
 * 
 * @author Martin Birch
 */
public class Crypto implements ActionListener, Password, PropertyChangeListener, FilePaths {
    
    private AccountDetails currentUser;        //stores username and password
    private JFrame program;	// The main UI of the program
    private Encryption encrypt; // Encryption object being used
    
    private JPanel pnlRoot;
    private StartMenuPanel taskBar;
    
    public static void main(String[] args) {
    	SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	Crypto c = new Crypto();
                c.startProgram();
            }
        });
    }
    
    /**
     * Class Constructor, sets the Look and Feel to the same as the systems
     */
    public Crypto() {
    	try {
    		if (!Files.exists(ROOT_DIRECTORY)) {
    			Files.createDirectories(ROOT_DIRECTORY);
    		}
    		//System.out.println(System.getProperty("os.name"));
    		//if (System.getProperty("os.name").contains("Linux")) {
    			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());    			
    		//}
        } catch (Exception e) {
        }
    }
    
    /**
     * Used to prompt the user with a login screen
     */
    private void promptLogin() {
    	LoginGUI loginGUI =  new LoginGUI();
    	ControllerLogin loginController = new ControllerLogin(loginGUI, DATABASE_LOGIN);
    	loginController.addPropertyChangeListener(this);
    	loginGUI.addController(loginController);
    	loginGUI.setVisible(true);
    }
    
    /**
     * Starts the program prompting the login screen
     */
    public void startProgram() {
		Platform.setImplicitExit(false); // Allows JavaFX GUI's to be repeatedly opened and closed
        program = new JFrame();
        //program.setResizable(false);
        program.addWindowListener(new WindowListener() {
			@Override
			public void windowOpened(WindowEvent e) { }
			@Override
			public void windowClosing(WindowEvent e) {
				Platform.setImplicitExit(true);
			}
			@Override
			public void windowClosed(WindowEvent e) { }
			@Override
			public void windowIconified(WindowEvent e) { }
			@Override
			public void windowDeiconified(WindowEvent e) { }
			@Override
			public void windowActivated(WindowEvent e) { }
			@Override
			public void windowDeactivated(WindowEvent e) { }
        });
        program.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        promptLogin();
        program.setVisible(false);
    }
    
    /**
     * Create a loginGUI that can be used to change the password
     * @param listener PropertyChangeListener to add to the GUI to listen for
     * 								when the GUI closes
     */
    private void startPasswordChange(PropertyChangeListener listener) {
		LoginGUI loginGUI = new LoginGUI(false, "Change");
		loginGUI.setExitOnClose(false);
		if (listener != null) {
			loginGUI.addPropertyChangeListener(listener);
		}
		CPWController cpwController = new CPWController(this, loginGUI);
		loginGUI.addController(cpwController);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JMenuItem) {
            JMenuItem b = (JMenuItem) e.getSource();
            switch (b.getName()) {
                case "btnChangePassword":
                	startPasswordChange(null);
                    break;
                case "btnExit":
                    program.dispatchEvent(new WindowEvent(program, WindowEvent.WINDOW_CLOSING));
                    break;
                case "btnKeyVault":
				try {
					VaultGUI vaultGUI = new VaultGUI();
					vaultGUI.addPropertyListener(this);
					ControllerKeyVault vaultController = new ControllerKeyVault(vaultGUI, DATABASE_VAULT, encrypt);
					vaultGUI.addController(vaultController);
					vaultGUI.setVisible(true);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
                    break;
            }
        }
    }
    
    @Override
    public char[] getUser() {
        return currentUser.getUsername();
    }

    @Override
    public char[] getPass() {
        return currentUser.getPassword();
    }

    @Override
    public void setUser(char[] user) {
        currentUser.addUsername(user);
    }

    @Override
    public void setPass(char[] pass) {
        currentUser.setPassword(pass);
    }

    /**
     * Creates a JFrame containing the JProgressBar supplied returns the JFrame created
     * @param bar JProgressBar to use in the JFrame
     * @return JFrame that has been created
     */
    private JFrame createProgressionWindow(JProgressBar bar) {
    	JFrame progressWindow = new JFrame("Changing Password");
    	progressWindow.setLayout(new FlowLayout(FlowLayout.CENTER));;
    	progressWindow.add(bar);
    	progressWindow.setSize(250, 53);
    	bar.setPreferredSize(new Dimension(240, 20));
    	progressWindow.setLocationRelativeTo(null);
    	progressWindow.setResizable(false);
    	progressWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    	progressWindow.setVisible(true);
    	return progressWindow;
    }
    
    @Override
    public void changePassword(char[] newUser, char[] newPass) {
    	JProgressBar bar = new JProgressBar();
    	JFrame window = createProgressionWindow(bar);
    	program.setVisible(false);
    	int amount = ControllerExplorer.countFiles(encrypt);
    	amount *= 10; // Each file worth 10
    	amount += 16;
    	int value = 6;
		bar.setMaximum(amount);
		bar.setValue(value);
		
    	Encryption encryptNew = new Encryption(newPass);
    	
    	LoginSystem ls = new LoginSystem(DATABASE_LOGIN);
        ls.changeAuthenticationDetails(newUser, newPass, getUser(), getPass());
        ls.destroy();
        value += 5;
        bar.setValue(value);
        KeyVault kv = new KeyVault(DATABASE_VAULT, RECORD_LOG, encrypt);
        try {
			kv.changeVaultKey(encryptNew);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IntegrityException e) {
			 JOptionPane.showConfirmDialog(window, "Integrity Error Whilst Reading Keys! Aborting!", 
						"Integrity Error", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// Wrong Password Can't Decrypt data
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// Invalid File type
			e.printStackTrace();
		}
        value += 5;
        bar.setValue(value);
        
        ControllerExplorer.changePassword(encrypt, encryptNew, ROOT_DIRECTORY, bar);
        
        Path p = RECORD_LOG.toPath();
        
        if (Files.exists(p)) {
			try {
				Path newPath = Paths.get(p.toAbsolutePath().toString() + ".temp");
				encrypt.changeFilePassword(p.toFile(), newPath.toFile(), encryptNew);
				Files.delete(p);
				Files.move(newPath, p);
			} catch (InvalidKeyException | FileNotFoundException | InvalidAlgorithmParameterException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
        
        encrypt = encryptNew;
        currentUser = new AccountDetails(encrypt);
        currentUser.setLogChanges(false);
        setUser(newUser);
        setPass(newPass);
        
		createGUI();
		window.setVisible(false);
		//window.dispose();
		program.setVisible(true);
    }
    
    /**
     * Creates and returns a StartMenu, containing its items
     * @return JComponent startmenu
     */
    private void createTaskBar() {
		taskBar = new StartMenuPanel(this, encrypt);
    }
    
    /**
     * Constructs the main GUI of the program
     */
    private void createGUI() {
    	program.setTitle("Crypto");
    	if (pnlRoot != null) {
    		program.remove(pnlRoot);
    		pnlRoot.removeAll();    		
    	}
    	CardLayout cLayout =  new CardLayout();
    	pnlRoot = new JPanel(cLayout);
		createTaskBar();
    	pnlRoot.add(taskBar);
    	pnlRoot.setPreferredSize(new Dimension(460, 500));
    	program.add(pnlRoot);
    	pnlRoot.revalidate();
    	pnlRoot.repaint();
    	program.revalidate();
    	program.repaint();
    }
    
    /**
     * Used to handle a successful user login event
     * @param evt PropertyChangeEvent
     */
    private void userLogin(PropertyChangeEvent evt) {
    	@SuppressWarnings("unchecked")
		Pair<Encryption, AccountDetails> loginDetails = (Pair<Encryption, AccountDetails>) evt.getNewValue();
    	
    	encrypt = loginDetails.getKey();
    	currentUser = loginDetails.getValue();
    	
    	currentUser.setLogChanges(false);
		createGUI();
		//program.add(pnlRoot);
		program.setSize(460, 500);
		program.setLocationRelativeTo(null);
		program.setVisible(true);
		
    }
    
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals("authorisedUser")) {
			if (evt.getSource() instanceof ControllerLogin) {
				userLogin(evt);
			}
		} else if (evt.getPropertyName().equals("WindowClosed")) {
			program.setVisible(true);
		} else if (evt.getPropertyName().equals("ApplicationOpened")) {
			program.setVisible(false);
			taskBar.setEnableTaskBarItems(true);
		} else if (evt.getPropertyName().equals("ProgramLoading")) {
			if ((boolean) evt.getNewValue()) {
				taskBar.setEnableTaskBarItems(false);
				program.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			} else {
				program.setCursor(Cursor.getDefaultCursor());
			}
		} else if (evt.getPropertyName().equals("InvokeChangePassword")) {
			startPasswordChange((PropertyChangeListener) evt.getNewValue());
		} else if (evt.getPropertyName().equals("InvokeChangeBackground")) {
			StartMenuPanel.setDefaultImage((Path) evt.getNewValue(), encrypt);
			taskBar.refreshDisplay();
		} else if (evt.getPropertyName().equals("InvokeChangeSyncAccounts")) {
			new JFXPanel(); //Start JavaFX
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					SyncManager sManager = new SyncManager(encrypt);
					sManager.addPropertyChangeListener( (PropertyChangeListener) evt.getNewValue());					
				}
			});
		}
	}

}
