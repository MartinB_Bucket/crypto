package cryptoPrototype;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Handles hashing of string and bytes arrays
 * @author Martin Birch
 */
public class HashingSystem implements Salt {
    /**
     * Amount of times to hash the input
     */
    //private static final int HASH_ITERATIONS = 80000;
    public static final int MODE_SECURE = 80000;
    public static final int MODE_FAST = 1;
    
    private static final String SHA = "SHA-256";
    private static final String MD5 = "MD5";
    
    private int currentMode;
    private String hashingAlgorithm;
    
    public HashingSystem() {
    	setHashMode(MODE_SECURE);
    }
    
    public HashingSystem(int mode) {
    	setHashMode(mode);
    }
    
    /**
     * Sets the Hashing Mode of the object to either produce a fast hash or a secure hash
     * @param mode MODE_SECURE or MODE_FAST
     */
    public void setHashMode(int mode) {
    	switch (mode) {
    	case MODE_SECURE:
    		currentMode = MODE_SECURE;
    		hashingAlgorithm = SHA;
    		break;
    	case MODE_FAST:
    		currentMode = MODE_FAST;
    		hashingAlgorithm = MD5;
    		break;
    	default:
    		currentMode = MODE_SECURE;
    		hashingAlgorithm = SHA;
    		
    	}
    }
    
    /**
     * Hashes a series of input bytes, and outputs the result of the array starting at offset and ending at the endPostion
     * @param inputBytes byte[] to hash
     * @param offset of the hash array max 31
     * @param length
     * @return byte[] hashed output
     */
    private byte[] hash(byte[] inputBytes, int offset, int length) {
        byte[] hash = null;
        try {
            MessageDigest mg = MessageDigest.getInstance(hashingAlgorithm);
            mg.update(inputBytes);
            
            byte[] dump =null;
            
            for (int i = 0; i < currentMode; ++i) {  
            	dump = mg.digest();
                hash = new byte[dump.length+inputBytes.length];
                System.arraycopy(dump, 0, hash, 0, dump.length);
                System.arraycopy(inputBytes, 0, hash, dump.length, inputBytes.length);
                mg.update(hash);
            }
            
            hash = new byte[length];
            if (length > dump.length) {
            	System.arraycopy(mg.digest(), offset, hash, 0, dump.length);
            } else {
            	System.arraycopy(mg.digest(), offset, hash, 0, length);
            }
            
            return hash;
        } catch (NoSuchAlgorithmException ex) {
            
        }
        return hash;
    }

    /**
     * Hashes a series of input characters
     * @param letters char[] to hash
     * @return byte[] of hash output
     */
    public byte[] hash(char[] letters) {
        byte[] bytes = new byte[letters.length];
        for (int i = 0; i<letters.length; ++i) {
            bytes[i] = (byte) letters[i];
        }
        
        return hash(bytes);
    }

    /**
     * Hashes a series of input bytes and returns the first 16 bytes of the hash
     * @param letters byte[] to hash
     * @return byte[] first 16 bytes of hash output will be returned
     */
    public byte[] hash(byte[] s) {
        return hash(s,0,16);
    }

    /**
     * Hashes a series of input characters, and returns a byte[] as a result of the hash
     * @param inputBytes byte[] to hash
     * @param offset of the hash array max 31
     * @param length max 32
     * @return byte[] hashed output
     */
    public byte[] hash(char[] s, int offset, int length) {
        byte[] bytes = new byte[s.length];
        for (int i = 0; i<s.length; ++i) {
            bytes[i] = (byte) s[i];
            s[i] = 'a';
        }
        return hash(bytes, offset, length);
    }

    /**
     * Creates a random salt, with the specified length
     * @param length of the salt
     * @return byte[]
     */
    public byte[] createSalt(int length) {
        SecureRandom secure = new SecureRandom();
        byte bytes[] = new byte[length];
        secure.nextBytes(bytes);
        return bytes;
    }
    
    /**
     * Creates a Salt, with the a standard length
     * @return byte[]
     */
    public byte[] createSalt() {
        SecureRandom secure = new SecureRandom();
        byte bytes[] = new byte[SALT_LENGTH];
        secure.nextBytes(bytes);
        return bytes;
    }
    
    /**
     * Adds series of bytes (saltBytes) onto the end of an byte[] (normalBytes)
     * @param normalBytes
     * @param saltBytes
     * @return byte[]
     */
    private byte[] addSalt(byte[] normalBytes, byte[] saltBytes) {
        byte[] totalBytes = new byte[normalBytes.length + SALT_LENGTH];
        
        for (int i = 0; i < normalBytes.length; ++i) {
            totalBytes[i] = (byte) normalBytes[i];
        }
        
        for (int i = normalBytes.length; i < (totalBytes.length); ++i) {
            totalBytes[i] = (byte) saltBytes[i-normalBytes.length];
        }
        
        return totalBytes;
    }
    
    /**
     * Adds a salt on to the end of a byte[] (normalBytes) and returns the result
     * @param normalBytes
     * @return byte[]
     */
    public byte[] addSalt(byte[] normalBytes) {
        return addSalt(normalBytes, createSalt());
    }
    
    /**
     * Converts a char[] to a byte[]
     * @param letters char[]
     * @return byte[]
     */
    public static byte[] convertCharToByte(char[] letters) {
        byte[] bytes = new byte[letters.length];
        for (int i = 0; i < bytes.length; ++i) {
            bytes[i] = (byte) letters[i];
        }
        return bytes;
    }
    
    /**
     * Converts a byte[] to a char[]
     * @param letters byte[]
     * @return char[]
     */
    public static char[] convertByteToChar(byte[] letters) {
        char[] bytes = new char[letters.length];
        for (int i = 0; i < bytes.length; ++i) {
            bytes[i] = (char) letters[i];
        }
        return bytes;
    }
    
    /**
     * Adds a random salt to the bytes, then hashes the bytes and adds the salt to
     * the end of the hashed bytes
     * @param bytes Bytes to Hash 
     * @return byte[] in the form Hash+Salt
     */
    public byte[] saltedHash(byte[] bytes) {
    	byte[] userSalt = createSalt();
        byte[] userBytes = addSalt(bytes, userSalt);
        byte[] userBytesHashed = new byte[32+SALT_LENGTH];
        System.arraycopy(hash(userBytes,0,32), 0, userBytesHashed, 0, 32);
        
        for (int i = 32; i < (32+SALT_LENGTH); ++i) {
            userBytesHashed[i] = userSalt[i-32];
        }
        return userBytesHashed;
    }
    
    /**
     * Adds a random salt to the bytes, then hashes the bytes and adds the salt to
     * the end of the hashed bytes
     * @param bytes[] Bytes to Hash 
     * @param bytes[] of the salt
     * @return byte[] in the form Hash+Salt
     */
    public byte[] saltedHash(byte[] bytes, byte[] salt) {
        byte[] userBytes = addSalt(bytes, salt);
        byte[] userBytesHashed = new byte[32+SALT_LENGTH];
        System.arraycopy(hash(userBytes,0,32), 0, userBytesHashed, 0, 32);
        
        for (int i = 32; i < (32+SALT_LENGTH); ++i) {
            userBytesHashed[i] = salt[i-32];
        }
        return userBytesHashed;
    }
    

	/**
	 * Gets the Hexadecimal equivalent of a b returned in a char[] in the form of: </b>
	 * byte = 24 </br>
	 * char[0] = 1 </br>
	 * char[1] = 8 </br>
	 * @param b byte to retrieve hexadecimal value of
	 * @return char[]
	 */
	public char[] convertToHex(byte b) {
		int amount = 0;
		int no = b;
		
		while (no > 16) {
			no -= 16;
			amount++;
		}
		
		char[] two = new char[2];
		two[0] = getHexLetter(amount);
		two[1] = getHexLetter(no);
		
		return two;
	}
	
	/**
	 * When give an integer less than 16 it's hexadecimal equivalent letter is returned
	 * @param i Integer between 0 (inclusive) and 16 (exclusive)
	 * @return char representing a hexadecimal letter
	 */
	private char getHexLetter(int i) {
		char c = 0;
		if (i < 9) {
			c = (char) i;
		} else {
			switch (i) {
			case 10:
				c = 'A';
				break;
			case 11:
				c = 'B';
				break;
			case 12:
				c = 'C';
				break;
			case 13:
				c = 'D';
				break;
			case 14:
				c = 'E';
				break;
			case 15:
				c = 'F';
				break;
			}			
		}
		return c;
	}
	
}
