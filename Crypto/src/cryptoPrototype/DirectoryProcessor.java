package cryptoPrototype;

import java.nio.file.Path;
/**
 * Used to define default methods to be used when either processing a directory or file
 * @author Martin Birch
 */
public interface DirectoryProcessor {
	/**
	 * Handles any processing the is required upon a directory and determines whether the next directory should be read
	 * @param p Path of the directory, the directory name may appear as a number in this path if it was encrypted
	 * @param dirName is the real directory name of the file
	 * @return true or false depending on whether the directory should be read
	 */
	public boolean processDirectory(Path p, String dirName);
	
	/**
	 * Handles any processing the is required upon a File
	 * @param p Path of the File, the filename may appear as a number in this path if it was encrypted
	 * @param fileName is the decrypted filename of the file (or just the standard file name if it is not encrypted)
	 */
	public void processFile(Path p, String fileName);
}
