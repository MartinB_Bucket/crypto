package cryptoPrototype;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

/**
 * A JImageButton is a button that consists of two main looks on hover and off hover. This class can be used to create a 
 * simple button which you would like to specify the images for.
 * 
 * @author Martin Birch
 */
public class JImageButton extends JButton implements MouseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4711788645916863116L;
	private Image onHover;
	private Image standard;
	private boolean hasFocus;
	private boolean changeOnHover;
	private boolean changeWhenClicked;
	private boolean isClicked;
	
	/**
	 * Class Constructor
	 * @param path1 Resource Path of the background image
	 * @param path2 Resource Path of the background image when the mouse is hovering over it
	 */
	public JImageButton(Image path1, Image path2) {
		super();
		standard = path1;
		onHover = path2;
		changeOnHover = true;
		addMouseListener(this);
	}
	
	public void paintComponent(Graphics g) {
		if (hasFocus || isClicked) {
			g.drawImage(onHover, 0, 0, getWidth(), getHeight(), null);
		} else {
			g.drawImage(standard, 0, 0, getWidth(), getHeight(), null);
		}
	}
	
	public void setChangeImageOnHover(boolean flag) {
		changeOnHover = flag;
	}
	
	public void setChangeOnClick(boolean flag) {
		changeWhenClicked = flag;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		if (changeWhenClicked) {
			isClicked = !isClicked;
			repaint();
		}
		firePropertyChange("ButtonClicked", false, true);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if (changeOnHover) {
			hasFocus = true;
			repaint();
		}
		firePropertyChange("MouseEnteredButton", false, true);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		hasFocus = false;
		repaint();
		firePropertyChange("MouseExitedButton", false, true);
	}
	
}
