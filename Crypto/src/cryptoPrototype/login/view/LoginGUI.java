package cryptoPrototype.login.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import cryptoPrototype.JImagePanel;
import cryptoPrototype.login.controller.LoginController;

/**
 * A simple login dialog
 * @author Martin Birch
 */
public class LoginGUI implements WindowListener {
    
	private PropertyChangeSupport pSupport;
	private JFrame loginWindow;
	private JFrame createAccount;
    private JTextField txtUsername;
    private JTextField txtConfirmUsername;
    private JPasswordField txtPassword;
    private JPasswordField txtConfirmPassword;
    private LoginController controller;
    private BufferedImage imgError;
    
    private boolean isLoginDisplayed;
    private boolean exitOnClose;
    
    private String actionButtonText;
    private JPanel rootLogin;
    private JPanel rootCreate;
    
    /**
     * Class Constructor
     */
    public LoginGUI() {
        pSupport = new PropertyChangeSupport(this);
        loginWindow = new JFrame();
        createAccount = new JFrame();
        actionButtonText = "Create";
        isLoginDisplayed = true;
        exitOnClose = true;
        try {
			imgError = ImageIO.read(getClass().getResourceAsStream("/Images/error.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
        createGUI();
    }
    
    /**
     * Class Constructor
     * @param displayLogin, true to display login view, or false to display create an account
     */
    public LoginGUI(boolean displayLogin) {
        pSupport = new PropertyChangeSupport(this);
        loginWindow = new JFrame();
        createAccount = new JFrame();
        actionButtonText = "Create";
        isLoginDisplayed = displayLogin;
        exitOnClose = true;
        try {
			imgError = ImageIO.read(getClass().getResourceAsStream("/Images/error.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
        createGUI();
    }
    
    /**
     * Class Constructor
     * @param displayLogin, true to display login view, or false to display create an account
     * @param str String text to appear on the main action Button
     */
    public LoginGUI(boolean displayLogin, String str) {
        pSupport = new PropertyChangeSupport(this);
        loginWindow = new JFrame();
        createAccount = new JFrame();
        actionButtonText = str;
        isLoginDisplayed = displayLogin;
        exitOnClose = true;
        try {
			imgError = ImageIO.read(getClass().getResourceAsStream("/Images/error.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
        createGUI();
    }
    
    /**
     * Sets the text that is show on the main action button
     * @param str String text to appear on the main action Button
     */
    public void setActionButtonText(String str) {
    	actionButtonText = str;
    }
    
    /**
     * Adds the controller to this class
     * @param con ControllerLogin
     */
    public void addController(LoginController con) {
    	controller = con;
        createGUI();
    }
    
    /**
     * Adds a Property Change Listener to this class
     * @param p PropertyChangeListener
     */
    public void addPropertyChangeListener(PropertyChangeListener p) {
    	if (pSupport != null) {
    		pSupport.addPropertyChangeListener(p);
    	}
    }
    
    /**
     * Sets whether the JFrame used should close the application or dispose
     * @param exitOnClose boolean true to cause the program to exit, or false
     * 										to make the class just dispose.
     */
    public void setExitOnClose(boolean exitOnClose) {
    	this.exitOnClose = exitOnClose;
    }
    
    /**
     * Creates Basic screen widgets
     */
    private void createGUI() {
    	if (isLoginDisplayed) {
    		displayLogin(false);
    	} else {
    		displayCreateAccount(false, false, false, false, null);
    	}
    }
    
    private JPanel createUsernamePanel(boolean flag) {
    	JPanel pnlUsername = new JPanel(new GridBagLayout());
    	pnlUsername.setPreferredSize(new Dimension(140, 40));
    	pnlUsername.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
        GridBagConstraints cUsername = new GridBagConstraints();

        int cellWidth = 6;
        
        if (flag) {
        	cellWidth = 7;
        }
        
        JLabel lblUsername = new JLabel("Username: ");
        cUsername.gridx = 0;
        cUsername.gridy = 0;
        cUsername.gridheight = 1;
        cUsername.gridwidth = cellWidth;
        cUsername.anchor = GridBagConstraints.WEST;
        pnlUsername.add(lblUsername, cUsername);
        
        txtUsername = new JTextField();
        cUsername.fill = GridBagConstraints.HORIZONTAL;
        cUsername.gridx = 0;
        cUsername.gridy = 1;
        cUsername.gridheight = 1;
        cUsername.gridwidth = 6;
        cUsername.weightx = 1.0;
        cUsername.ipady = 0;
        cUsername.anchor = GridBagConstraints.WEST;
        pnlUsername.add(txtUsername, cUsername);
        txtUsername.addKeyListener(controller);
        
        if (flag) {
            JImagePanel pnlError = new JImagePanel(imgError);
            cUsername.fill = GridBagConstraints.HORIZONTAL;
            cUsername.gridx = 6;
            cUsername.gridy = 1;
            cUsername.gridheight = 1;
            cUsername.gridwidth = 1;
            cUsername.weightx = 0;
            cUsername.ipady = 0;
            cUsername.anchor = GridBagConstraints.WEST;
            pnlUsername.add(pnlError, cUsername);
        }
        
        return pnlUsername;
    }
    
    private JPanel createUsernameConfirmPanel(boolean flag) {
    	JPanel pnlConfirmUsername = new JPanel(new GridBagLayout());
    	pnlConfirmUsername.setPreferredSize(new Dimension(140, 40));
    	pnlConfirmUsername.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
        GridBagConstraints cConfirmUsername = new GridBagConstraints();

        int cellWidth = 6;
        
        if (flag) {
        	cellWidth = 7;
        }
        
        JLabel lblConfirmUsername = new JLabel("Confirm Username: ");
        cConfirmUsername.gridx = 0;
        cConfirmUsername.gridy = 0;
        cConfirmUsername.gridheight = 1;
        cConfirmUsername.gridwidth = cellWidth;
        cConfirmUsername.anchor = GridBagConstraints.WEST;
        pnlConfirmUsername.add(lblConfirmUsername, cConfirmUsername);
        
        txtConfirmUsername = new JTextField();
        cConfirmUsername.fill = GridBagConstraints.HORIZONTAL;
        cConfirmUsername.gridx = 0;
        cConfirmUsername.gridy = 1;
        cConfirmUsername.gridheight = 1;
        cConfirmUsername.gridwidth = 6;
        cConfirmUsername.weightx = 1.0;
        cConfirmUsername.ipady = 0;
        cConfirmUsername.anchor = GridBagConstraints.WEST;
        pnlConfirmUsername.add(txtConfirmUsername, cConfirmUsername);
        txtConfirmUsername.addKeyListener(controller);
        
        if (flag) {
            JImagePanel pnlError = new JImagePanel(imgError);
            cConfirmUsername.fill = GridBagConstraints.HORIZONTAL;
            cConfirmUsername.gridx = 6;
            cConfirmUsername.gridy = 1;
            cConfirmUsername.gridheight = 1;
            cConfirmUsername.gridwidth = 1;
            cConfirmUsername.weightx = 0;
            cConfirmUsername.ipady = 0;
            cConfirmUsername.anchor = GridBagConstraints.WEST;
            pnlConfirmUsername.add(pnlError, cConfirmUsername);
        }
        
        return pnlConfirmUsername;
    }
    
    private JPanel createPasswordPanel(boolean flag) {
    	JPanel pnlPassword = new JPanel(new GridBagLayout());
    	pnlPassword.setPreferredSize(new Dimension(140, 40));
    	pnlPassword.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
        GridBagConstraints cPassword = new GridBagConstraints();

        int cellWidth = 6;
        
        if (flag) {
        	cellWidth = 7;
        }
        
        JLabel lblPassword = new JLabel("Password: ");
        cPassword.gridx = 0;
        cPassword.gridy = 0;
        cPassword.gridheight = 1;
        cPassword.gridwidth = cellWidth;
        cPassword.anchor = GridBagConstraints.WEST;
        pnlPassword.add(lblPassword, cPassword);
        
        txtPassword = new JPasswordField();
        cPassword.fill = GridBagConstraints.HORIZONTAL;
        cPassword.gridx = 0;
        cPassword.gridy = 1;
        cPassword.gridheight = 1;
        cPassword.gridwidth = 6;
        cPassword.weightx = 1.0;
        cPassword.ipady = 0;
        cPassword.anchor = GridBagConstraints.WEST;
        pnlPassword.add(txtPassword, cPassword);
        txtPassword.addKeyListener(controller);

        if (flag) {
            JImagePanel pnlError = new JImagePanel(imgError);
            cPassword.fill = GridBagConstraints.HORIZONTAL;
            cPassword.gridx = 6;
            cPassword.gridy = 1;
            cPassword.gridheight = 1;
            cPassword.gridwidth = 1;
            cPassword.weightx = 0;
            cPassword.ipady = 0;
            cPassword.anchor = GridBagConstraints.WEST;
            pnlPassword.add(pnlError, cPassword);
        }
        
        return pnlPassword;
    }
    
    private JPanel createConfirmPasswordPanel(boolean flag) {
    	JPanel pnlConfirmPassword = new JPanel(new GridBagLayout());
    	pnlConfirmPassword.setPreferredSize(new Dimension(140, 50));
    	pnlConfirmPassword.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
        GridBagConstraints cConfirmPassword = new GridBagConstraints();
        
        int cellWidth = 6;
        
        if (flag) {
        	cellWidth = 7;
        }
        
        JLabel lblConfirmPassword = new JLabel("Confirm Password: ");
        cConfirmPassword.fill = GridBagConstraints.BOTH;
        cConfirmPassword.gridx = 0;
        cConfirmPassword.gridy = 0;
        cConfirmPassword.gridheight = 1;
        cConfirmPassword.gridwidth = cellWidth;
        cConfirmPassword.anchor = GridBagConstraints.WEST;
        pnlConfirmPassword.add(lblConfirmPassword, cConfirmPassword);
        
        txtConfirmPassword = new JPasswordField();
        cConfirmPassword.fill = GridBagConstraints.HORIZONTAL;
        cConfirmPassword.gridx = 0;
        cConfirmPassword.gridy = 1;
        cConfirmPassword.gridheight = 1;
        cConfirmPassword.gridwidth = 6;
        cConfirmPassword.weightx = 1.0;
        cConfirmPassword.ipady = 0;
        cConfirmPassword.anchor = GridBagConstraints.WEST;
        pnlConfirmPassword.add(txtConfirmPassword, cConfirmPassword);
        txtConfirmPassword.addKeyListener(controller);

        if (flag) {
            JImagePanel pnlError = new JImagePanel(imgError);
            cConfirmPassword.fill = GridBagConstraints.HORIZONTAL;
            cConfirmPassword.gridx = 6;
            cConfirmPassword.gridy = 1;
            cConfirmPassword.gridheight = 1;
            cConfirmPassword.gridwidth = 1;
            cConfirmPassword.weightx = 0;
            cConfirmPassword.ipady = 0;
            cConfirmPassword.anchor = GridBagConstraints.WEST;
            pnlConfirmPassword.add(pnlError, cConfirmPassword);
        }
        
        return pnlConfirmPassword;
    }
    
    /**
     * Displays a create Account view.
     * The Integer passed causes different error messages to be displayed,
     * however, use -1 to display no error message
     * @param errorUsername show Username error image
     * @param errorConfirmUsername  show Confirm Username error image
     * @param errorPassword  show Password error image
     * @param errorConfirmPassword  show Confirm Password error image
     * @param str String the error message to be displayed, use null to display no message
     */
    public void displayCreateAccount(boolean errorUsername, boolean errorConfirmUsername, 
    																			boolean errorPassword, boolean errorConfirmPassword, String str) {

    	createAccount.setResizable(true);
    	if (rootCreate != null) {
    		createAccount.remove(rootCreate);
    	}
    	createAccount.setTitle("Create Account");
    	createAccount.setResizable(false);
    	createAccount.addWindowListener(this);
        
        rootCreate = new JPanel(new BorderLayout()); 
        
        JPanel pnlGrouper  = new JPanel(); 
        pnlGrouper.setLayout(new BoxLayout(pnlGrouper, BoxLayout.Y_AXIS));
        
        JPanel pnlInput = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        JPanel pnlUsername = createUsernamePanel(errorUsername);
        c.gridx = 0;
        c.gridy = 0;
        c.gridheight = 1;
        c.gridwidth = 3;
        c.anchor = GridBagConstraints.WEST;
        pnlInput.add(pnlUsername, c);
        
        JPanel pnlConfirmUsername = createUsernameConfirmPanel(errorConfirmUsername);
        c.gridx = 0;
        c.gridy = 1;
        c.gridheight = 1;
        c.gridwidth = 3;
        c.anchor = GridBagConstraints.WEST;
        pnlInput.add(pnlConfirmUsername, c);

        JPanel pnlPassword = createPasswordPanel(errorPassword);
        c.gridx = 0;
        c.gridy = 2;
        c.gridheight = 1;
        c.gridwidth = 3;
        c.anchor = GridBagConstraints.WEST;
        pnlInput.add(pnlPassword, c);

        JPanel pnlConfirmPassword = createConfirmPasswordPanel(errorConfirmPassword);
        c.gridx = 0;
        c.gridy = 3;
        c.gridheight = 1;
        c.gridwidth = 3;
        c.anchor = GridBagConstraints.WEST;
        pnlInput.add(pnlConfirmPassword, c);
        
        
        JPanel pnlOptions = new JPanel(new GridBagLayout());
        GridBagConstraints cOptions = new GridBagConstraints();
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        JButton btnCreateAccount = new JButton(actionButtonText);
        buttonPanel.add(btnCreateAccount);
        btnCreateAccount.setName("btnCreate");
        btnCreateAccount.addActionListener(controller);
        
        buttonPanel.add(Box.createHorizontalStrut(10));
        JButton btnCancel = new JButton("Cancel");
        buttonPanel.add(btnCancel);
        btnCancel.setName("btnCancel");
        btnCancel.addActionListener(controller);
        cOptions.gridx = 0;
        cOptions.gridy = 4;
        cOptions.gridheight = 1;
        cOptions.gridwidth = 3;
        cOptions.ipady = 10;
        cOptions.anchor = GridBagConstraints.CENTER;
        pnlOptions.add(buttonPanel, cOptions);
              
        pnlGrouper.add(pnlInput);
        pnlGrouper.add(pnlOptions);

        rootCreate.add(pnlGrouper, BorderLayout.CENTER);
        
        createAccount.setMinimumSize(new Dimension(220, 250));
        
        if (str != null) {
        	JPanel pnlCenter = new JPanel(new FlowLayout(FlowLayout.CENTER));
        	JPanel pnlErrorMessage = new JPanel();
        	pnlErrorMessage.setLayout(new BoxLayout(pnlErrorMessage, BoxLayout.Y_AXIS));
        	System.out.println(str);
        	String[] messages = str.split("\\n");
        	int count = 0;
        	for (String message : messages) {
        		count++;
        		JLabel lblErrorMessage = new JLabel(message);
        		if (count > 1 || messages.length < 2) {
        			lblErrorMessage.setIcon(new ImageIcon(new ImageIcon(imgError).getImage().getScaledInstance(20, 20, Image.SCALE_FAST)));        			
        		}
        		lblErrorMessage.setAlignmentX(Component.LEFT_ALIGNMENT);
        		pnlErrorMessage.add(lblErrorMessage);
        	}
        	pnlErrorMessage.setAlignmentX(Component.CENTER_ALIGNMENT);
        	pnlCenter.add(pnlErrorMessage);
        	rootCreate.add(pnlCenter, BorderLayout.SOUTH);
        	//rootCreate.setPreferredSize(new Dimension(240, 250 + (count * 20)));
        	//createAccount.setPreferredSize(new Dimension(240, 250 + (count * 20)));
        }
        
        createAccount.add(rootCreate);
        //loginWindow.pack();
        if (exitOnClose) {
        	createAccount.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        	
        } else {
        	createAccount.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        }
        createAccount.setLocationRelativeTo(null);
        createAccount.revalidate();
        createAccount.repaint();
        createAccount.setResizable(false);
        createAccount.pack();
        loginWindow.setVisible(false);
        createAccount.setVisible(true);
    }
    
	public void displayLogin(boolean hasErrorMessage) {
    	//loginWindow.getContentPane().removeAll();
		if (rootLogin != null) {
			loginWindow.remove(rootLogin);
		}
        loginWindow.setTitle("Login");

        rootLogin = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        JLabel lblLogin = new JLabel("Username: ");
        c.gridx = 0;
        c.gridy = 0;
        c.gridheight = 1;
        c.gridwidth = 1;
        c.anchor = GridBagConstraints.EAST;
        rootLogin.add(lblLogin, c);
        
        JLabel lblPassword = new JLabel("Password: ");
        c.gridx = 0;
        c.gridy = 1;
        c.gridheight = 1;
        c.gridwidth = 1;
        rootLogin.add(lblPassword, c);
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        JButton btnLogin = new JButton("Login");
        btnLogin.setMaximumSize(new Dimension(100, 28));
        buttonPanel.add(btnLogin);
        btnLogin.setName("btnLogin");
        btnLogin.addActionListener(controller);
        
        buttonPanel.add(Box.createHorizontalStrut(40));
        JButton btnCreateAccount = new JButton("Create Account");
        btnCreateAccount.setMaximumSize(new Dimension(210, 28));
        buttonPanel.add(btnCreateAccount);
        btnCreateAccount.setName("btnCreateAccount");
        btnCreateAccount.addActionListener(controller);
        c.gridx = 0;
        c.gridy = 2;
        c.gridheight = 1;
        if (hasErrorMessage) {
        	c.gridwidth = 4;
        } else {
        	c.gridwidth = 3;        	
        }
        c.ipady = 10;
        c.anchor = GridBagConstraints.CENTER;
        rootLogin.add(buttonPanel, c);
        
        txtUsername = new JTextField();
        txtUsername.setPreferredSize(new Dimension(120, 25));
        txtUsername.addKeyListener(controller);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 0;
        c.gridheight = 1;
        c.gridwidth = 2;
        c.ipady = 0;
        c.anchor = GridBagConstraints.WEST;
        rootLogin.add(txtUsername, c);
        
        txtPassword = new JPasswordField();
        txtPassword.setPreferredSize(new Dimension(120, 25));
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 1;
        c.gridheight = 1;
        c.gridwidth = 2;
        c.ipady = 0;
        rootLogin.add(txtPassword, c);
        txtPassword.addKeyListener(controller);

        if (hasErrorMessage) {
            JImagePanel pnlError = new JImagePanel(imgError);
            c.fill = GridBagConstraints.HORIZONTAL;
            c.gridx = 4;
            c.gridy = 0;
            c.gridheight = 1;
            c.gridwidth = 1;
            c.weightx = 0;
            c.ipady = 0;
            c.anchor = GridBagConstraints.WEST;
            rootLogin.add(pnlError, c);
            
            JImagePanel pnlError2 = new JImagePanel(imgError);
            c.fill = GridBagConstraints.HORIZONTAL;
            c.gridx = 4;
            c.gridy = 1;
            c.gridheight = 1;
            c.gridwidth = 1;
            c.weightx = 0;
            c.ipady = 0;
            c.anchor = GridBagConstraints.WEST;
            rootLogin.add(pnlError2, c);
            
            JLabel lblErrorMessage = new JLabel("Login Credentails are Incorrect");
            lblErrorMessage.setIcon(new ImageIcon(imgError.getScaledInstance(13, 13, Image.SCALE_FAST)));
            c.fill = GridBagConstraints.HORIZONTAL;
            c.gridx = 0;
            c.gridy = 3;
            c.gridheight = 1;
            c.gridwidth = 5;
            c.anchor = GridBagConstraints.EAST;
            rootLogin.add(lblErrorMessage, c);
        }
        
        rootLogin.setMinimumSize(new Dimension(230, 130));
        //root.setPreferredSize(new Dimension(240, 130));
        rootLogin.setMaximumSize(new Dimension(240, 130));
        
        loginWindow.getContentPane().add(rootLogin);
        //loginWindow.getContentPane().add(root);
        if (exitOnClose) {
        	loginWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        	
        } else {
        	loginWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        }
        
        
        loginWindow.setResizable(false);
        loginWindow.setMinimumSize(new Dimension(280, 140));
        loginWindow.setMaximumSize(new Dimension(280, 140));        
        //root.revalidate();
        //root.repaint();
        //loginWindow.revalidate();
        loginWindow.setLocationRelativeTo(null);
        loginWindow.getContentPane().revalidate();
        loginWindow.getContentPane().repaint();
        loginWindow.setVisible(true);
        txtUsername.requestFocusInWindow();
        createAccount.setVisible(false);
    }
    
    /**
     * Sets the visibility of the login dialog
     * @param visible true or false
     */
    public void setVisible(boolean visible) {
    	loginWindow.setVisible(true);
    }
    
    /**
     * Gets the Username entered
     * @return char[] username
     */
    public char[] getUser() {
        return txtUsername.getText().toCharArray();
    }
    
    /**
     * Gets the password entered
     * @return char[] password
     */
    public char[] getPass() {
        return txtPassword.getPassword();
    }
    
    /**
	 * @return the txtConfirmUsername
	 */
	public char[] getConfirmUsername() {
		return txtConfirmUsername.getText().toCharArray();
	}

	/**
	 * @return the txtConfirmPassword
	 */
	public char[] getConfirmPassword() {
		return txtConfirmPassword.getPassword();
	}

    
    /**
     * Fires a Property Change
     * @param propertyName String
     * @param b1 old value boolean
     * @param b2 new value boolean
     */
    public void firePropertyChange(String propertyName, boolean b1, boolean b2) {
    	pSupport.firePropertyChange(propertyName, b1, b2);
    }
    
    /**
     * Disposes the login window
     */
    public void closeWindow() {
    	if (exitOnClose) {
    		loginWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    		createAccount.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	} else {
    		loginWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    		createAccount.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    	}
    	
    	if (createAccount.isVisible()) {
    		createAccount.dispatchEvent(new WindowEvent(createAccount, WindowEvent.WINDOW_CLOSING));
    		loginWindow.dispose();
    	} else {
    		if (loginWindow.isVisible()) {
    			loginWindow.dispatchEvent(new WindowEvent(createAccount, WindowEvent.WINDOW_CLOSING));
    			createAccount.dispose();
    		}    		
    	}
    	
    }

	@Override
	public void windowOpened(WindowEvent e) { }

	@Override
	public void windowClosing(WindowEvent e) { 
		System.out.println("Fired");
		pSupport.firePropertyChange("WindowClosed", false, true);
	}

	@Override
	public void windowClosed(WindowEvent e) { }

	@Override
	public void windowIconified(WindowEvent e) { }

	@Override
	public void windowDeiconified(WindowEvent e) { }

	@Override
	public void windowActivated(WindowEvent e) { }

	@Override
	public void windowDeactivated(WindowEvent e) { }

}
