package cryptoPrototype.login.model;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.security.auth.DestroyFailedException;
import javax.security.auth.Destroyable;

import cryptoPrototype.HashingSystem;
import cryptoPrototype.Salt;

/**
 * The Login System class provides a simple and safe way of storing login credentials, 
 * as well as methods for their modification, deletion and insertion. This class writes directly
 * to the file provided and is only suitable for a storage of a small amount of login information.
 * 
 * <p>Username and Password Storage:</p>
 * <p> x = username </br>
 *     y = password </br>
 *     s = 8 byte salt </br>
 *     h(x) = hashing function with 16 byte output </br>
 * </p>
 * <p>Stored Format: </p>
 * <p> username: h(x + s1 + h(y + s2)) + s1 </br>
 *     password: h(y + s2 + h(x + s1)) + s2
 * </p>
 * <p>This results in h(x), x, h(y), y being unknown and safe to use as they are not stored
 * 
 * @author Martin Birch
 */
public class LoginSystem extends HashingSystem implements Salt, Destroyable {

    private File database;
    private boolean destroyed;
    
    /**
     * Class Constructor
     * @param database location
     */
    public LoginSystem(File database){
        this.database = database;
        destroyed = false;
    }
    
    /**
     * Checks to see if the user login details are present
     * @param user username char[]
     * @param pass password char[]
     * @return
     */
    public boolean login(char[] user, char[] pass) {
        
        ArrayList<HashKey> users = readKeys();
       
        for (HashKey key: users) {
            if (key.equals(convertCharToByte(user), convertCharToByte(pass), this)) {
            	return true;
            }
        }
        return false;
    }
    
    /**
     * Creates a new user
     * @param user username char[]
     * @param pass password char[]
     */
    public void createUser(char[] user, char[] pass) {
        HashKey newUser = new HashKey(user, pass);
        
        try {
            FileOutputStream outStream = new FileOutputStream(database, true);
            BufferedOutputStream outBuffered = new BufferedOutputStream(outStream);
            outBuffered.write(newUser.getUser());
            outBuffered.write(newUser.getPass());
            outBuffered.flush();
            outBuffered.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(LoginSystem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LoginSystem.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
			newUser.destroy();
		} catch (DestroyFailedException e) {
			e.printStackTrace();
		}
    }
    
    /**
     * Gets the current authorisation state
     * @return true or false depending on whether the user has been successfully logged in
     */
    //public boolean getAuthorised() {
   //     return authorised;
   // }
    
    /**
     * Sets the current login state
     * @param authorised
     */
    //public void setAuthorised(boolean authorised) {
    //	this.authorised = authorised;
    //}
    
    /**
     * Reads In all the Usernames and Passwords from the file
     * @return
     */
    private ArrayList<HashKey> readKeys() {
        ArrayList<HashKey> users = new ArrayList<HashKey>();
        FileInputStream inStream;
        try {
            inStream = new FileInputStream(database.getPath());
            BufferedInputStream inBuffer = new BufferedInputStream(inStream);
            try {
            	byte[] bytes = new byte[40];
            	int i = inBuffer.read(bytes);
            	while (i != -1) {
            		HashKey h = new HashKey();
            		h.setUser(bytes);
            		bytes = new byte[40];
            		inBuffer.read(bytes);
            		h.setPass(bytes);
            		users.add(h);
            		i = inBuffer.read(bytes);
            	}
            	
            } catch (IOException e) {
            	
            } finally {
            	inBuffer.close();
            }
        } catch (FileNotFoundException ex) {
            
        } catch (IOException ex) {
        	
        }
        return users;
    }
    
    /**
     * Changes a users login in credentials
     * @param NewUser new username char[]
     * @param NewPass new password char[]
     * @param oldUser old username char[]
     * @param oldPass old password char[]
     */
    public void changeAuthenticationDetails(char[] NewUser, char[] NewPass, char[] oldUser, char[] oldPass) {
        byte[] userBytes = convertCharToByte(oldUser);
        byte[] passBytes = convertCharToByte(oldPass);

        ArrayList<HashKey> users = readKeys();
        
        for (HashKey key: users) {
            if (key.equals(userBytes, passBytes, this)) {
                //Update user key details
            	try {
					key.destroy();
				} catch (DestroyFailedException e) {
					e.printStackTrace();
				}
                //key.setUser(saltedHash(convertCharToByte(NewUser)));
                //key.setPass(saltedHash(convertCharToByte(NewPass)));
            }
        }
        users.add(new HashKey(NewUser, NewPass));
        
        try {
            FileOutputStream outStream = new FileOutputStream(database, false);
            BufferedOutputStream outBuffered = new BufferedOutputStream(outStream);
            for (HashKey key: users) {
                if (key != null && !key.isDestroyed()) {
                    outBuffered.write(key.getUser());
                    outBuffered.write(key.getPass());
                }
            }
            outBuffered.flush();
            outBuffered.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(LoginSystem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LoginSystem.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
	 * Method for validation a Password to make sure it meets the acceptable standard
	 * @param chars char[]
	 * @return true, or false, depending whether the password is valid
	 */
	public static boolean isValidPassword(char[] chars) {
		//Regex was not implemented due to requirement
		// of String which would bring in a security flaw
		int lowerCaseRequirement = 3;
		int upperCaseRequirement = 3;
		int numberRequirement = 3;
		int totalRequirementSum = lowerCaseRequirement + upperCaseRequirement + numberRequirement;
		int length = 12;
		
		int lowerMet = 0;
		int upperMet = 0;
		int numberMet = 0;
		
		if (chars.length < length) {
			return false;
		} else {
			int count = 0;
			for (char c : chars) {
				if (Character.isLowerCase(c)) {
					++lowerMet;
				} else if (Character.isUpperCase(c)) {
					++upperMet;
				} else if (Character.isDigit(c)) {
					++numberMet;
				}
				
				if (count >= totalRequirementSum) {
					if (lowerMet >= lowerCaseRequirement && 
							upperMet >= upperCaseRequirement && 
								numberMet >= numberRequirement) {
						return true;
					}
				}
				++count;
			}
		}
		return false;
	}
    
    @Override
    public void destroy() {
        database = new File("");
        destroyed = true;
    }
    
    @Override
    public boolean isDestroyed() {
        return destroyed;
    }
}
