/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptoPrototype.login.model;

import java.util.Arrays;

import javax.security.auth.DestroyFailedException;
import javax.security.auth.Destroyable;
import cryptoPrototype.HashingSystem;
import cryptoPrototype.Salt;
/**
 * Stores a hashed username, and password and allows a non hashed username and password to be compared against the hashed ones.
 * By default it assumes a Salt is used and uses the length defined in the Salt interface.
 * @author Martin Birch
 */
public class HashKey implements Salt, Destroyable {
    private byte[] user;
    private byte[] pass;
    private boolean destroyed;
    
    /**
     * Class Constructor
     */
    public HashKey() {
        user = new byte[32 + SALT_LENGTH];
        pass = new byte[32 + SALT_LENGTH];
        destroyed = false;
    }
    
    /**
     * Class Constructor, stores username and password as:
     * <p> username: h(x + s1 + h(y + s2)) + s1
     *     password: h(y + s2 + h(x + s1)) + s2 </p>
     *     @param username char[] unhashed
     *     @param password char[] unhashed
     */
    public HashKey(char[] username, char[] password) {
        HashingSystem hs = new HashingSystem();
        
        byte[] s1 = hs.createSalt();
    	byte[] s2 = hs.createSalt();
    	
    	byte[] x_s1 = new byte[username.length + SALT_LENGTH];
    	System.arraycopy(HashingSystem.convertCharToByte(username), 0, x_s1, 0, username.length);
    	System.arraycopy(s1, 0, x_s1, username.length, s1.length);
    	
    	byte[] y_s2 = new byte[password.length + SALT_LENGTH];
    	System.arraycopy(HashingSystem.convertCharToByte(password), 0, y_s2, 0, password.length);
    	System.arraycopy(s2, 0, y_s2, password.length, s2.length);
    	
        user = new byte[username.length + SALT_LENGTH + 16];
    	System.arraycopy(x_s1, 0, user, 0, x_s1.length);
    	System.arraycopy(hs.hash(y_s2), 0, user, x_s1.length, 16);
        user = hs.saltedHash(user, s1);
        
        pass = new byte[password.length + SALT_LENGTH + 16];
    	System.arraycopy(y_s2, 0, pass, 0, y_s2.length);
    	System.arraycopy(hs.hash(x_s1), 0, pass, y_s2.length, 16);
        pass = hs.saltedHash(pass, s2);
        destroyed = false;
    }
    
    
    /**
     * Sets the hashed username
     * @param user username byte[]
     */
    public void setUser(byte[] user) {
    	this.user = user;
    }
    
    /**
     * Sets the hashed password
     * @param pass password byte[]
     */
    public void setPass(byte[] pass) {
    	this.pass = pass;
    }
    
    /**
     * Gets the hashed Username
     * @return byte[] hashed username
     */
    public byte[] getUser() {
        return user;
    }
    
    /**
     * Gets the hashed password
     * @return byte[] password
     */
    public byte[] getPass() {
        return pass;
    }
    
    /**
     * Gets the salt from a byte[]
     * @param b byte[]
     * @return byte[] salt
     */
    private byte[] getSalt(byte[] b) {
        byte[] bytes = new byte[SALT_LENGTH];
        if (b.length > SALT_LENGTH) {
            System.arraycopy(b, (b.length - SALT_LENGTH), bytes, 0, SALT_LENGTH);
        }
        return bytes;
    }
    
    /**
     * Tests to see if two hash keys are the same
     * @param h HashKey
     * @return true or false depending on whether they are the same or not
     */
    public boolean equals(HashKey h) {
        return (Arrays.equals(user, h.user) && Arrays.equals(pass, h.pass));
    }
    
    /**
     * Tests whether a specified username, and password match the ones inside this HashKey.
     * Applies the salt from username, and password inside this HashKey onto the username and password specified before 
     * hashing them and testing if they are the same 
     * @param otherUser other username byte[]
     * @param otherPass other password byte[]
     * @param hs HashingSystem being used
     * @return true or false depending on whether they are the same or not
     */
    public boolean equals(byte[] otherUser, byte[] otherPass, HashingSystem hs) {
    	byte[] s1 = getSalt(user);
    	byte[] s2 = getSalt(pass);
    	
    	byte[] x_s1 = new byte[otherUser.length + SALT_LENGTH];
    	System.arraycopy(otherUser, 0, x_s1, 0, otherUser.length);
    	System.arraycopy(s1, 0, x_s1, otherUser.length, s1.length);
    	
    	byte[] y_s2 = new byte[otherPass.length + SALT_LENGTH];
    	System.arraycopy(otherPass, 0, y_s2, 0, otherPass.length);
    	System.arraycopy(s2, 0, y_s2, otherPass.length, s2.length);
    	
        byte[] userNEW = new byte[otherUser.length + SALT_LENGTH + 16];
    	System.arraycopy(x_s1, 0, userNEW, 0, x_s1.length);
    	System.arraycopy(hs.hash(y_s2), 0, userNEW, x_s1.length, 16);
    	userNEW = hs.saltedHash(userNEW, s1);
        
        byte[] passNEW = new byte[otherPass.length + SALT_LENGTH + 16];
    	System.arraycopy(y_s2, 0, passNEW, 0, y_s2.length);
    	System.arraycopy(hs.hash(x_s1), 0, passNEW, y_s2.length, 16);
    	passNEW = hs.saltedHash(passNEW, s2);
    	
        return Arrays.equals(user, userNEW) && Arrays.equals(pass, passNEW);
    }

    public void destroy() throws DestroyFailedException {
        for (int i = 0 ; i < user.length; ++i) {
        	user[i] = 'a';
        }
        for (int i = 0 ; i < pass.length; ++i) {
        	pass[i] = 'a';
        }
        destroyed = true;
    }

    public boolean isDestroyed() {
        return destroyed;
    }
}
