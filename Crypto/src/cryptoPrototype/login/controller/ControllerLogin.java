package cryptoPrototype.login.controller;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.Arrays;

import javax.swing.JButton;

import cryptoPrototype.Encryption;
import cryptoPrototype.keyVault.model.AccountDetails;
import cryptoPrototype.login.model.LoginSystem;
import cryptoPrototype.login.view.LoginGUI;
import javafx.util.Pair;
/**
 * A Controller designed for the LoginGUI which allows the user to sign in
 * or create a new account
 * @author Martin Birch
 */
public class ControllerLogin extends LoginController  {
	
	private LoginGUI loginGUI;
	private LoginSystem loginSystem;
	private boolean isDisplayingLogin;
	private PropertyChangeSupport pSupport;
	/**
	 * Class Constructor
	 * @param loginGUI LoginGUI
	 * @param database File
	 */
	public ControllerLogin(LoginGUI loginGUI, File database) {
		this.loginGUI = loginGUI;
		loginSystem = new LoginSystem(database);
		isDisplayingLogin = true;
		pSupport = new PropertyChangeSupport(this);
	}
	
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pSupport.addPropertyChangeListener(listener);
	}
	
	private void successfulLogin() {
    	Encryption encrypt = new Encryption(loginGUI.getPass());
    	AccountDetails currentUser = new AccountDetails(encrypt);
    	currentUser.setLogChanges(false);
    	currentUser.addPassword(loginGUI.getPass());
    	currentUser.addUsername(loginGUI.getUser());
    	pSupport.firePropertyChange("authorisedUser", false, new Pair<Encryption, AccountDetails>(encrypt, currentUser));
    	loginGUI.setExitOnClose(false);
    	loginGUI.closeWindow();
	}
	
	private void processCreateAccount() {
		char[] pass = loginGUI.getPass();
		char[] user = loginGUI.getUser();
		char[] cUser = loginGUI.getConfirmUsername();
		char[] cPass = loginGUI.getConfirmPassword();
		
		if (Arrays.equals(user, cUser)) {
			if (Arrays.equals(pass, cPass)) {
				if (LoginSystem.isValidPassword(pass)) {
					loginSystem.createUser(user, pass);
					loginGUI.displayLogin(false);
					isDisplayingLogin = true;
				} else {
					loginGUI.displayCreateAccount(false, false, true, false, 
										"Password must contain: \n 3 Lower case letters \n "
												+ "3 Upper case letters \n 3 numbers \n "
													+ "12 or more characters");
				}
			} else {
				loginGUI.displayCreateAccount(false, false, true, true, "Passwords do not equal");
			}
		} else {
			loginGUI.displayCreateAccount(true, true, false, false, "Usernames do not equal");
		}
	}
	
	public LoginGUI getLoginGUI() {
		return loginGUI;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			if (btn.getName().equals("btnCreateAccount")) {
				loginGUI.displayCreateAccount(false, false, false, false, null);
				isDisplayingLogin = false;
			} else if (btn.getName().equals("btnLogin")) {
				if (loginSystem.login(loginGUI.getUser(), loginGUI.getPass())) {
					successfulLogin();
				} else {
					loginGUI.displayLogin(true);
				}
			} else if (btn.getName().equals("btnCreate")) {
				processCreateAccount();
			} else if (btn.getName().equals("btnCancel")) {
				loginGUI.displayLogin(false);
				isDisplayingLogin = true;
			}
		}
	}
	
	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			if (isDisplayingLogin) {
				if (loginSystem.login(loginGUI.getUser(), loginGUI.getPass())) {
					successfulLogin();
				} else {
					loginGUI.displayLogin(true);
				}				
			} else {
				processCreateAccount();
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}
}
