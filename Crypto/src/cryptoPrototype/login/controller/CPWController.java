package cryptoPrototype.login.controller;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import cryptoPrototype.Password;
import cryptoPrototype.login.model.LoginSystem;
import cryptoPrototype.login.view.LoginGUI;

public class CPWController extends LoginController {
	
	private Password p;
	private LoginGUI view;
	
	public CPWController(Password p, LoginGUI view) {
		this.view = view;
		this.p = p;
	}
	
    @Override
    public void actionPerformed(ActionEvent e) {
    	if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			 if (btn.getName().equals("btnCreate")) {
				char[] pass = view.getPass();
				char[] user = view.getUser();
				char[] cUser = view.getConfirmUsername();
				char[] cPass = view.getConfirmPassword();
				
				if (Arrays.equals(user, cUser)) {
					if (Arrays.equals(pass, cPass)) {
						if (LoginSystem.isValidPassword(pass)) {
							Thread t1 = new Thread(new Runnable() {
								@Override
								public void run() {
			                    	p.changePassword(user, pass);
			                        JOptionPane.showMessageDialog(null, "Password Changed!", 
			                                                                "Cypto", JOptionPane.INFORMATION_MESSAGE);

								}
	                        });
	                        t1.start();
	                        view.closeWindow();
						} else {
							view.displayCreateAccount(false, false, true, false, 
												"Password must contain: \n 3 Lower case letters \n "
														+ "3 Upper case letters \n 3 numbers \n "
															+ "12 or more characters");
						}
					} else {
						view.displayCreateAccount(false, false, true, true, "Passwords do not equal");
					}
				} else {
					view.displayCreateAccount(true, true, false, false, "Usernames do not equal");
				}
			} else if (btn.getName().equals("btnCancel")) {
            	view.closeWindow();
			}
		}
    }

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}
