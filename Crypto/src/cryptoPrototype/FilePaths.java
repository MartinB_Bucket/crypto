package cryptoPrototype;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
/**
 * This interface contains the main file and folder paths used throughout the program
 * @author Martin Birch
 */
public interface FilePaths {
	public final File DATABASE_LOGIN = new File("lgn.db"); // Location of the login data
	public final File DATABASE_VAULT =  new File("data.db"); // Location of the KeyVault data
	public final File RECORD_LOG = new File("syncData.db");
	public final Path ROOT_DIRECTORY = Paths.get("Files" + File.separator ); // Location of the Encrypted File
	public final String MASTER_FILE = "info.db";
	public final int FILE_NAMES_FILE_DELIMITER_AMOUNT = 5; // The amount of characters needed to represent a delimiter
	public final Path DEFAULT_BACKGROUND_IMAGE = Paths.get("settings.config");
	
	/**
	 * Master File Format:</br>
	 * File or Folder Name </br>
	 * followed by 1 '?' </br>
	 * followed by the hash of the filename </br>
	 * followed by 5 '|' marking the end of the entry </br> </br>
	 * Example: </br>
	 * FileName?FileNameHash|||||
	 * 
	 */
}
