package cryptoPrototype;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Spliterator;

/**
 * The FileSystem class allows the master files inside the 'Files/' directory to be maintained more easily.
 * By using the methods inside this class to encrypt, create and delete files and folders the necessary 
 * entries are maintained in the master files.
 * @author Martin Birch
 */
public class FileSystem extends MasterFileEditor implements FilePaths {
	
	public FileSystem(Encryption encrypt) {
		super(encrypt);
	}
	
	/**
	 * When given a decrypted directory Path (A path without its folder names obscured),
	 * it returns the encrypted path of the file, this can be used to locate the file on the computer.
	 * <br> Requires that the file path starts from 'Files\' for example: 'Files\Documents\'
	 * <br> This method does not support file paths
	 * @param currentDirectory A decrypted file path
	 * @return Path encrypted path which can be used to locate the file
	 */
	public Path getEncryptedPath(Path currentDirectory) {
		Spliterator<Path> str = currentDirectory.spliterator();
		StringBuilder sb = new StringBuilder();

		ChangablePath previous = new ChangablePath();

		str.forEachRemaining(e -> {
			if (!(previous.getPath() == null)) {
				Path oldMasterFile = Paths.get(previous.getPath() + File.separator + MASTER_FILE);
				
				HashMap<Integer, String> fileMap = processMasterFile(oldMasterFile);
				for (Entry<Integer, String> entry : fileMap.entrySet()) {
					if (entry.getValue().equals(e.toString())) {
						sb.append(entry.getKey() + File.separator);
						previous.addToPath(entry.getKey().toString());
						break;
					}
				}
			} else {
				sb.append(e.toString() + File.separator);
				previous.addToPath(e.toString());
			}
		});

		return Paths.get(sb.toString());
	}

	public Path getDecryptedPath(Path current) {
		Path back = current;
		ChangablePath forward = new ChangablePath();
		ArrayList<String> str = new ArrayList<String>();
		
		forward.addToPath("Files");
		
		while (!back.equals(FilePaths.ROOT_DIRECTORY)) {
			str.add(getFileName(back));
			back = back.getParent();
		}

		for (int i = (str.size() - 1); i > -1; --i) {
			forward.addToPath(str.get(i));
		}
		
		return forward.getPath();
	}
	
	public boolean changeFileName(Path filePath, String newFileName) {
		return changeEntryName(Integer.valueOf(filePath.getFileName().toString()), 
				filePath.getParent().resolve(MASTER_FILE), getFileName(filePath), newFileName);
	}
	
	/**
	 * Deletes a Directory, and its content
	 * @param directory Path to delete
	 */
	public void deleteDirectory(Path directory) {
		try (DirectoryStream<Path> entries = Files.newDirectoryStream(directory))  {
			for (Path entry : entries) {
				if (Files.isDirectory(entry)) {
					deleteDirectory(entry);
				} else {
					secureDelete(entry, 2);
					//Files.delete(entry);
					removeEntry(entry);
				}
			}
		} catch (IOException e) {
			
		}
		try {
			Files.delete(directory);
			removeEntry(directory);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Should be used to delete a file under the root directory.
	 * This method ensures the entry made in the master file is removed, 
	 * as well as the file's secure deletion.
	 * @param file Path of a file
	 */
	public void deleteFile(Path file) {
		if (!Files.isDirectory(file)) {
			try {
				secureDelete(file, 2);
				//Files.delete(file);
				removeEntry(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Class used to build and append to a path
	 * @author Martin Birch
	 */
	private class ChangablePath {
		private Path p;
		
		public ChangablePath() {
			
		}
		
		public void setPath(Path p) {
			this.p = p;
		}
		
		public void addToPath(String p) {
			if (!(this.p == null)) {
				//this.p = Paths.get(this.p.toString() + File.separator + p);
				this.p = this.p.resolve(p);
			} else {
				setPath(Paths.get(p));
			}
		}
		
		public Path getPath() {
			return p;
		}
	}
	
	/**
	 * Encrypts a File, when given a path to the file and an encrypted Path location to save the file.
	 * Using this method automatically creates the necessary entry in the master file of the directory it is being saved in,
	 * it then returns the Integer filename representing the file on the computer.
	 * @param pathOfFile location to encrypt
	 * @param pathToSaveFile Encrypted Path location of the directory to save the encrypted file in
	 * @param filename new name of the file
	 * @return TODO
	 */
	public int encryptFile(Path pathOfFile, Path pathToSaveFile, String filename) {
		//Path masterFile = Paths.get(pathToSaveFile.toString() + File.separator + MASTER_FILE);
		Path masterFile = pathToSaveFile.resolve(MASTER_FILE);

		int i = 0;
		
		if (Files.exists(masterFile)) {
			i = writeNewEntry(masterFile, filename);
		} else {
			createMasterFile(masterFile);
			i = writeNewEntry(masterFile, filename);
		}
		
		try {
			//encryption.encryptFile(pathOfFile.toFile(), Paths.get(pathToSaveFile.toString()  + File.separator + i).toFile());
			encryption.encryptFile(pathOfFile.toFile(), pathToSaveFile.resolve("" + i ).toFile());
		} catch (InvalidKeyException | FileNotFoundException e) {
			e.printStackTrace();
		}
		return i;
	}
	
	/**
	 * Creates a File, when given an encrypted Path location to save the file, as well as the unencrypted filename and the bytes contents of the file.
	 * Using this method automatically creates the necessary entry in the master file of the directory it is being saved in,
	 * it then returns the Integer filename representing the file on the computer.
	 * @param pathOfFile location to encrypt
	 * @param filename new name of the file (unencrypted)
	 * @param byte[] the bytes content of the file
	 * @return TODO
	 */
	public int createFile(Path pathToSaveFile, String filename, byte[] bytes) {
		//Path masterFile = Paths.get(pathToSaveFile.toString() + File.separator + MASTER_FILE);
		Path masterFile = pathToSaveFile.resolve(MASTER_FILE);

		int i = 0;
		
		if (Files.exists(masterFile)) {
			i = writeNewEntry(masterFile, filename);
		} else {
			createMasterFile(masterFile);
			i = writeNewEntry(masterFile, filename);
		}
		
		try {
			//encryption.encryptFile(pathOfFile.toFile(), Paths.get(pathToSaveFile.toString()  + File.separator + i).toFile());
			encryption.encryptBytes(bytes, pathToSaveFile.resolve("" + i ).toFile());
		} catch (InvalidKeyException | FileNotFoundException e) {
			e.printStackTrace();
		}
		return i;
	}

	/**
	 *  Creates a directory and it's master file needed, the master file in the parent directory
	 *  is also updated, however, it should be noted this should only be used to create one drectory and
	 *  no sub directories! Therefore, all the parent directories must be already create in order to create this
	 *  directory. However, it is not necessary for the previous directory to have an exiting master file.
	 * @param pathToCreate Path encrypted location of where the directory should be created 														
	 * @param String The new directory name to create
	 * @return int representing the folder name used
	 */
	public int createDirectory(Path pathToCreate, String strDirectoryName) {
		int i = 0;

		try {
			//Path oldMasterFile = Paths.get(pathToCreate.toString() + File.separator + MASTER_FILE);
			Path oldMasterFile = pathToCreate.resolve(MASTER_FILE);
			if (!Files.exists(oldMasterFile)) {
				createMasterFile(oldMasterFile);
			}

			i = writeNewEntry(oldMasterFile, strDirectoryName);
			Path newDirectory = pathToCreate.resolve("" + i );

			Files.createDirectory(newDirectory);

			createMasterFile(newDirectory.resolve(MASTER_FILE));

		} catch (IOException e) {
			e.printStackTrace();
		}

		return i;
	}

	/**
	 * When given an encrypted Path, this method will return the real name
	 * of the file or folder at the end of the Path.
	 * @param p Path of file or folder name to get
	 * @return String of file or folder name
	 */
	public String getFileName(Path p) {
		Path directory = p.getParent().resolve(MASTER_FILE);
		HashMap<Integer, String> fileMap = processMasterFile(directory);
		return fileMap.get(Integer.parseInt(p.getFileName().toString()));
	}

}

