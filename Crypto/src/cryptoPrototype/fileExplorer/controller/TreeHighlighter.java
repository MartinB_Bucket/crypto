package cryptoPrototype.fileExplorer.controller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.JTree;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreePath;

import cryptoPrototype.fileExplorer.model.TreeCellGraphics;
/**
 * A Controller used to fire Property Change Events for events in a JTree
 * @author Martin Birch
 */
public class TreeHighlighter implements MouseListener, MouseMotionListener {
	private PropertyChangeSupport pSupport;
	
	/**
	 * Class Constructor
	 * @param listener PropertyChangeListener
	 */
	public TreeHighlighter(PropertyChangeListener listener) {
		if (listener != null) {
			pSupport = new PropertyChangeSupport(this);
			pSupport.addPropertyChangeListener(listener);
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		JTree tree = (JTree) e.getSource();
		TreeCellEditor test = tree.getCellEditor();
		Object oldPath = null;
		if (test instanceof TreeCellGraphics) {
			oldPath = ((TreeCellGraphics) test).getLastSelectedPath();
		}
		if (oldPath != null) {
			oldPath = ((TreePath) oldPath).getLastPathComponent();
		}
		
		if (pSupport != null) {
			if (tree.getSelectionPath() != null) {
				if (tree.getSelectionPath().getLastPathComponent() != null) {
					pSupport.firePropertyChange("nodeChanged", oldPath , tree.getSelectionPath().getLastPathComponent());
				}
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mouseDragged(MouseEvent e) {}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (e.getSource() instanceof JTree) {
			JTree tree = (JTree) e.getSource();
			TreePath path = tree.getPathForLocation(e.getX(), e.getY());
			if (path != null) {
				tree.startEditingAtPath(path);
			}
		}
	}

}
