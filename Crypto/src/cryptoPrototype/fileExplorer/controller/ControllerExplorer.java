package cryptoPrototype.fileExplorer.controller;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.tree.DefaultMutableTreeNode;

import cryptoPrototype.DirectoryProcessor;
import cryptoPrototype.DirectoryReader;
import cryptoPrototype.Encryption;
import cryptoPrototype.FilePaths;
import cryptoPrototype.FileSystem;
import cryptoPrototype.JImageButton;
import cryptoPrototype.cryptoPad.controller.ControllerPad;
import cryptoPrototype.cryptoPad.view.CryptoPadGUI;
import cryptoPrototype.cryptoPictureViewer.view.PictureViewer;
import cryptoPrototype.fileExplorer.model.FilePathGrouper;
import cryptoPrototype.fileExplorer.model.IGVFileHandler;
import cryptoPrototype.fileExplorer.view.ExplorerGUI;
import cryptoPrototype.fileExplorer.view.JDragNDropButton;
import cryptoPrototype.fileExplorer.view.JDragNDropPanel;
/**
 * A Controller that handles the functionality of the ExplorerGUI
 * @author Martin Birch
 */
public class ControllerExplorer implements PropertyChangeListener, WindowListener, ActionListener, FilePaths {

	private ExplorerGUI browserGUI;
	private IGVFileHandler fileHandler;
	private FilePathGrouper preSelectedFile;
	private Encryption encrypt;
	private static int filesAmount;
	
	/**
	 * Class Constructor
	 * @param browserGUI ExplorerGUI
	 * @param encrypt Encryption used on files and folders
	 * @param fileHandler IGVFileHandler
	 */
	public ControllerExplorer(ExplorerGUI browserGUI, Encryption encrypt, IGVFileHandler fileHandler) {
		this.browserGUI = browserGUI;
		this.encrypt = encrypt;
		this.fileHandler = fileHandler;
	}
	
	/**
	 * Checks if the file can be opened, and then opens it in either the crypto pad or the pciture viewer
	 * @param p Path of the file to open
	 */
	public void openFile(Path p, String realname) {
		if (Files.exists(p)) {
			try {
				if (fileHandler.isPicture(p.getParent().resolve(realname))) {
					PictureViewer picViewer = new PictureViewer(ImageIO.read(new ByteArrayInputStream(encrypt.decryptToBytes(p.toFile()))), p.getFileName().toString());
					picViewer.setVisible(true);
				} else if (fileHandler.isTextFile(p.getParent().resolve(realname))) {
					CryptoPadGUI view = new CryptoPadGUI();
					ControllerPad controller = new ControllerPad(view, p.getParent(), encrypt);
					view.addController(controller);
					controller.openFile(p.toFile());
					view.setVisible(true);
				}
			} catch (InvalidKeyException | InvalidAlgorithmParameterException | IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Used to add a file and then update the view.
	 * @param f File can be either a file or folder
	 */
	private void addFile(File f, Path locationToImport, boolean promptForDelete) {
		Path filePath = Paths.get(f.getPath());
		if (Files.isDirectory(filePath)) {
			readDirectory(filePath, locationToImport);
		} else {
			FileSystem fs = new FileSystem(encrypt);
			fs.encryptFile(filePath, locationToImport, filePath.getFileName().toString());
			fs.close();
			
			if (promptForDelete) {
				int result = JOptionPane.showConfirmDialog(browserGUI.getWindow(), "Would You Like To Securely Erase The Original File: \n" + f.getName(), 
						"Delete Original?",JOptionPane.YES_NO_OPTION);
				if (result == JOptionPane.YES_OPTION) {
					try {
						FileSystem.secureDelete(filePath, 2);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
		}
	}
	
	/**
	 * Deletes the file or folder provided and then updates the view.
	 * @param f File of a file or folder
	 */
	private void deleteFile(File f) {
		Thread t2 = new Thread(new Runnable(){
			@Override
			public void run() {
				browserGUI.getWindow().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				Path filePath = Paths.get(f.getPath());
				FileSystem fs = new FileSystem(encrypt);
				if (Files.isDirectory(filePath)) {
					fs.deleteDirectory(filePath);
				} else {
					fs.deleteFile(filePath);						
				}
				fs.close();
				browserGUI.getWindow().setCursor(Cursor.getDefaultCursor());
				browserGUI.refreshDisplay();
			}
		});
		t2.start();
	}
	
	private void processDoubleClickOnSamePath(FilePathGrouper fpg, Path pathSelected) {
		if (Files.isDirectory(pathSelected)) {
			browserGUI.getWindow().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			browserGUI.setJTreeDirectory(pathSelected);
			browserGUI.setGridDirectory(pathSelected);
			browserGUI.getWindow().setCursor(Cursor.getDefaultCursor());
			browserGUI.refreshDisplay();
		} else {
			openFile(pathSelected, fpg.getFilename());
		}
	}
	
	private void loadDifferentDirectory(PropertyChangeEvent e) {
		DefaultMutableTreeNode objNodeNewSelected =  (DefaultMutableTreeNode) e.getNewValue();
		if (objNodeNewSelected != null) {
			browserGUI.getWindow().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			browserGUI.setGridDirectory(((FilePathGrouper) objNodeNewSelected.getUserObject()).getFilepath());
			browserGUI.getWindow().setCursor(Cursor.getDefaultCursor());
			browserGUI.refreshDisplay();
		}
	}
	
	private ImageIcon loadPathImageRepresentation(FilePathGrouper fpg, Path pathSelected) {
		ImageIcon ico = null;
		if (!Files.isDirectory(pathSelected)) {
			try {
				String strFileName = fileHandler.getFiles().get(pathSelected);
				if (fileHandler.isPicture(fpg.getFilepath().getParent().resolve(fpg.getFilename()))) {
					ico = new ImageIcon(encrypt.decryptToBytes(pathSelected.toFile()));
				} else {
					ico = (ImageIcon) browserGUI.getGridView().getFileHandler().getImage(pathSelected, strFileName);
				}
			} catch (InvalidKeyException | InvalidAlgorithmParameterException | IOException e2) {
				e2.printStackTrace();
			}
		} else {
			ico = (ImageIcon) fileHandler.getImage(pathSelected, "default");
		}
		return ico;
	}
	
	private void updateFileDetailsPanel(FilePathGrouper fpg, Path pathSelected) {
		try {
			ImageIcon ico = loadPathImageRepresentation(fpg, pathSelected);
			
			ico = new ImageIcon(ico.getImage().getScaledInstance(100, 100, java.awt.Image.SCALE_FAST));
			
			browserGUI.createFileDetailsPanel(Files.readAttributes(fpg.getFilepath(), BasicFileAttributes.class, 
					LinkOption.NOFOLLOW_LINKS), fpg, ico);

		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent e) {
		if (e.getSource() instanceof JImageButton) {
			
		} else if  (e.getPropertyName().equals("nodeChanged")) {
			
			loadDifferentDirectory(e);
			browserGUI.createFileDetailsPanel(null, null, null); // Make Details Pane Empty
			
		} else if (e.getPropertyName().equals("fileSelected")) {
			FilePathGrouper fpg = (FilePathGrouper) e.getNewValue();
			Path pathSelected = fpg.getFilepath();
			
			if (pathSelected != null ) {
				if (preSelectedFile != null && pathSelected.equals(preSelectedFile.getFilepath())) {
					// If the user click on the same button switch
					processDoubleClickOnSamePath(fpg, pathSelected);
					
				} else {
					// Update File Details Panel
					updateFileDetailsPanel(fpg, pathSelected);
				}
				preSelectedFile = fpg;
			}
		} else if (e.getPropertyName().equals("navigation")) {
			if (fileHandler.getParentDirectory() != null && e.getNewValue().equals("up")) {
				browserGUI.getWindow().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				browserGUI.setJTreeDirectory(fileHandler.getParentDirectory());
				browserGUI.setGridDirectory(fileHandler.getParentDirectory());
				browserGUI.getWindow().setCursor(Cursor.getDefaultCursor());
				browserGUI.refreshDisplay();
			}
		} else if (e.getPropertyName().equals("FileToAdd")) {
			
			if (e.getSource() instanceof JDragNDropPanel) {
				// Import File into current directory of the Explorer
				importDataTo(e, fileHandler.getCurrentDirectory(), true);
			} else if (e.getSource() instanceof JDragNDropButton){
				// File was dragged over a folder, add it to that folder
				JDragNDropButton btnSource = (JDragNDropButton) e.getSource();
				FilePathGrouper fpg = (FilePathGrouper) btnSource.getClientProperty("FileInfo");
				System.out.println(fpg.getFilepath());
				importDataTo(e, fpg.getFilepath(), false);
			}
			
		}
	}
	
	private void importDataTo(PropertyChangeEvent e, Path locationToImport, boolean promptForDelete) {
		if (e.getNewValue() instanceof File) {
			Thread t1 = new Thread(new Runnable(){
				@Override
				public void run() {
					browserGUI.getWindow().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					addFile((File) e.getNewValue(), locationToImport, promptForDelete);
					browserGUI.getWindow().setCursor(Cursor.getDefaultCursor());
					browserGUI.refreshDisplay();
				}
			});
			t1.start();
		} else if (e.getNewValue() instanceof List<?>) {
			List<?> files = (List<?>) e.getNewValue();
			if (files != null) {
				if (files.get(0) instanceof File) {
					@SuppressWarnings("unchecked")
					List<File> files2 = (List<File>) files;
					Thread t1 = new Thread(new Runnable(){
						@Override
						public void run() {
							browserGUI.getWindow().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
							for (File file : files2) {
								addFile(file, locationToImport, promptForDelete);
							}
							browserGUI.getWindow().setCursor(Cursor.getDefaultCursor());
							browserGUI.refreshDisplay();
						}
					});
					t1.start();
				}
			}
		}
	}
	
	
	@Override
	public void windowOpened(WindowEvent e) {}

	@Override
	public void windowClosing(WindowEvent e) {
		browserGUI.firePropertyChange("WindowClosed", false, true);
	}

	@Override
	public void windowClosed(WindowEvent e) {}

	@Override
	public void windowIconified(WindowEvent e) {}

	@Override
	public void windowDeiconified(WindowEvent e) {}

	@Override
	public void windowActivated(WindowEvent e) {}

	@Override
	public void windowDeactivated(WindowEvent e) {}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			actionEventToolBar(e, btn);
		}
	}
	
	/**
	 * Handles the processing of a button click event on the toolbar
	 * @param e ActionEvent
	 * @param btn JButton that was pressed 
	 */
	private void actionEventToolBar(ActionEvent e, JButton btn) {
		if (btn.getName().equals("btnAdd")) {
			JFileChooser fc = new JFileChooser();
			fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			int returnedValue = fc.showDialog(browserGUI.getWindow(), "Encrypt");
			if (returnedValue == JFileChooser.APPROVE_OPTION) {
				Thread t1 = new Thread(new Runnable(){
					@Override
					public void run() {
						browserGUI.getWindow().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
						addFile(fc.getSelectedFile(), fileHandler.getCurrentDirectory(), true);
						browserGUI.getWindow().setCursor(Cursor.getDefaultCursor());
						browserGUI.refreshDisplay();
					}
				});
				t1.start();
			}
		} else if (btn.getName().equals("btnDelete")) {
			JFileChooser fc = new JFileChooser();
			fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			fc.setCurrentDirectory(fileHandler.getCurrentDirectory().toFile());
			int returnedValue = fc.showDialog(browserGUI.getWindow(), "Delete") ;
			if (returnedValue == JFileChooser.APPROVE_OPTION) {
				deleteFile(fc.getSelectedFile());
			}
		} else if (btn.getName().equals("btnExtract")) {
			JFileChooser fc = new JFileChooser();
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			fc.setCurrentDirectory(fileHandler.getCurrentDirectory().toFile());
			int returnedValue = fc.showDialog(browserGUI.getWindow(), "Extract") ;
			if (returnedValue == JFileChooser.APPROVE_OPTION) {
				extractFile(fc.getSelectedFile());
			}
		} else if (btn.getName().equals("btnRename")) {
			if (preSelectedFile != null) {
				String strNewFileName = JOptionPane.showInputDialog("New File Name:");
				if (strNewFileName != null && (!strNewFileName.equals(""))) {
					FileSystem fs = new FileSystem(encrypt);
					fs.changeFileName(preSelectedFile.getFilepath(),	strNewFileName);
					fs.close();
					browserGUI.refreshDisplay();
				}
			}
		}
	}
	
	/**
	 * Decrypts a file to the Path provided
	 * @param location Path of a directory
	 */
	private void extractFile(File location) {
		if (Files.isDirectory(preSelectedFile.getFilepath())) {
			extractFolder(location.toPath());
		} else {
			File file = preSelectedFile.getFilepath().toFile();
			String fileName = preSelectedFile.getFilename();
			File decryptedFile = new File(location.getPath() + File.separator + fileName);

			try {
				encrypt.decryptFile(file, decryptedFile);
			} catch (InvalidKeyException | InvalidAlgorithmParameterException | IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * Decrypts a folders contents to the location provided.
	 * @param p Path of a directory to extract the contents to
	 */
	private void extractFolder(Path p) {
		Path root = p.resolve(preSelectedFile.getFilename());
		
		if (!Files.exists(root)) {
			try {
				Files.createDirectory(root);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		Path currentDirectory = root;
		
		DirectoryReader reader = new DirectoryReader(new DirectoryProcessor() {
			@Override
			public boolean processDirectory(Path p, String str) {
				currentDirectory.resolve(str);
				if (!Files.exists(currentDirectory)) {
					try {
						Files.createDirectory(currentDirectory);
					} catch (IOException e) {
						e.printStackTrace();
						return false;
					}
				}
				return true;
			}
			
			@Override
			public void processFile(Path p, String str) {
				if (!str.equals("MASTER_FILE")) {
					File decryptedFile = new File(currentDirectory + File.separator + str);
					try {
						encrypt.decryptFile(p.toFile(), decryptedFile);
					} catch (InvalidKeyException | InvalidAlgorithmParameterException | IOException e) {
						e.printStackTrace();
					}
				}
			}
		}, encrypt);
		reader.readDirectory(preSelectedFile.getFilepath(), true);
	}
	
	/**
	 * Used to read a directory, and encrypt its content
	 * @param directoryToRead
	 * @param currentDirectoryInExplorer
	 */
	private void readDirectory(Path directoryToRead, Path currentDirectoryInExplorer) {
		FileSystem editor = new FileSystem(encrypt);

		//Path newFolderLocation = DirectoryReader.getNewPath(currentDirectoryInExplorer, directoryToRead.getParent(), directoryToRead);
		//Path exisitingDirectoryPath = editor.getEncryptedPath(newFolderLocation.getParent());
		int dirEName = editor.createDirectory(currentDirectoryInExplorer, directoryToRead.getFileName().toString());
		
		browserGUI.addNewNodeToParent(
				new FilePathGrouper(directoryToRead.getFileName().toString(), currentDirectoryInExplorer.resolve("" + dirEName), true),
				browserGUI.getSelectedTreeNode());
		
		int result = JOptionPane.showConfirmDialog(browserGUI.getWindow(), "Would You Like To Securely Erase The Original Directory: \n"
									+ directoryToRead.getFileName(), "Delete Original?",JOptionPane.YES_NO_OPTION);
		
		Path decryptedExplorer = editor.getDecryptedPath(currentDirectoryInExplorer);
		
		DirectoryReader reader = new DirectoryReader(new DirectoryProcessor() {
			@Override
			public boolean processDirectory(Path p, String str) {
				Path newFolderLocation = DirectoryReader.getNewPath(decryptedExplorer, directoryToRead.getParent(), p);
				Path exisitingDirectoryPath = editor.getEncryptedPath(newFolderLocation.getParent());
				int eName = editor.createDirectory(exisitingDirectoryPath, str);
				
				browserGUI.addNewNodeToParent(
						new FilePathGrouper(str, exisitingDirectoryPath.resolve("" + eName), true),
						browserGUI.getTreeNodeByPath(exisitingDirectoryPath));
				
				return true;
			}
			
			@Override
			public void processFile(Path p, String str) {
				Path newFolderLocation = DirectoryReader.getNewPath(decryptedExplorer, directoryToRead.getParent(), p);
				Path exisitingDirectoryPath = editor.getEncryptedPath(newFolderLocation.getParent());
				editor.encryptFile(p, exisitingDirectoryPath, str);
				if (result == JOptionPane.YES_OPTION) {
					try {
						FileSystem.secureDelete(p, 2);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}, encrypt);
		
		reader.readDirectory(directoryToRead, false);
		editor.close();
	}
	
	public static int countFiles(Encryption oldEncryption) {
		filesAmount = 0;
		DirectoryReader counter = new DirectoryReader(new DirectoryProcessor() {
			@Override
			public boolean processDirectory(Path p, String str) {
				return true;
			}
			
			@Override
			public void processFile(Path p, String str) {
				if (Files.exists(p)) {
					filesAmount++;
				}
			}
		}, oldEncryption);
		counter.readDirectory(ROOT_DIRECTORY, false);
		return filesAmount;
	}
	
	/**
	 * Changes the Password placed on all the files from the root Directory onwards
	 * @param oldEncryption Encryption that is currently on the files
	 * @param newEncryption New Encryption to be placed on the file
	 * @param rootDirectory Path
	 */
	public static void changePassword(Encryption oldEncryption, Encryption newEncryption, Path rootDirectory, JProgressBar bar) {
		
		DirectoryReader reader = new DirectoryReader(new DirectoryProcessor() {
			@Override
			public boolean processDirectory(Path p, String str) {
				return true;
			}
			
			@Override
			public void processFile(Path p, String str) {
				if (Files.exists(p)) {
					try {
						bar.setValue(bar.getValue() + 1);
						Path newPath = Paths.get(p.toAbsolutePath().toString() + ".temp");
						bar.setValue(bar.getValue() + 1);
						oldEncryption.changeFilePassword(p.toFile(), newPath.toFile(), newEncryption);
						bar.setValue(bar.getValue() + 7);
						FileSystem.secureDelete(p, 2);
						Files.move(newPath, p);
						bar.setValue(bar.getValue() + 1);
					} catch (InvalidKeyException | FileNotFoundException | InvalidAlgorithmParameterException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}, newEncryption);
		reader.readDirectory(rootDirectory, false);
	}
}
