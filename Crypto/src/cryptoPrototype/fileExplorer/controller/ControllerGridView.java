package cryptoPrototype.fileExplorer.controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.JButton;

import cryptoPrototype.FilePaths;
import cryptoPrototype.JImageButton;
import cryptoPrototype.fileExplorer.model.FilePathGrouper;
import cryptoPrototype.fileExplorer.model.IGVFileHandler;
import cryptoPrototype.fileExplorer.view.GridViewItemsGUI;
import cryptoPrototype.fileExplorer.view.JDragNDropButton;

public class ControllerGridView implements ActionListener, PropertyChangeListener, FilePaths {

	private IGVFileHandler model;
	private GridViewItemsGUI view;
	
	private JButton btnFileSelected;
	private PropertyChangeSupport pSupport;
	
	private boolean isNameUp;
	@SuppressWarnings("unused")
	private boolean sort;
	
	/**
	 * Class Constructor
	 * @param fileHandler The file handler to use for the retrieval of files and their names
	 */
	public ControllerGridView(IGVFileHandler fileHandler) {
		pSupport = new PropertyChangeSupport(this);
		model = fileHandler;
		sort = true;
	}
	
	/**
	 * Adds a view to which this controller will control
	 * @param view GridViewItemsGUI
	 */
	public void addView(GridViewItemsGUI view) {
		this.view = view;
		setDirectory(ROOT_DIRECTORY);
		view.addPropertyChangeListener("RefreshDisplay", this);
		view.addPropertyChangeListener("ButtonClicked", this);
	}
	
	/**
	 * Adds a Property Change Listener
	 * @param listener PropertyChangeListener
	 */
	public void addPropertyListener(PropertyChangeListener listener) {
		pSupport.addPropertyChangeListener(listener);
	}
	
	/**
	 * Loads the files and folders currently in the model into the view.
	 * And then refreshes the display.
	 */
	public void loadFilesAndFolders() {
		view.clearDisplay();
		
		/*
		ArrayList<String> files = new ArrayList<String>(model.getFiles().keySet());
		ArrayList<String> folders = new ArrayList<String>(model.getFolders().keySet());
		
		if (sort) {
			Collections.sort(files);
			Collections.sort(folders);
		} else {
			Collections.reverse(files);
			Collections.reverse(folders);
		}*/
		
		HashMap<Path, String> folderMap = model.getFolders();
		for (Map.Entry<Path, String> folder : folderMap.entrySet()) {
			FilePathGrouper fpg = new FilePathGrouper(folder.getValue(), folder.getKey(), true);
			view.addElementButton(folder.getValue(), model.getImage(folder.getKey(), folder.getValue()), true, fpg);
		}
		
		HashMap<Path, String> fileMap = model.getFiles();
		for (Map.Entry<Path, String> file : fileMap.entrySet()) {
			FilePathGrouper fpg = new FilePathGrouper(file.getValue(), file.getKey(), true);
			Icon ico = model.getImage(file.getKey(), file.getValue());
			view.addElementButton(file.getValue(), ico, false, fpg);
		}
		view.refreshDisplay();
	}
	
	/**
	 * Changes the directory being viewed
	 * @param directory Path
	 */
	public void setDirectory(Path directory) {
		if (model.setDirectory(directory)) {
			Path pre = model.getPreviousDirectory();
			if (pre != null) {
				pSupport.firePropertyChange("directoryChange", pre, model.getCurrentDirectory());
			}
		}
		loadFilesAndFolders();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			
			if (btn.getText().equals("...")) {
				pSupport.firePropertyChange("navigation", false, "up");
			} else {
				if (btnFileSelected != null) {
					btnFileSelected.setOpaque(false);
				}
			}
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getSource() instanceof JImageButton) {
			JImageButton btn = (JImageButton) evt.getSource();
			if (btn.getName().equals("NameSort")) {
				sort = !isNameUp;
				isNameUp = !isNameUp;
				loadFilesAndFolders();
			}		
		} else if (evt.getPropertyName().equals("FileToAdd")) {
			if (pSupport != null) {
				pSupport.firePropertyChange("FileToAdd", null, (File) evt.getNewValue());
			}
		} else if (evt.getPropertyName().equals("RefreshDisplay")) {
			model.refreshDirectory();
			loadFilesAndFolders();
		} else if (evt.getPropertyName().equals("ButtonClicked")) {
			JDragNDropButton btn = (JDragNDropButton) evt.getNewValue();
			btnFileSelected = btn;
			btnFileSelected.setBackground(Color.GRAY);
			btnFileSelected.setOpaque(true);
			//If the button is representing a file
			pSupport.firePropertyChange("fileSelected", false, btnFileSelected.getClientProperty("FileInfo"));
		}
	}
	
}
