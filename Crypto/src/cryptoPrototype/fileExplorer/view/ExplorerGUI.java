package cryptoPrototype.fileExplorer.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import cryptoPrototype.Encryption;
import cryptoPrototype.fileExplorer.controller.ControllerExplorer;
import cryptoPrototype.fileExplorer.controller.ControllerGridView;
import cryptoPrototype.fileExplorer.model.FilePathGrouper;
import cryptoPrototype.fileExplorer.model.GVFileHandler;
import cryptoPrototype.fileExplorer.model.IGVFileHandler;
/**
 * An Explorer that can be used to browse a directory or directories.
 * The Explorer contains a tree view and a grid view in which files and folders
 * are loaded into. The grid view used in the Explorer also supports DragNDrop.
 * @author Martin Birch
 */
public class ExplorerGUI {
	
	private GridViewItemsGUI pnlGridView;
	private TreeView jTree;
	private JPanel pnlFileDetails;
	private PropertyChangeSupport pSupport;
	private JPanel pnlRoot;
	private JFrame window;
	private ControllerExplorer controller;
	private ControllerGridView gridController;
	private String rootDirectory;
	private IGVFileHandler fileHandler;
	private Encryption encrypt;
	
	/**
	 * Class Constructor, creates a visible Explorer
	 * @param fileHandler The root Directory to start browsing from
	 */
	public ExplorerGUI(GVFileHandler fileHandler, Encryption encrypt) {
		super();
		this.encrypt = encrypt;
		this.rootDirectory = fileHandler.getCurrentDirectory().toString();
		this.fileHandler = fileHandler;
		pSupport = new PropertyChangeSupport(this);
		window = new JFrame();
		window.setTitle("File Explorer");
	}
	
	/**
	 * Gets the JFrame
	 * @return JFrame as a component
	 */
	public Component getWindow() {
		return window;
	}
	
	/**
	 * Used to create the basic screen widgets
	 */
	private void createGUI() {
		window.getContentPane().removeAll();
		pnlRoot = new JPanel(new BorderLayout());
		pnlRoot.add(createHeaderPanel(), BorderLayout.NORTH);  /// ----- SLOW
		
		//Create JTree
		jTree = new TreeView(Paths.get(rootDirectory), fileHandler.getEncryption());        /// ----- SLOW
		JScrollPane scrJTree = new JScrollPane(jTree);
		Border outside = BorderFactory.createLoweredBevelBorder();
		//pnlFileDetails.setBorder(BorderFactory.createCompoundBorder(outside, inside));
		scrJTree.setBorder(outside);
		scrJTree.setAutoscrolls(true);
		scrJTree.setPreferredSize(new Dimension(200, 630));
		scrJTree.setMinimumSize(new Dimension(140, 200));
		
		//Create GridView
		gridController = new ControllerGridView(fileHandler);
		pnlGridView = new GridViewItemsGUI(fileHandler, gridController, encrypt);    /// ---- SLOW
		gridController.addView(pnlGridView);
		gridController.addPropertyListener(controller);
		
		pnlGridView.addPropertyChangeListener(controller); ///////////////////////////////////////////////////////////
		pnlGridView.addPropertyChangeListenerToItems(controller);
		
		JScrollPane scrGridView = new JScrollPane(pnlGridView);
		scrGridView.getVerticalScrollBar().setUnitIncrement(14);
		
		//Create Panel to Display File Details
		createFileDetailsPanel(null, null, null);
		
		if (controller != null) {
			jTree.addPropertyListener(controller);
			//gridController.addPropertyListener(controller);
			window.addWindowListener(controller);
		}
		
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scrJTree, scrGridView);
		splitPane.setOneTouchExpandable(true);
		splitPane.setDividerLocation(200);
		
		pnlRoot.add(splitPane, BorderLayout.CENTER);
		window.add(pnlRoot);
		window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		window.setSize(800, 500);
		window.setLocationRelativeTo(null);
	}
	
	 /**
	  *   Creates a toolbar for the explorer
	  * @return JToolBar
	  */
    private JToolBar createToolBar() {
        JToolBar tb = new JToolBar();
        JButton b;
        
        b = new JButton("Add");
        b.setRequestFocusEnabled(false);
		b.setName("btnAdd");
		b.addActionListener(controller);
        tb.add(b);
        
        b = new JButton("Extract");
        b.setRequestFocusEnabled(false);
		b.setName("btnExtract");
		b.addActionListener(controller);
        tb.add(b);
        
        b = new JButton("Delete");
        b.setRequestFocusEnabled(false);
		b.setName("btnDelete");
		b.addActionListener(controller);
        tb.add(b);
        
        b = new JButton("Rename");
        b.setRequestFocusEnabled(false);
		b.setName("btnRename");
		b.addActionListener(controller);
        tb.add(b);
        
        tb.setFloatable(false);
        return tb;
    }
	
	/**
	 * Creates a Header Panel, containing add and delete buttons 
	 * @return JPanel
	 */
	private JPanel createHeaderPanel() {
		JPanel pnlHeader = new JPanel(new FlowLayout());
		//pnlHeader.setBackground(SystemColor.window);
		//pnlHeader.setBorder(BorderFactory.createRaisedBevelBorder());
		FlowLayout layout = (FlowLayout) pnlHeader.getLayout();
		layout.setHgap(0);
		layout.setVgap(0);
		pnlHeader.setPreferredSize(new Dimension(700, 30));
		pnlHeader.setMaximumSize(new Dimension(700, 30));
		pnlHeader.add(createToolBar());
		return pnlHeader;
	}
	
	/**
	 * Creates and adds a JPanel that can display a files details to the bottom of the pnlRoot JPanel
	 * @param attrs BasicFileAttributes of a file
	 * @param p Path of the file
	 * @param ico ImageIcon to display 
	 */
	public void createFileDetailsPanel(BasicFileAttributes attrs, FilePathGrouper fpg, ImageIcon ico) {
		try {
			pnlRoot.remove(pnlFileDetails);
			pnlFileDetails.removeAll();
		} catch (Exception e) {
			
		}
		
		pnlFileDetails = new JPanel(new BorderLayout());
		//pnlFileDetails.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
		Border outside = BorderFactory.createLoweredBevelBorder();
		Border inside = BorderFactory.createEmptyBorder(0, 10, 0, 0);
		pnlFileDetails.setBorder(BorderFactory.createCompoundBorder(outside, inside));
		//((BorderLayout) pnlFileDetails.getLayout()).
		pnlFileDetails.setPreferredSize(new Dimension(750, 120));
		JPanel pnlLabels = new JPanel();
		pnlLabels.setLayout(new BoxLayout(pnlLabels, BoxLayout.Y_AXIS));
		
		JLabel lblFilePicture = new JLabel();
		JLabel lblFileSelectedName = new JLabel();
		JLabel lblFileSelectedEncrypted = new JLabel();
		JLabel lblFileSize = new JLabel();
		JLabel lblFileCreatedOn = new JLabel();
		JLabel lblFileLastModified = new JLabel();

		if (attrs != null && fpg != null && fpg.getFilepath() != null) {
			if (ico != null) {
				lblFilePicture.setIcon(ico);
			}
			lblFileSelectedName.setText("File Name: " + fpg.getFilename());
			lblFileSelectedEncrypted.setText("Encrypted Name : " + fpg.getEncryptedFileName());
			lblFileSize.setText("File Size: " + getDisplaySize(attrs.size()));
			lblFileCreatedOn.setText("Created On: " + attrs.creationTime().toString().replace("T", " ").substring(0, 19));
			lblFileLastModified.setText("Last Modified: " + attrs.lastModifiedTime().toString().replace("T", " ").substring(0, 19));
		}
		
		pnlLabels.add(Box.createVerticalGlue());
		pnlLabels.add(lblFileSelectedName);
		pnlLabels.add(lblFileSelectedEncrypted);
		pnlLabels.add(lblFileSize);
		pnlLabels.add(lblFileCreatedOn);
		pnlLabels.add(lblFileLastModified);
		pnlLabels.add(Box.createVerticalGlue());
		pnlFileDetails.add(lblFilePicture, BorderLayout.WEST);
		lblFilePicture.setPreferredSize(new Dimension(120, 120));
		
		pnlFileDetails.add(pnlLabels, BorderLayout.CENTER);
		pnlLabels.setPreferredSize(new Dimension(750, 120));

		pnlRoot.add(pnlFileDetails, BorderLayout.SOUTH);
		window.revalidate();
		window.repaint();
	}
	
	/**
	 * Refreshes the pnlGirdView Panel and the JFrame
	 */
	public void refreshDisplay() {
		pnlGridView.getFileHandler().refreshDirectory();
		gridController.loadFilesAndFolders();
		pnlGridView.revalidate();
		pnlGridView.repaint();
		window.revalidate();
		window.repaint();
	}
	
	/**
	 * Sets the directory currently being viewed for the grid view
	 * @param p Directory to be viewed
	 */
	public void setGridDirectory(Path p) {
		gridController.setDirectory(p);
	}
	
	public DefaultMutableTreeNode getSelectedTreeNode() {
		TreePath selectedPaths = jTree.getSelectionPath();
		if (selectedPaths != null) {
			return (DefaultMutableTreeNode) selectedPaths.getLastPathComponent(); 
		}
		return null;
	}
	
	public void addNewNodeToParent(FilePathGrouper fpg, DefaultMutableTreeNode parent) {
		DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(fpg);
		if (parent != null) {
			parent.add(newNode);			
		} else {
			((DefaultMutableTreeNode) jTree.getModel().getRoot()).add(newNode);
		}
	}
	
	public void removeNode(DefaultMutableTreeNode node) {
		DefaultMutableTreeNode node2 = ((DefaultMutableTreeNode) node.getParent());
		node2.remove(node);
		jTree.setSelectionPath(new TreePath(node2.getPath()));
	}
	
	public DefaultMutableTreeNode getTreeNodeByPath(Path p) {
		return (DefaultMutableTreeNode) jTree.getTreePathByValue(p.toString()).getLastPathComponent();
	}
	
	/**
	 * Sets the directory currently being viewed for the JTree
	 * @param p Directory to be viewed
	 */
	public void setJTreeDirectory(Path p) {
		jTree.setSelectionPath(jTree.getTreePathByValue(p.toString()));
	}
	
	/**
	 * Gets the size to display given a size in bytes
	 * @param size bytes
	 * @return String size in B, KB, MB, GB, or TB
	 */
	public String getDisplaySize(long size) {
		int i = 0;
		long newSize = size;
		while (newSize >= 1024) {
			newSize = (long) (newSize / 1024.00);
			++i;
		}
		String strSize = null;
		if (i == 0) {
			strSize = String.valueOf(newSize) + " B";
		} else if (i == 1) {
			strSize = String.valueOf(newSize) + " KB";
		} else if (i == 2) {
			strSize = String.valueOf(newSize) + " MB";
		} else if (i == 3) {
			strSize = String.valueOf(newSize) + " GB";
		} else if (i == 4) {
			strSize = String.valueOf(newSize) + " TB";
		}
		return strSize;
	}

	/**
	 * Gets the GirdViewPanel
	 * @return JComponent
	 */
	public GridViewItemsGUI getGridView() {
		return pnlGridView;
	}
	
	/**
	 * Adds a controller
	 * @param con ControllerExplorer
	 */
	public void addController(ControllerExplorer con) {
		controller = con;
		createGUI();
	}
	
	/**
	 * Adds a PropertyChangeListener
	 * @param listener PropertyChangeListener
	 */
	public void addPropertyListener(PropertyChangeListener listener) {
		pSupport.addPropertyChangeListener(listener);
	} 

	/**
	 * Fires a PropertyChange
	 * @param property name String
	 * @param b1 old value boolean
	 * @param b2 new value boolean
	 */
	public void firePropertyChange(String property, boolean b1, boolean b2) {
		pSupport.firePropertyChange(property, b1, b2);
	}
	
	/**
	 * Gets the root directory being used
	 * @return String root directory
	 */
	public String getRootDirectory() {
		return rootDirectory;
	}

	/**
	 * Sets the window visibility
	 * @param b true or false
	 */
	public void setVisible(boolean b) {
		createGUI();
		window.setVisible(b);
	}
}
