package cryptoPrototype.fileExplorer.view;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.TransferHandler;
/**
 * This JPanel supports files being dragged into it as a method to import them.
 * This method does not handle the importing of files, instead it fires a PropertyChange
 * that is later handled in the Controller for the explorer.
 * @author Martin Birch
 */
public class JDragNDropPanel extends JPanel {
	
	private static final long serialVersionUID = -2862113107056325449L;
	private TransferHandler transferHandler;
	
	/**
	 * Class Constructor, creates and links in the transfer handler needed
	 */
	public JDragNDropPanel() {
		createHandler();
		linkInTransferHandler();
	}
    
	/**
	 * Creates the transfer handler
	 */
	private void createHandler() {
		transferHandler = new TransferHandler() {

			private static final long serialVersionUID = 1L;
			
			public boolean canImport(TransferHandler.TransferSupport support) {
			    // we only import Strings
			    if (!support.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
			        return false;
			    }

			    // check if the source actions (a bitwise-OR of supported actions)
			    // contains the COPY action
			    boolean copySupported = (COPY & support.getSourceDropActions()) == COPY;
			    if (copySupported) {
			        support.setDropAction(COPY);
			        return true;
			    }

			    // COPY is not supported, so reject the transfer
			    return false;
			}
			
			@SuppressWarnings("unchecked")
			public boolean importData(TransferSupport supp) {
			    if (!canImport(supp)) {
			        return false;
			    }

			    // Fetch the Transferable and its data
			    Transferable t = supp.getTransferable();
			    List<File> data = null;
			    try {
					data = (List<File>) t.getTransferData(DataFlavor.javaFileListFlavor);
					addFile(data);
				} catch (UnsupportedFlavorException | IOException e) {
					e.printStackTrace();
				}

			    return true;
			}
		};
	}
	
	/**
	 * Removes the transfer handler but keeps a copy of it
	 * so it can be added back later
	 */
	public void unlinkTransferHandler() {
		setTransferHandler(null);
	}
	
	/**
	 * Adds the Transfer Handler back in
	 */
	public void linkInTransferHandler() {
		setTransferHandler(transferHandler);
	}
	
	private void addFile(List<File> f) {
		//This is later handled in the Controller for the ExplorerGUI
		firePropertyChange("FileToAdd", null, f);
	}
}
