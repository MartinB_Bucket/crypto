package cryptoPrototype.fileExplorer.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import cryptoPrototype.Encryption;
import cryptoPrototype.FilePaths;
import cryptoPrototype.JImageButton;
import cryptoPrototype.fileExplorer.controller.ControllerGridView;
import cryptoPrototype.fileExplorer.model.FilePathGrouper;
import cryptoPrototype.fileExplorer.model.IGVFileHandler;
/**
 * A JPanel that can be populated with folders and files in a grid (NOT YET IMPLEMENTED) or a list.
 * This grid view supports the dragging and dropping of files as a method or import and export them.
 * @author Martin Birch
 */
public class GridViewItemsGUI extends JDragNDropPanel implements FilePaths, PropertyChangeListener {

	private static final long serialVersionUID = 239444524757445719L;
	
	protected ArrayList<String> files;
	protected ArrayList<String> folders;
	
	public static final int GRID_VIEW = 1;
	public static final int DETAILS_VIEW = 2;
	
	public static final int DISPLAY_SIZE_SMALL = 3;
	public static final int DISPLAY_SIZE_MEDUIM = 4;
	public static final int DISPLAY_SIZE_LARGE = 5;

	private IGVFileHandler fileSystem;
	
	private int view;
	private JPanel pnlMain;
	private JPanel pnlHeader;
	private ControllerGridView controller;
	private Encryption encrypt;
	private PropertyChangeListener listenerExplorer;
	/**
	 * Class Constructor
	 * @param fileSystem The IGVHandler to use for processing the files and folders
	 */
	public GridViewItemsGUI(IGVFileHandler fileSystem, ControllerGridView con, Encryption encrypt) {
		this.encrypt = encrypt;
		//ssetBackground(Color.LIGHT_GRAY);
		this.fileSystem = fileSystem;
		controller = con;
		view = DETAILS_VIEW;
		fileSystem.setRootDirectory(ROOT_DIRECTORY);
		setLayout(new BorderLayout());
		setBorder(null);
		clearDisplay();   ///  --- SLOW
		createHeader();   ///  --- SLOW
		//createDisplay();
		add(pnlHeader, BorderLayout.NORTH);
		add(pnlMain, BorderLayout.CENTER);
	}
	
	public void addPropertyChangeListenerToItems(PropertyChangeListener listener) {
		listenerExplorer = listener;
	}
	
	/**
	 * Adds a object implementing IGVFileHandler to handle the processing of files
	 * @param fileSystem IGVFileHandler
	 */
	public void addFileHandler(IGVFileHandler fileSystem) {
		this.fileSystem = fileSystem;
	}
	
	/**
	 * Return the IGVFileHandler
	 * @return IGVFileHandler
	 */
	public IGVFileHandler getFileHandler() {
		return fileSystem;
	}
	
	/**
	 * Used to create a file or folder button 
	 * @param name Text to display
	 * @param ico Icon to display
	 * @param folder is the Path a folder or file
	 * @param info FilePathGrouper
	 * @return JButton
	 */
	public void addElementButton(String name, Icon ico, boolean folder, FilePathGrouper info) {
		JDragNDropButton btnItem = new JDragNDropButton(name, encrypt);
		btnItem.addPropertyChangeListener("DraggingButton", this);
		btnItem.addPropertyChangeListener("FileRemoved", this);
		btnItem.addPropertyChangeListener("ButtonClicked", this);
		btnItem.putClientProperty("FileInfo", info);
		btnItem.setMinimumSize(new Dimension(150, 45));
		btnItem.setIcon(ico);
		btnItem.setBorder(BorderFactory.createEmptyBorder(3, 5, 3, 3));
		btnItem.addActionListener(controller);
		
		if (listenerExplorer != null) {
			btnItem.addPropertyChangeListener("FileToAdd", listenerExplorer);
		}
				
		btnItem.setBackground(Color.LIGHT_GRAY);
		if (!folder) {
			btnItem.setAllowImport(false);
		}
		pnlMain.add(btnItem);
	}
	
	/**
	 * Used to create a header, with sorting options
	 */
	private void createHeader() {
		if (pnlHeader != null) {
			pnlHeader.removeAll();
		} else {
				pnlHeader = new JPanel();
				FlowLayout layout = new FlowLayout(FlowLayout.LEFT);
				layout.setHgap(1);
				pnlHeader.setBorder(BorderFactory.createEmptyBorder());
				pnlHeader.setLayout(layout);
				pnlMain.setOpaque(false);
				pnlHeader.setOpaque(true);
				pnlHeader.setPreferredSize(new Dimension(400, 30));
				pnlHeader.setMaximumSize(new Dimension(900, 30));
				//pnlHeader.setBackground(Color.LIGHT_GRAY);
				pnlHeader.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
		}
		
		JImageButton btnNameSort = new JImageButton(
				new ImageIcon(getClass().getResource("/Images/explorer/Name.png")).getImage(),
				new ImageIcon(getClass().getResource("/Images/explorer/Name2.png")).getImage());
		btnNameSort.setPreferredSize(new Dimension(80, 28));
		btnNameSort.setChangeImageOnHover(false);
		btnNameSort.setOpaque(false);
		btnNameSort.setChangeOnClick(true);
		btnNameSort.setName("NameSort");
		btnNameSort.setBackground(Color.LIGHT_GRAY);
		btnNameSort.addPropertyChangeListener(controller);
		pnlHeader.add(btnNameSort);
	}
	
	/**
	 * Resets the GridView to an empty state
	 */
	public void clearDisplay() {
		if (pnlMain != null) {
			pnlMain.removeAll();
		} else {
			pnlMain = new JPanel();
			pnlMain.setLayout(new BoxLayout(pnlMain, BoxLayout.Y_AXIS));
			pnlMain.setOpaque(false);
		}
		if (view == DETAILS_VIEW) {
			JButton btnBack = new JButton("...");
			btnBack.setBorder(BorderFactory.createEmptyBorder(3, 5, 3, 3));
			btnBack.addActionListener(controller);
			btnBack.setBackground(Color.LIGHT_GRAY);
			pnlMain.add(btnBack);
		}
	}

	/**
	 * Refreshes the files and folders, and layout of the JPanel
	 */
	public void refreshDisplay() {
		pnlMain.revalidate();
		pnlMain.repaint();
	}
	
	/**
	 * Sets the display size of the images
	 * @param size Display Size
	 */
	public void setDisplaySize(int size) {
		switch (size) {
		case DISPLAY_SIZE_SMALL: 
			fileSystem.setSize(15,15);
			break;
		case DISPLAY_SIZE_MEDUIM:
			fileSystem.setSize(25,25);
			break;
		case DISPLAY_SIZE_LARGE:
			fileSystem.setSize(35,35);
			break;
		default:
			fileSystem.setSize(25,25);
			break;
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals("DraggingButton")) {
			if ((boolean) evt.getNewValue()) {
				unlinkTransferHandler();
			} else {
				linkInTransferHandler();
			}
		} else if (evt.getPropertyName().equals("FileRemoved")) {
			firePropertyChange("RefreshDisplay", false, true);
		} else if (evt.getPropertyName().equals("ButtonClicked")) {
			firePropertyChange("ButtonClicked", false, evt.getNewValue());
		}
	}
	
}
