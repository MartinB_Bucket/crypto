package cryptoPrototype.fileExplorer.view;

import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.HashMap;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import cryptoPrototype.Encryption;
import cryptoPrototype.FilePaths;
import cryptoPrototype.MasterFileEditor;
import cryptoPrototype.fileExplorer.controller.TreeHighlighter;
import cryptoPrototype.fileExplorer.model.FilePathGrouper;
import cryptoPrototype.fileExplorer.model.TreeCellGraphics;
import cryptoPrototype.fileExplorer.model.TreeGraphics;

/**
 * A JTree used for displaying the folders in a directory, uses custom graphics
 * @author Martin Birch
 */
public class TreeView extends JTree implements FilePaths {
	private static final long serialVersionUID = 4152339956906081794L;

	/**
	 * Class Constructor
	 * @param directory Path to load
	 */
	public TreeView(Path directory, Encryption encrypt) {
		super(readDirectoryNew(directory, encrypt, null));
		setEditable(true);
		TreeGraphics graph = new TreeGraphics();
		setCellRenderer(graph);
		setCellEditor(new TreeCellGraphics(graph));
		setRowHeight(graph.getRowHeight());
	}

	/**
	 * Adds a Property Change Listener
	 * @param listener PropertyChangeListener
	 */
	public void addPropertyListener(PropertyChangeListener listener) {
		TreeHighlighter myHighlighter = new TreeHighlighter(listener);
		addMouseListener(myHighlighter);
		addMouseMotionListener(myHighlighter);
	}
	
	/**
	 * Reads a Directory and makes a node tree for the root directory which contains all sub directories
	 * @param current Path of root directory
	 * @return DefaultMutableTreeNode root node
	 */
	public static DefaultMutableTreeNode readDirectoryNew(Path current, Encryption encrypt, MasterFileEditor editor) {
		if (editor == null) {
			 editor = new MasterFileEditor(encrypt);			
		}
		DefaultMutableTreeNode root = null;
		
		// Get Real Name of File
		try {
			if (!Files.isSameFile(current, ROOT_DIRECTORY)) {
				HashMap<Integer, String> files = editor.processMasterFile(current.getParent().resolve(MASTER_FILE));
				
				Integer intFileNumber = Integer.parseInt(current.getFileName().toString());
				
				root = new DefaultMutableTreeNode(new FilePathGrouper(files.get(intFileNumber), current, true));
				
			} else {
				root = new DefaultMutableTreeNode(new FilePathGrouper("Files", current, true));
			}
		} catch (NumberFormatException | IOException e1) {
			e1.printStackTrace();
		}
		
		try (DirectoryStream<Path> entries = Files.newDirectoryStream(current))  {
			for (Path entry : entries) {
				if (Files.isDirectory(entry) && !Files.isHidden(entry)) {
					DefaultMutableTreeNode newNode = readDirectoryNew(entry, encrypt, editor);
					root.add(newNode);
				}
			}
		} catch (IOException e) {
			
		}
		return root;
	}
	
	/**
	 * Gets A Tree Path from this JTree by its node value
	 * @param s String of node to look for
	 * @return TreePath of the node being searched for, or null if it is not found
	 */
	public TreePath getTreePathByValue(String s) {
		@SuppressWarnings("unchecked")
		Enumeration<DefaultMutableTreeNode> e = ((DefaultMutableTreeNode) getModel().getRoot()).depthFirstEnumeration();
	    while (e.hasMoreElements()) {
	        DefaultMutableTreeNode node = e.nextElement();
	        if (node.toString().equalsIgnoreCase(s)) {
	            return new TreePath(node.getPath());
	        }
	    }
	    return null;
	}
}
