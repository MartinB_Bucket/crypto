package cryptoPrototype.fileExplorer.view;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.TransferHandler;

import cryptoPrototype.Encryption;
import cryptoPrototype.FileSystem;
import cryptoPrototype.fileExplorer.model.FilePathGrouper;
/**
 * JButton that supports the dragging and dropping of files out of it.
 * The information about the file being dropped is contained as a ClientProperty.
 * For the file to be exported a temporally file is created in a folder called 'Extracted' and
 * is then deleted afterwards
 * @author Martin Birch
 */
public class JDragNDropButton extends JButton  {

	private static final long serialVersionUID = -4718540390244373068L;
	private Encryption encrypt;
	private boolean dragging;
	private TransferHandler transferHandler;
	private boolean importAllowed;
	
	/**
	 * Class Constructor
	 * @param name The text to be displayed on the button
	 * @param encrypt Encryption object used on the file
	 */
	public JDragNDropButton(String name, Encryption encrypt) {
		super(name);
		importAllowed = true;
		this.encrypt = encrypt;
		createHandler();
		linkInTransferHandler();
		JDragNDropButton btn = this;
		this.addMouseListener(new MouseAdapter() {
			@Override
		    public void mousePressed(MouseEvent e) {
				//Button Click Event is handled this way so it is processed before
				//The exportDone method is called
				firePropertyChange("ButtonClicked", false, btn);
			}
		});
		this.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                JButton button = (JButton) e.getSource();
                TransferHandler handle = button.getTransferHandler();
                firePropertyChange("DraggingButton", dragging, true);
                handle.exportAsDrag(button, e, TransferHandler.MOVE);
            button.setDropTarget(new DropTarget() {
            	
            });
                dragging = true;
            }
            
        });
	}
    
	/**
	 * This method creates the transfer handler
	 */
	private void createHandler() {
		transferHandler = new TransferHandler() {

			private static final long serialVersionUID = 1L;
			
			public int getSourceActions(JComponent c) {
			    return COPY_OR_MOVE;
			}

			public Transferable createTransferable(JComponent c) {
				FilePathGrouper fpg = (FilePathGrouper) c.getClientProperty("FileInfo");
				if (fpg != null) {
					File file = fpg.getFilepath().toFile();
					FileSystem fs = new FileSystem(encrypt);
					String fileName = fs.getFileName(fpg.getFilepath());
					File extracted = new File("Extracted" + File.separator + fileName);
					try {
						Path extractedFolder = Paths.get("Extracted");
						if (!Files.exists(extractedFolder)) {
							Files.createDirectory(extractedFolder);
						}
						encrypt.decryptFile(file, extracted);
					} catch (InvalidKeyException | InvalidAlgorithmParameterException | IOException e) {
						e.printStackTrace();
					}
					return new FileTransferer(extracted, fpg.getFilepath());
				}
			    //return new StringSelection(c.getName());
				return null;
			}
			
			@SuppressWarnings("unchecked")
			protected void exportDone(JComponent source, Transferable data, int action) {
				List<File> lst = null;
				try {
					lst = (List<File>) data.getTransferData(DataFlavor.javaFileListFlavor);
				} catch (UnsupportedFlavorException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (data instanceof FileTransferer) {
					if (action == DnDConstants.ACTION_MOVE) {
						Path sourcePath = ((FileTransferer) data).getOriginalSource();
						if (Files.exists(sourcePath)) {
							FileSystem fs = new FileSystem(encrypt);
							fs.deleteFile(sourcePath);
							fs.close();
							firePropertyChange("FileRemoved", false, true);
						}
					}
					//Delete the temp decrypted File
					if (lst != null) {
						for (File f : lst) {
							try {
								FileSystem.secureDelete(f.toPath(), 2);
							} catch (IOException e) {
								e.printStackTrace();
							}
							//f.delete();
						}
					}
				}
				//Notify Listeners that the drag has ended
            	firePropertyChange("DraggingButton", dragging, false);
            	dragging = false;
			}
		
			public boolean canImport(TransferHandler.TransferSupport support) {
				if (!importAllowed) {
					return false;
				}
				
				// we only import Strings
			    if (!support.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
			        return false;
			    }

			    // check if the source actions (a bitwise-OR of supported actions)
			    // contains the COPY action
			    boolean copySupported = (COPY & support.getSourceDropActions()) == COPY;
			    if (copySupported) {
			        support.setDropAction(COPY);
			        return true;
			    }

			    // COPY is not supported, so reject the transfer
			    return false;
			}
			
			@SuppressWarnings("unchecked")
			public boolean importData(TransferSupport supp) {
			    if (!canImport(supp)) {
			        return false;
			    }

			    // Fetch the Transferable and its data
			    Transferable t = supp.getTransferable();
			    List<File> data = null;
			    try {
					data = (List<File>) t.getTransferData(DataFlavor.javaFileListFlavor);
					addFile(data);
				} catch (UnsupportedFlavorException | IOException e) {
					e.printStackTrace();
				}

			    return true;
			}
		};
	}

	public void setAllowImport(boolean flag) {
		importAllowed = flag;
	}
	
	private void addFile(List<File> f) {
		//This is later handled in the Controller for the ExplorerGUI
		firePropertyChange("FileToAdd", null, f);
	}
	
	/**
	 * Removes the transfer handler but keeps a copy of it
	 * so it can be added back later
	 */
	public void unlinkTransferHandler() {
		setTransferHandler(null);
	}
	
	/**
	 * Adds the Transfer Handler back in
	 */
	public void linkInTransferHandler() {
		setTransferHandler(transferHandler);
	}
	
	/**
	 * Class used to handle to transfer of the file
	 * @author Martin Birch
	 */
	private class FileTransferer implements Transferable {

	    private final DataFlavor[] flavors;
	    
	    private final List<DataFlavor> flavorList;

	    private File decryptedfile;
	    private Path originalFile;
	    
	    public FileTransferer(File decryptedfile, Path originalSource) {
	    	this.decryptedfile = decryptedfile;
	    	originalFile = originalSource;
	    	flavors = new DataFlavor[] {
	    	        DataFlavor.javaFileListFlavor
	    	    };
	    	flavorList = Arrays.asList( flavors );
	    }
		
		@Override
		public DataFlavor[] getTransferDataFlavors() {
			return flavors;
		}

		@Override
		public boolean isDataFlavorSupported(DataFlavor flavor) {
			return flavorList.contains(flavorList);
		}

		@Override
		public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
			
			if (flavor == DataFlavor.javaFileListFlavor) {
				List<File> fileList = new ArrayList<File>();
				
				fileList.add(decryptedfile);
				return fileList;
			}
			
			return null;
		}
		
		public Path getOriginalSource() {
			return originalFile;
		}
	}
}
