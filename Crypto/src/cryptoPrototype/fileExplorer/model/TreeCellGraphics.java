package cryptoPrototype.fileExplorer.model;

import java.awt.Component;
import java.util.EventObject;

import javax.swing.JTree;
import javax.swing.event.CellEditorListener;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;
/**
 * TreeCellEditor for JTree
 * @author Martin Birch
 */
public class TreeCellGraphics implements TreeCellEditor {
	private TreeCellRenderer renderer;
	private boolean shouldSelect;

	private Component comp;
	private TreePath lastSelected;
	
	/**
	 * Class Constructor
	 * @param renderer TreeCellRenderer being used in JTree
	 */
	public TreeCellGraphics(TreeCellRenderer renderer) {
		this.renderer = renderer;
	}

	@Override
	public Component getTreeCellEditorComponent(JTree tree, Object value, boolean isSelected, boolean expanded,
			boolean leaf, int row) {

		lastSelected = tree.getSelectionPath();
		
		if (tree != null && value != null) {
			comp = renderer.getTreeCellRendererComponent(tree, value, shouldSelect, expanded, leaf, row, true);
		}
		
		return comp;
	}

	/**
	 * Gets the last selected path
	 * @return TreePath of the last selected path
	 */
	public TreePath getLastSelectedPath() {
		return lastSelected;
	}


	@Override
	public Object getCellEditorValue() {
		return comp;
	}

	@Override
	public boolean isCellEditable(EventObject anEvent) {
		if (anEvent != null) {
			shouldSelect = true;
			if (anEvent.getSource() instanceof JTree) {
				return ((JTree)anEvent.getSource()).isEditable();
			}
		}
		shouldSelect = false;
		return true;
	}

	@Override
	public boolean shouldSelectCell(EventObject anEvent) {
		if (anEvent == null) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public boolean stopCellEditing() {
		return false;
	}

	@Override
	public void cancelCellEditing() {
	}

	@Override
	public void addCellEditorListener(CellEditorListener l) {
	}

	@Override
	public void removeCellEditorListener(CellEditorListener l) {
	}

}
