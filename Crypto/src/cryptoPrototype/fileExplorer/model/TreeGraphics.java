package cryptoPrototype.fileExplorer.model;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;
/**
 * TreeCellRenderer Graphics for JTree
 * @author Martin Birch
 */
public class TreeGraphics implements TreeCellRenderer {
	private JPanel nodeContainer; // Stores current node being rendered
	private JLabel nodeLabel; // Stores the label of the node currently being rendered
	private int nodeWidth; //Used to store the preferred width of the node
	private int nodeHeight; //Used to store the preferred height of the node
	
	//private Color treeBGColor; // Background color of tree
	private Color focusBGColor; // Background color 
	
	private Icon closedIcon; // non-leaf nodes, not expanded
	private Icon openIcon; // non-leaf nodes, are expanded
	private Icon leafIcon; // leaf nodes
	
	//private Color textSelectionColor; // Foreground of selected nodes
	//private Color textNonSelectionColor; // Foreground of non-selected nodes
	private Color backgroundSelectionColor; // Background of selected nodes
	//private Color backgroundNonSelectionColor; // Background of non-selected nodes
	
	private Color borderSelectionColor; //Border color of Node in focus
	private Color borderfocusColor; //Border color of Node in focus
	
	/**
	 * Class Constructor
	 */
	public TreeGraphics() {
		populate();
		borderSelectionColor = Color.GRAY;
		backgroundSelectionColor = Color.LIGHT_GRAY;
		
		borderfocusColor = Color.LIGHT_GRAY;
		focusBGColor = Color.GRAY;
		nodeWidth = 100;
		nodeHeight = 34;
	}
	
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, 
													boolean expanded, boolean leaf, int row, boolean hasFocus) {


		Icon ico = null;
		nodeContainer = new JPanel(new FlowLayout(FlowLayout.LEFT));
		DefaultMutableTreeNode node = null;
		String filename = "";
		if (value != null) {
			DefaultMutableTreeNode sNode = (DefaultMutableTreeNode) value;
			FilePathGrouper fpGrouper = (FilePathGrouper) ((DefaultMutableTreeNode) value).getUserObject();
			if (fpGrouper != null && sNode != null) {
				node = sNode;
				filename = fpGrouper.getFilename();
			}
		}
		
		if (expanded) {
			ico = getOpenIcon();
		} else {
			ico = getClosedIcon();
		}
		
		if (hasFocus) {
			nodeContainer.setOpaque(true);
			nodeContainer.setBackground(focusBGColor);
			nodeContainer.setBorder(BorderFactory.createLineBorder(borderfocusColor, 2));
		} else {
			nodeContainer.setOpaque(false);
			nodeContainer.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		}
		
		if (sel) {
			nodeContainer.setOpaque(true);
			nodeContainer.setBackground(backgroundSelectionColor);
			nodeContainer.setBorder(BorderFactory.createLineBorder(borderSelectionColor, 2));
		}

		nodeLabel = new JLabel(filename);
		nodeLabel.setIcon(ico);
		nodeLabel.setOpaque(false);
		nodeContainer.add(nodeLabel);
		nodeContainer.setMinimumSize(new Dimension(determineWidth(node.getLevel(), tree), nodeHeight));
		nodeContainer.setPreferredSize(new Dimension(determineWidth(node.getLevel(), tree), nodeHeight));
		
		return nodeContainer;
	}
	
	/**
	 * Used to determine an appropriate width for a node, and sets the minimum size
	 * @param indent Levels in JTree
	 * @param tree JTree
	 * @return int width
	 */
	private int determineWidth(int indentLevel, JTree tree) {
		int width = nodeWidth - (indentLevel * 20); //--20 is the indentation size
		if (width <= (nodeLabel.getPreferredSize().getWidth() + 20)) {
			width = (int) nodeLabel.getPreferredSize().getWidth() + 15;
			nodeContainer.setMinimumSize(new Dimension(width, nodeHeight));
		}
		/*
		int fullWidth = width + (indentLevel * 20);
		if (fullWidth < tree.getWidth()) {
			width = tree.getWidth() - (indentLevel * 20);
		}
		*/
		return width;
	}
	
	/**
	 * Used to load images in
	 */
	private void populate() {
		closedIcon = scaleImageToIcon("/Images/closed folder circle.png");
		openIcon = scaleImageToIcon("/Images/closed folder.png");
	}
	
	/**
	 * Used to scale an image
	 * @param str String location of image
	 * @return Icon scaled
	 */
	private Icon scaleImageToIcon(String str) {
		Image img = new ImageIcon(getClass().getResource(str)).getImage();
		Image newimg = img.getScaledInstance(20, 20,  java.awt.Image.SCALE_SMOOTH);
		return new ImageIcon(newimg);
	}
	
	/**
	 * Changes the default image for the leaf Icon
	 * @param ico Icon to change leaf Icon to.
	 */
	public void setLeafIcon(Icon ico) {
		leafIcon = ico;
	}
	
	/**
	 * An Icon representing the leaf node
	 * @return Leaf Icon
	 */
	public Icon getLeafIcon() {
		return leafIcon;
	}
	
	/**
	 * Changes the default image for the closed non-leaf Icon
	 * @param ico Icon to change closed non-leaf Icon to.
	 */
	public void setClosedIcon(Icon ico) {
		closedIcon = ico;
	}
	
	/**
	 * Gets Icon representing the closed non-leaf node
	 * @return Icon
	 */
	public Icon getClosedIcon() {
		return closedIcon;
	}
	
	/**
	 * Changes the default image for the open non-leaf Icon
	 * @param ico Icon to change open non-leaf Icon to.
	 */
	public void setOpenIcon(Icon ico) {
		openIcon = ico;
	}
	
	/**
	 * Gets Icon representing the open non-leaf node
	 * @return Icon
	 */
	public Icon getOpenIcon() {
		return openIcon;
	}
	
	public int getRowHeight() {
		return nodeHeight;
	}
}
