package cryptoPrototype.fileExplorer.model;

import java.awt.Image;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.HashMap;
import java.util.Stack;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import cryptoPrototype.DirectoryProcessor;
import cryptoPrototype.DirectoryReader;
import cryptoPrototype.Encryption;
/**
 * An implementation of IGVFileHandler that can handle the reading of encrypted files
 * @author Martin Birch
 */
public class GVFileHandler implements IGVFileHandler {

	private HashMap<String, ImageIcon> imagesStandard;
	private HashMap<String, ImageIcon> imagesLiveCacheSmall;
	
	private Encryption encrypt;

	private int imageWidth;
	private int imageHeight;

	private Path rootDirectory;
	private Stack<Path> pathHistory;
	
	private HashMap<Path, String> files;
	private HashMap<Path, String> folders;
	
	private boolean directoryLocked;
	
	/**
	 * Class Constructor
	 * @param encrypt Encryption used on the files
	 */
	public GVFileHandler(Encryption encrypt) {
		imagesLiveCacheSmall = new HashMap<String, ImageIcon>();
		pathHistory = new Stack<Path>() {
			private static final long serialVersionUID = 1L;
			private static final int maxSize = 12;
			public Path push(Path item) {
		        if (maxSize == size()) {
		        	this.removeElementAt(maxSize - 1);
		        } 
				addElement(item);
		        return item;
		    }
		};
		this.encrypt = encrypt;
		imageWidth = 30;
		imageHeight = 30;
		populate();
		directoryLocked = false;
	}
	
	/**
	 * Sets the root directory of the model
	 */
	public void setRootDirectory(Path p) {
		rootDirectory = p;
	}
	
	/**
	 * Gets an image that has been associated with a certain extension type.
	 * Calls to this method are buffered.
	 */
	public Icon getImageByExtension(String extension) {
		ImageIcon img = imagesLiveCacheSmall.get(extension);
		if (img != null) {
			return img;
		} else {
			ImageIcon img2 = imagesStandard.get(extension);
			if (img2 != null) {
				Image img3 = img2.getImage().getScaledInstance(imageWidth, imageHeight, java.awt.Image.SCALE_FAST);
				imagesLiveCacheSmall.put(extension, new ImageIcon(img3));
				return new ImageIcon(img3);
			} else {
				return  new ImageIcon(imagesStandard.get("default").getImage().getScaledInstance(imageWidth, imageHeight, java.awt.Image.SCALE_SMOOTH));
			}
		}
	}

	/**
	 * Returns an icon associated with that path.
	 * First it checks the buffer, if it is not there is reads the file in and stores it in the buffer
	 * @param p Path of the file
	 * @return Icon
	 */
	private Icon getImageByPath(Path p) {
		ImageIcon img = imagesLiveCacheSmall.get(p.toString());
		if (img != null) {
			return img; // Buffered Image
		} else {
			ImageIcon img2 = null;
			try {
				img2 = new ImageIcon(getDecryptedImage(p)); //Image Read from HD
			} catch (Exception e) {
				img2 = (ImageIcon) getImageByExtension("default");
			}
			ImageIcon img3 = new ImageIcon(img2.getImage().getScaledInstance(imageWidth, imageHeight, java.awt.Image.SCALE_FAST));
			imagesLiveCacheSmall.put(p.toString(), img3);
			return img3;
		}
	}

	public Icon getImage(Path p, String strFileName) {
		Icon ico = null;
		if (Files.isDirectory(p)) {
			ico = getImageByExtension("folder");
		} else {
			String extension = getExtension(strFileName);
			if (isPicture(extension)) {
				ico = getImageByPath(p);	
			} else {
				ico = getImageByExtension(extension);
			}
		}
		
		return ico;
	}
	
	/**
	 * Returns the extension of a file, returns default if not extension is found
	 * @param name String filename or path
	 * @return extension String
	 */
	public String getExtension(String name) {
		String[] splited = name.split("\\.");
		if (splited != null && splited.length > 1) {
			return splited[(splited.length-1)];
		}
		return "default";
	}
	
	/**
	 * Returns a decrypted Image in the form of byte[]
	 * @param p Path of the image
	 * @return byte[] containing the data of an image
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 * @throws IOException
	 */
	private byte[] getDecryptedImage(Path p) throws InvalidKeyException, InvalidAlgorithmParameterException, IOException {
		return encrypt.decryptToBytes(p.toFile());
	}
	
	/**
	 * Used to test if the extension is an image
	 * @param extension
	 * @return
	 */
	public boolean isPicture(String extension) {
		return (extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("png") || extension.equalsIgnoreCase("jpeg"));
	}
	
	/**
	 * Returns whether the Path points to a picture
	 * @param p
	 * @return
	 */
	public boolean isPicture(Path p) {
		if (Files.isDirectory(p)) {
			return false;
		} else {
			return isPicture(getExtension(p.toString()));
		}
	}
	
	@Override
	public boolean isTextFile(Path p) {
		if (Files.isDirectory(p)) {
			return false;
		} else {
			return getExtension(p.toString()).equals("txt");
		}
	}
	
	/**
	 * Used to load the files and folders in of a directory
	 */
	private void loadDirectory() {
		files = new HashMap<Path, String>();
		folders = new HashMap<Path, String>();
		DirectoryReader reader = new DirectoryReader( new DirectoryProcessor() {
			@Override
			public boolean processDirectory(Path p, String dirName) {
				folders.put(p, dirName);
				return false;
			}

			@Override
			public void processFile(Path p, String fileName) {
				if (fileName != null && fileName != "MASTER_FILE" ) {
					files.put(p, fileName);					
				}
			}
		}, encrypt);
		reader.readDirectory(pathHistory.peek(), true);
	}
	
	/**
	 * Used to refresh a directory
	 */
	public void refreshDirectory() {
		loadDirectory();
	}
	
	@Override
	public HashMap<Path, String> getFiles() {
		return files;
	}

	@Override
	public HashMap<Path, String> getFolders() {
		return folders;
	}

	@Override
	public boolean setDirectory(Path p) {
		if (p != null) {		
			if (directoryLocked) {
				if ((rootDirectory.compareTo(p)) <= 0) {
					pathHistory.push(p);
					loadDirectory();
					return true;
				}
			} else {
				pathHistory.push(p);
				loadDirectory();
				return true; //If the GV is not locked to a directory
			}
		}
		return false; //If An invalid directory was provided
	}
	
	@Override
	public Path getParentDirectory() {
		Path p = pathHistory.peek().getParent();
		if (p != null) {		
			if (directoryLocked) {
				if ((rootDirectory.compareTo(p)) <= 0) {
					return p;
				}
			} else {
				return p; //If the GV is not locked to a directory
			}
		}
		return null;
	}

	@Override
	public Path getCurrentDirectory() {
		return pathHistory.peek();
	}
	
	public Path getPreviousDirectory() {
		if (pathHistory.size() > 1) {
			Path current = pathHistory.pop();
			Path previous = pathHistory.peek();
			pathHistory.push(current);
			return previous;
		}
		return null;
	}
	
	/**
	 * Used to load the HashMap imageStandard with default images for regular extension types
	 */
	private void populate() {
		imagesStandard = new HashMap<String, ImageIcon>();
		imagesStandard.put("default", new ImageIcon(getClass().getResource("/Images/tiger2.png")));
		imagesStandard.put("folder", new ImageIcon(getClass().getResource("/Images/closed folder.png")));
		
		imagesStandard.put("doc", new ImageIcon(getClass().getResource("/Images/word.png")));
		imagesStandard.put("docx", new ImageIcon(getClass().getResource("/Images/word.png")));
		imagesStandard.put("accdb", new ImageIcon(getClass().getResource("/Images/Access.png")));
		imagesStandard.put("xlsm", new ImageIcon(getClass().getResource("/Images/Excel.png")));
		imagesStandard.put("xlsx", new ImageIcon(getClass().getResource("/Images/Excel.png")));
		imagesStandard.put("ppt", new ImageIcon(getClass().getResource("/Images/Powerpoint.png")));
		imagesStandard.put("pptx", new ImageIcon(getClass().getResource("/Images/Powerpoint.png")));
		
		imagesStandard.put("pdf", new ImageIcon(getClass().getResource("/Images/acroread.png")));

		imagesStandard.put("txt", new ImageIcon(getClass().getResource("/Images/pad.png")));
		
		ImageIcon musicImg = new ImageIcon(getClass().getResource("/Images/MediaCenter_Green.png"));
		imagesStandard.put("wmv", musicImg);
		imagesStandard.put("mp3", musicImg);
		imagesStandard.put("flac", musicImg);
		imagesStandard.put("mp4", musicImg);
	}
	
	public void setSize(int width, int height) {
		imageWidth = width;
		imageHeight = height;
	}

	public void lockToDirectory(boolean lock) {
		directoryLocked = lock;
	}
	
	public Encryption getEncryption() {
		return encrypt;
	}
}
