package cryptoPrototype.fileExplorer.model;

import java.nio.file.Path;

import cryptoPrototype.FilePaths;
/**
 * This class is used to encapsulate the information about a file
 * @author Martin Birch
 */
public class FilePathGrouper implements FilePaths {
	
	private String filename;
	private Integer encryptedFileName;
	private Path filepath;
	private boolean process;
	
	/**
	 * Class Constructor.
	 * @param filename The unencrypted filename of the file
	 * @param filepath The path to the file
	 * @param processFile boolean should the file be displayed
	 */
	public FilePathGrouper(String filename, Path filepath, boolean processFile) {
		this.filename = filename;
		this.filepath = filepath;
		this.process = processFile;
		if (filename == "Files") {
			encryptedFileName = -1;
		} else {
			encryptedFileName = Integer.parseInt(filepath.getFileName().toString());			
		}
	}

	/**
	 * Should the file be processed
	 * @return boolean
	 */
	public boolean isProcessable() {
		return process;
	}
	
	/**
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @param filename the filename to set
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Integer getEncryptedFileName() {
		return encryptedFileName;
	}
	
	/**
	 * @return the filepath
	 */
	public Path getFilepath() {
		return filepath;
	}

	/**
	 * @param filepath the filepath to set
	 */
	public void setFilepath(Path filepath) {
		this.filepath = filepath;
	}
	
	public String toString() {
		return filepath.toString();
	}
}
