package cryptoPrototype.fileExplorer.model;

import java.nio.file.Path;
import java.util.HashMap;

import javax.swing.Icon;

import cryptoPrototype.Encryption;

public interface IGVFileHandler {
	
	/**
	 * Used to Handle to retrieval of icon from a given path
	 * @param p The Path of the image
	 * @param strFileName the name of the file to get
	 * @return A scaled Icon
	 */
	public Icon getImage(Path p, String strFileName);
	
	/**
	 * Used to read the files in to be displayed
	 * @return An ArrayList<Path> where each Path relates to a file
	 */
	public HashMap<Path, String> getFiles();
	
	/**
	 * Used to read in the folders to be displayed
	 * @return An ArrayList<Path> where each Path relates to a folder
	 */
	public HashMap<Path, String> getFolders();
	
	/**
	 * Used to refresh the files and folders loaded from the directory
	 */
	public void refreshDirectory();
	
	/**
	 * Used to set the working directory
	 * @param The Path of the directory
	 * @return boolean if the change is valid and is successful
	 */
	public boolean setDirectory(Path p);
	
	/**
	 * Gets the parent directory of the current directory being viewed
	 */
	public Path getParentDirectory();
	
	/**
	 * Gets the previous path that was set
	 * @return A Path
	 */
	public Path getPreviousDirectory();
	
	/**
	 * Gets the current working directory
	 */
	public Path getCurrentDirectory();
	
	/**
	 * Used to Handle to retrieval of an Image from a given path
	 * @param extension of the file image
	 * @return A scaled Icon
	 */
	public Icon getImageByExtension(String extension);

	/**
	 * Sets the Image sizes to use
	 */
	public void setSize(int width, int height);

	/**
	 * Sets whether the GridView should be locked to so it can't browse above the root directory
	 * @param lock
	 */
	public void lockToDirectory(boolean lock);
	
	/**
	 * Sets the root directory
	 * @param p Path of directory
	 */
	public void setRootDirectory(Path p);
	
	/**
	 * Tests to check whether the file is an image
	 * @param pFileName String file name of the file
	 * @return true, or false if the file is an image
	 */
	public boolean isPicture(Path pFileName);
	
	/**
	 * Tests to check whether the file is an text file
	 * @return true, or false if the file is an text file
	 */
	public boolean isTextFile(Path p);

	/**
	 * Gets the Encrypt object used in this file Handler
	 * @return Encryption, or null if no encryption is used
	 */
	public Encryption getEncryption();
	
	/**
	 * Returns the extension of a file, returns default if not extension is found
	 * @param name String filename or path
	 * @return extension String
	 */
	public String getExtension(String name);
}
