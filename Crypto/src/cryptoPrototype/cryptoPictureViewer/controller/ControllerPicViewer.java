package cryptoPrototype.cryptoPictureViewer.controller;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.nio.file.Path;

import javax.swing.JButton;
import javax.swing.JFileChooser;

import cryptoPrototype.Encryption;
import cryptoPrototype.cryptoPictureViewer.model.ImageHandler;
import cryptoPrototype.cryptoPictureViewer.view.PictureViewer;
/**
 * A controller used to add functionality to the PictureViewer class
 * @author Martin Birch
 */
public class ControllerPicViewer implements ActionListener, WindowListener {

	private PictureViewer viewerGUI;
	private ImageHandler imageHandler;
	private boolean isFullScreen;
	
	/**
	 * Class Constructor
	 * @param viewerGUI PictureViewer to add functionality to
	 * @param currentDirectory Directory to browse for images
	 * @param encrypt The Encryption used on the images
	 */
	public ControllerPicViewer(PictureViewer viewerGUI, Path currentDirectory, Encryption encrypt) {
		this.viewerGUI = viewerGUI;
		imageHandler = new ImageHandler(currentDirectory, encrypt);
		BufferedImage startImage = imageHandler.getCurrent();
		if (startImage != null) {
			viewerGUI.setImage(startImage);
			viewerGUI.setPictureName(imageHandler.getCurrentPictureName());
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() instanceof JButton) {
			JButton btn = (JButton) evt.getSource();
			switch(btn.getName()) {
			case "btnDelete":
				imageHandler.delete();
				viewerGUI.setImage(imageHandler.getCurrent());
				break;
			case "btnPrevious":
				viewerGUI.setImage(imageHandler.getPrevious());
				viewerGUI.setPictureName(imageHandler.getCurrentPictureName());
				break;
			case "btnFullScreen":
				if (isFullScreen) {
					viewerGUI.setSize(600, 600);
					isFullScreen = false;
				} else {
					Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
					viewerGUI.setSize(screen.width, screen.height);
					isFullScreen = true;
				}
				break;
			case "btnNext":
				viewerGUI.setImage(imageHandler.getNext());
				viewerGUI.setPictureName(imageHandler.getCurrentPictureName());
				break;
			case "btnDownload":
				JFileChooser jfcInputFilePath = new JFileChooser();
				int choice = jfcInputFilePath.showDialog(viewerGUI.getWindow(), "Download");
				if (choice == JFileChooser.APPROVE_OPTION) {
					imageHandler.download(jfcInputFilePath.getSelectedFile());
				}
			}
		}
	}

	@Override
	public void windowActivated(WindowEvent arg0) {}

	@Override
	public void windowClosed(WindowEvent arg0) {}

	@Override
	public void windowClosing(WindowEvent evt) {
		imageHandler.closeAndSave();
		viewerGUI.firePropertyChange("WindowClosed", false, true);
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {}

	@Override
	public void windowDeiconified(WindowEvent arg0) {}

	@Override
	public void windowIconified(WindowEvent arg0) {}

	@Override
	public void windowOpened(WindowEvent arg0) {}

}
