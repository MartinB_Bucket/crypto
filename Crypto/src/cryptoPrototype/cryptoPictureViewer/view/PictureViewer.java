package cryptoPrototype.cryptoPictureViewer.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cryptoPrototype.JImagePanel;
import cryptoPrototype.cryptoPictureViewer.controller.ControllerPicViewer;
/**
 * Basic PictureViewer requires ControllerPicViewer to add functionality
 * @author Martin Birch
 */
public class PictureViewer {
	
	private PropertyChangeSupport pSupport;
	private ControllerPicViewer controller;
	private JFrame window;
	private JPanel pnlImage;
	private BufferedImage image;
	private String imageName;
	
	/**
	 * Class Constructor
	 */
	public PictureViewer() {
		imageName = "";
		pSupport = new PropertyChangeSupport(this);
		createJFrame();
	}
	
	/**
	 * Class Constructor
	 * @param image BufferedImage to display upon loading
	 * @param imageName String
	 */
	public PictureViewer(BufferedImage image, String imageName) {
		this.image = image;
		this.imageName = imageName;
		createJFrame();
	}
	
	/**
	 * Used to create the JFrame and content
	 */
	private void createJFrame() {
		window = new JFrame("Picture Viewer: " + imageName);
		window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		window.setSize(600, 600);
		window.setLocationRelativeTo(null);
		createGUI();
	}
	
	/**
	 * Creates the content inside the JFrame
	 */
	private void createGUI() {
		window.getContentPane().removeAll();
		JPanel pnlRoot = new JPanel(new BorderLayout());
		
		pnlImage = new JPanel(new BorderLayout());
		if (image != null) {
			pnlImage.add(new JImagePanel(image), BorderLayout.CENTER);
		} else {
			JPanel pnlImageSub = new JPanel(new FlowLayout(FlowLayout.CENTER));
			pnlImageSub.add(new JLabel("Image Could Not Be Displayed"));
			pnlImage.add(pnlImageSub, BorderLayout.CENTER);
		}
		
		pnlRoot.add(pnlImage, BorderLayout.CENTER);
		
		JPanel pnlMenu = new JPanel();
		JButton jbtDelete = new JButton("Delete"); 
		jbtDelete.setName("btnDelete");
		JButton jbtPreviousImage = new JButton("<<"); 
		jbtPreviousImage.setName("btnPrevious");
		JButton jbtFullScreen = new JButton("FullScreen"); 
		jbtFullScreen.setName("btnFullScreen");
		JButton jbtNextImage = new JButton(">>"); 
		jbtNextImage.setName("btnNext");
		JButton jbtDownload = new JButton("Download"); 
		jbtDownload.setName("btnDownload");
		
		if (controller != null) {
			window.addWindowListener(controller);
			jbtDelete.addActionListener(controller);
			jbtPreviousImage.addActionListener(controller);
			jbtFullScreen.addActionListener(controller);
			jbtNextImage.addActionListener(controller);
			jbtDownload.addActionListener(controller);
			window.addWindowListener(controller);
			jbtDelete.setEnabled(true);
			jbtPreviousImage.setEnabled(true);
			jbtFullScreen.setEnabled(true);
			jbtNextImage.setEnabled(true);
			jbtDownload.setEnabled(true);
		} else {
			jbtDelete.setEnabled(false);
			jbtPreviousImage.setEnabled(false);
			jbtFullScreen.setEnabled(false);
			jbtNextImage.setEnabled(false);
			jbtDownload.setEnabled(false);
		}
		
		pnlMenu.add(jbtDelete);
		pnlMenu.add(jbtPreviousImage);
		pnlMenu.add(jbtFullScreen);
		pnlMenu.add(jbtNextImage);
		pnlMenu.add(jbtDownload);
		
		pnlRoot.add(pnlMenu, BorderLayout.SOUTH);
		window.add(pnlRoot);
	}
	
	/**
	 * Sets the visibility of the Picture Viewer
	 * @param visible true or false
	 */
	public void setVisible(boolean visible) {
		window.setVisible(visible);
	}

	/**
	 * Refreshes the display
	 */
	public void refreshDisplay() {
		pnlImage.revalidate();
		pnlImage.repaint();
		window.revalidate();
		window.repaint();
	}
	
	/**
	 * Adds a controller to the Picture Viewer
	 * @param controller ControllerPicViewer
	 */
	public void addController(ControllerPicViewer controller) {
		this.controller = controller;
		createGUI();
	}

	/**
	 * Sets the Image being displayed and refreshes the display
	 * @param image BufferedImage to display
	 */
	public void setImage(BufferedImage image) {
		this.image = image;
		pnlImage.removeAll();
		if (image != null) {
			pnlImage.add(new JImagePanel(image), BorderLayout.CENTER);
		} else {
			pnlImage.add(new JLabel("Image Could Not Be Displayed"), BorderLayout.CENTER);
		}
		refreshDisplay();
	}

	/**
	 * Sets the name of the Picture at the top of the JFrame
	 * @param name
	 */
	public void setPictureName(String name) {
		imageName = name;
		window.setTitle("Picture Viewer: " + imageName);
	}

	/**
	 * Adds a Property Change Listener to this class
	 * @param listener PropertyChangeListener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pSupport.addPropertyChangeListener(listener);
	}
	
	/**
	 * Fires a Property Change Event
	 * @param propertyName String
	 * @param b1 old value boolean
	 * @param b2 new value boolean
	 */
	public void firePropertyChange(String propertyName, boolean b1, boolean b2) {
		pSupport.firePropertyChange(propertyName, b1, b2);
	}

	/**
	 * Sets the size of this JFrame
	 * @param width int
	 * @param height int
	 */
	public void setSize(int width, int height) {
		window.setSize(width, height);
		window.setLocationRelativeTo(null);
	}
	
	/**
	 * Gets the JFrame being used
	 * @return
	 */
	public JFrame getWindow() {
		return window;
	}
}
