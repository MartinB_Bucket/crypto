package cryptoPrototype.cryptoPictureViewer.model;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;

import cryptoPrototype.DirectoryProcessor;
import cryptoPrototype.DirectoryReader;
import cryptoPrototype.Encryption;
import cryptoPrototype.FileSystem;
/**
 * Provides methods for reading and buffering images being read in a directory
 * @author Martin Birch
 */
public class ImageHandler {
	
	private Encryption encrypt;
	private Path currentDirectory;
	private int currentImageIndex;
	
	private HashMap<Path, BufferedImage> imageFiles;
	private ArrayList<Path> pathIndex;
	private FileSystem fs;
	/**
	 * Class Constructor, loads in two images in a seperate thread
	 * @param currentDirectory The Directory to be searched for images
	 * @param encrypt The Encryption used on the images
	 */
	public ImageHandler(Path currentDirectory, Encryption encrypt) {
		this.encrypt = encrypt;
		this.currentDirectory = currentDirectory;
		imageFiles = new HashMap<Path, BufferedImage>();
		pathIndex = new ArrayList<Path>();
		fs = new FileSystem(encrypt);
		loadImagePaths();
	}
	
	/**
	 * Tests whether the extension is an image
	 * @param extension
	 * @return
	 */
	private static boolean isPicture(Path p, Encryption encrypt) {
		try {
		    Image image = ImageIO.read(  new ByteArrayInputStream(
		    		encrypt.decryptToBytes( p.toFile() )  )  );
		    if (image == null) {
		        return false;
		    } else {
		    	return true; 	
		    }
		} catch(IOException | InvalidKeyException | InvalidAlgorithmParameterException ex) {
		    return false;
		}
	}

	public void closeAndSave() {
		fs.close();
	}
	
	/**
	 * Gets the extension type of a file
	 * @param name String filename
	 * @return String extension type
	 *//*
	private String getExtension(String name) {
		String[] splited = name.split("\\.");
		if (splited != null && splited.length > 1) {
			return splited[(splited.length-1)];
		}
		return "default";
	}*/
	
	/**
	 * Loads all the image paths from a directory and stores them in the ArrayList
	 */
	private void loadImagePaths() {
		DirectoryReader reader = new DirectoryReader( new DirectoryProcessor() {
			@Override
			public boolean processDirectory(Path p, String str) {
				return false;
			}

			@Override
			public void processFile(Path p, String str) {
				if (isPicture(p, encrypt)) {
					pathIndex.add(p);
					imageFiles.put(p, null);
					if (pathIndex.size() < 3) {
						loadImage(p);
					}
				}
			}

		}, encrypt);
		reader.readDirectory(currentDirectory, true);
	}
	
	/**
	 * Reads an image from the hard drive, decrypts it and stores it in the hashmap
	 * @param p Path of the image
	 */
	private void loadImage(Path p) {
		try {
			imageFiles.replace(p, ImageIO.read(new ByteArrayInputStream(encrypt.decryptToBytes(p.toFile()))));
		} catch (IOException | InvalidKeyException | InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets an image from the hashmap given an index for the path ArrayList
	 */
	private BufferedImage getImage(int index) {
		if (pathIndex.size() > 0) {
			Path p = pathIndex.get(index);
			BufferedImage img = imageFiles.get(p);
			if (img == null) {
				loadImage(p);
				return imageFiles.get(p);
			} else {
				return img;
			}
		}
		return null;
	}
	
	/**
	 * Preloads the next image in the background
	 */
	private void preloadNext() {
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				if (pathIndex.size() > currentImageIndex + 2) {
					getImage(currentImageIndex + 1);
				} else {
					getImage(0);
				}
			}
		});
		t1.start();
	}
	
	/**
	 * Preloads the previous image in the background
	 */
	private void preloadPrevious() {
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				if (currentImageIndex == 0) {
					getImage(pathIndex.size() - 1);
				} else {
					getImage(currentImageIndex - 1);
				}
			}
		});
		t1.start();
	}
	
	/**
	 * Gets the next image
	 * @return BufferedImage
	 */
	public BufferedImage getNext() {
		if (pathIndex.size() > currentImageIndex + 1) {
			++currentImageIndex;
			preloadNext();
			updateBufferRange();
			return getImage(currentImageIndex);
		} else {
			currentImageIndex = 0;
			preloadNext();
			updateBufferRange();
			return getImage(currentImageIndex);
		}
	}
	
	/**
	 * Gets the previous image
	 * @return BufferedImage
	 */
	public BufferedImage getPrevious() {
		if (!pathIndex.isEmpty()) {
			if (currentImageIndex == 0) {
				currentImageIndex = pathIndex.size() - 1;
				preloadPrevious();
				updateBufferRange();
				return getImage(currentImageIndex);
			} else {
				--currentImageIndex;
				preloadPrevious();
				updateBufferRange();
				return getImage(currentImageIndex);
			}
		} else {
			return null; 
		}
	}
	
	public String getCurrentPictureName() {
		if (!pathIndex.isEmpty()) {
			return fs.getFileName(pathIndex.get(currentImageIndex));
		} else {
			return null;
		}
	}
	
	/**
	 * Gets the current image
	 * @return BufferedImage
	 */
	public BufferedImage getCurrent() {
		if (!pathIndex.isEmpty()) {
			return getImage(currentImageIndex);
		} else {
			return null;
		}
	}
	
	/**
	 * Deletes the current image
	 */
	public void delete() {
		FileSystem fs = new FileSystem(encrypt);
		fs.deleteFile(pathIndex.get(currentImageIndex));
		fs.close();
		//Files.delete(pathIndex.get(currentImageIndex));
		imageFiles.remove(pathIndex.get(currentImageIndex));
		pathIndex.remove(currentImageIndex);
		getPrevious();
	}
	
	/**
	 * Decrypts the encrypted image and saves it to the specified location
	 * @param f File location to save the image to
	 * @return true, or false depending on whether the image was successfully decrypted
	 */
	public boolean download(File f) {
		if (!pathIndex.isEmpty()) {
			if (currentImageIndex > -1 && currentImageIndex < pathIndex.size()) {
				try {
					encrypt.decryptFile(pathIndex.get(currentImageIndex).toFile(), f);
					return true;
				} catch (InvalidKeyException | InvalidAlgorithmParameterException | IOException e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	/**
	 * Removes images stored in the hashmap that are greater than 1 index away from the current Image Index
	 * Prevents lots of memory being used if there is a directory with lots of images being viewed
	 */
	private void updateBufferRange() {
		if (pathIndex.size() > 3) {
			int previousIndex = currentImageIndex - 2;
			int futureIndex = currentImageIndex + 2;
			if (previousIndex < 0) {
				previousIndex = (pathIndex.size() - 1) + previousIndex;
			}
			if (futureIndex >= pathIndex.size()) {
				futureIndex = futureIndex - pathIndex.size();
			}
			imageFiles.replace(pathIndex.get(previousIndex), null);
			imageFiles.replace(pathIndex.get(futureIndex), null);
		}
	}
}
