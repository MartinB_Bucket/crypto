scp -r ./$1 avrfiddle:~/avrfiddle
SCRIPT="sudo cp app.wsgi avrfiddle; sudo rm -r /var/www/avrfiddle; sudo mv avrfiddle /var/www/avrfiddle; cd /var/www/; sudo chmod -R 755 avrfiddle; chmod 757 avrfiddle/avr_simulator/resources; sudo service apache2 restart"
ssh avrfiddle "${SCRIPT}"
